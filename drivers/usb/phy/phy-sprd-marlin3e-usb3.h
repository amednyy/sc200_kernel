#ifndef _PHY_SPRD_MARLIN3E_USB3_H_
#define _PHY_SPRD_MARLIN3E_USB3_H_

#include <misc/wcn_bus.h>
#include <linux/slab.h>

#define MARLIN3E_USB_BASE 0x40d00000
#define SPRD_MARLIN3E_USB3_PHY

static inline
int sprdwcn_bus_reg_setmask(unsigned int addr, unsigned int mask)
{
	struct sprdwcn_bus_ops *bus_ops = get_wcn_bus_ops();
	int temp_val = 0;
	if (!bus_ops || !bus_ops->read_l)
		return 0;

	bus_ops->read_l(addr, &temp_val);

	temp_val |= mask;

	return bus_ops->write_l(addr, &temp_val);
}

static inline
int sprdwcn_bus_reg_clearmask(unsigned int addr, unsigned int mask)
{
	struct sprdwcn_bus_ops *bus_ops = get_wcn_bus_ops();
	int temp_val = 0;
	if (!bus_ops || !bus_ops->read_l)
		return 0;

	bus_ops->read_l(addr, &temp_val);

	temp_val &= ~mask;

	return bus_ops->write_l(addr, &temp_val);
}

static inline
int sprdwcn_bus_reg_readl(unsigned int addr)
{
	struct sprdwcn_bus_ops *bus_ops = get_wcn_bus_ops();
	int temp_val = 0;
	if (!bus_ops || !bus_ops->read_l)
	{
		printk(KERN_EMERG "sprdwcn_bus_reg_readl error !!!!!\n");
		return 0;
	}

	bus_ops->read_l(addr, &temp_val);

	return temp_val;
}

static inline
int sprdwcn_bus_reg_writel(unsigned int addr,int data)
{
	struct sprdwcn_bus_ops *bus_ops = get_wcn_bus_ops();

	if (!bus_ops || !bus_ops->write_l)
	{

		printk(KERN_EMERG "sprdwcn_bus_reg_writel error !!!!!\n");
		return 0;
	}

	return bus_ops->write_l(addr, &data);
}

static inline
int sprdwcn_bus_memset(unsigned int addr,int data,unsigned int size)
{
	void *buf;
	buf = kmalloc(size,GFP_KERNEL);
	if(!buf)
	{
		printk(KERN_ERR "sprdwcn_bus_memset error \n");
		return -1;

	}
	memset(buf,data,size);
	sprdwcn_bus_direct_write_unbuffered(addr,buf,size);
	kfree(buf);

	return 0;
}

/* src -> dst */
static inline
int sprdwcn_bus_reg_copy(unsigned int src,unsigned int dst)
{
	struct sprdwcn_bus_ops *bus_ops = get_wcn_bus_ops();
	int temp_val = 0;
	if (!bus_ops || !bus_ops->read_l)
		return 0;
	bus_ops->read_l(src, &temp_val);

	return bus_ops->write_l(dst, &temp_val);
}

static inline
int sprdwcn_bus_mem_copy(unsigned int src,unsigned int dst,int size)
{
	struct sprdwcn_bus_ops *bus_ops = get_wcn_bus_ops();
	int *buf;
	if (!bus_ops || !bus_ops->read_l)
		return 0;
	buf = kmalloc(size,GFP_KERNEL);
	if(!buf)
	{
		printk(KERN_ERR "sprdwcn_bus_mem_copy error \n");
		return -1;

	}

	bus_ops->direct_read(src, buf, size);
    bus_ops->direct_write(dst, buf, size);
	kfree(buf);
	return size;

}




#endif
