/**
 * phy-sprd-marlin3e-usb3.c - Spreadtrum USB3 PHY Glue layer
 *
 * Copyright (c) 2015 Spreadtrum Co., Ltd.
 *		http://www.spreadtrum.com
 *
 * Author: Miao Zhu <miao.zhu@spreadtrum.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2  of
 * the License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/regmap.h>
#include <linux/mfd/syscon.h>
#include <linux/platform_device.h>
#include <linux/regulator/consumer.h>
#include <linux/usb/phy.h>
#include <linux/bitops.h>
#include <linux/genalloc.h>
#include <linux/init.h>

#include "phy-sprd-marlin3e-usb3.h"

struct sprd_ssphy {
	struct usb_phy		phy;

	atomic_t		reset;
	atomic_t		inited;
	atomic_t		susped;
};


int sprd_marlin3e_phy_init(void)
{
	printk("\n sprd_marlin3e_phy_init !\n");

	/* do in marlin3e,return directly here */
	return 0;
}

static int sprd_ssphy_init(struct usb_phy *x)
{
	struct sprd_ssphy *phy = container_of(x, struct sprd_ssphy, phy);

	if (atomic_read(&phy->inited)) {
		dev_dbg(x->dev, "%s is already inited!\n", __func__);
		return 0;
	}

	sprd_marlin3e_phy_init();

	if (!atomic_read(&phy->reset)) {
		msleep(5);

		atomic_set(&phy->reset, 1);
	}

	atomic_set(&phy->inited, 1);

	return 0;
}

/* Turn off PHY and core */
static void sprd_ssphy_shutdown(struct usb_phy *x)
{
	struct sprd_ssphy *phy = container_of(x, struct sprd_ssphy, phy);

	if (!atomic_read(&phy->inited)) {
		dev_dbg(x->dev, "%s is already shut down\n", __func__);
		return;
	}

	atomic_set(&phy->inited, 0);
	atomic_set(&phy->reset, 0);
}


static struct sprd_ssphy *__sprd_ssphy;

static int sprd_marlin3e_ssphy_probe(struct platform_device *pdev)
{
	struct sprd_ssphy *phy;
	struct device *dev = &pdev->dev;

	int ret = 0;

	phy = devm_kzalloc(dev, sizeof(*phy), GFP_KERNEL);
	if (!phy)
		return -ENOMEM;

	__sprd_ssphy = phy;

	platform_set_drvdata(pdev, phy);
	phy->phy.dev				= dev;
	phy->phy.label				= "sprd-ssphy";
	phy->phy.init				= sprd_ssphy_init;
	phy->phy.shutdown			= sprd_ssphy_shutdown;
	/*
	 * TODO:
	 * phy->phy.set_suspend			= sprd_ssphy_set_suspend;
	 * phy->phy.notify_connect		= sprd_ssphy_notify_connect;
	 * phy->phy.notify_disconnect		= sprd_ssphy_notify_disconnect;
	 * phy->phy.reset			= sprd_ssphy_reset;
	 */
	phy->phy.type				= USB_PHY_TYPE_USB3;

	ret = usb_add_phy_dev(&phy->phy);
	if (ret) {
		dev_err(dev, "fail to add phy\n");
		return ret;
	}

	sprd_marlin3e_phy_init();
	return 0;
}

static int sprd_marlin3e_ssphy_remove(struct platform_device *pdev)
{
	struct sprd_ssphy *phy = platform_get_drvdata(pdev);

	//sysfs_remove_groups(&pdev->dev.kobj, usb_ssphy_groups);
	usb_remove_phy(&phy->phy);
	//regulator_disable(phy->vdd);
	devm_kfree(&pdev->dev,phy);

	return 0;
}

static const struct of_device_id sprd_marlin3e_ssphy_match[] = {
	{ .compatible = "sprd,marlin3e_ssphy" },
	{},
};

MODULE_DEVICE_TABLE(of, sprd_marlin3e_ssphy_match);

static struct platform_driver sprd_marlin3e_ssphy_driver = {
	.probe		= sprd_marlin3e_ssphy_probe,
	.remove		= sprd_marlin3e_ssphy_remove,
	.driver		= {
		.name	= "sprd-ssphy",
		.of_match_table = sprd_marlin3e_ssphy_match,
	},
};


int  sprd_marlin3e_ssphy_driver_init(void)
{
	printk("dwc3: %s enter\n", __func__);
	return platform_driver_register(&sprd_marlin3e_ssphy_driver);
}
EXPORT_SYMBOL(sprd_marlin3e_ssphy_driver_init);

void  sprd_marlin3e_ssphy_driver_exit(void)
{
	printk("dwc3: %s enter \n", __func__);
	platform_driver_unregister(&sprd_marlin3e_ssphy_driver);
	printk("dwc3: %s exit\n", __func__);
}
EXPORT_SYMBOL(sprd_marlin3e_ssphy_driver_exit);


MODULE_ALIAS("platform:sprd-marlin3e-ssphy	");
MODULE_AUTHOR("Kun Sun <skdun.sun@unisoc.com>");
MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("DesignWare USB3 SPRD PHY Glue Layer");

