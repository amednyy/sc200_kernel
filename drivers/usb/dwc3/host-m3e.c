/**
 * host.c - DesignWare USB3 DRD Controller Host Glue
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com
 *
 * Authors: Felipe Balbi <balbi@ti.com>,
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2  of
 * the License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/platform_device.h>
#include <linux/usb.h>
#include <linux/usb/hcd.h>

#include "core.h"
#include "../host/xhci-sdio.h"


#define DWC3_HOST_SUSPEND_COUNT		100
#define DWC3_HOST_SUSPEND_TIMEOUT	100

int dwc3_host_init(struct dwc3 *dwc)
{
	struct property_entry	props[3];
	struct platform_device	*xhci;
	int ret;
	int			prop_idx = 0;

	dev_info(dwc->dev, "%s enter", __func__);
	xhci = platform_device_alloc("xhci-hcd", PLATFORM_DEVID_AUTO);
	if (!xhci) {
		dev_err(dwc->dev, "couldn't allocate xHCI device\n");
		return -ENOMEM;
	}

	/*
	 * FIXME: It seems we should set the dma operations firstly when setting
	 * the dma mask, otherwise it will set dma mask failed.
	 */
	if (get_dma_ops(&xhci->dev) == get_dma_ops(NULL))
		xhci->dev.dma_ops = get_dma_ops(dwc->dev);

	dma_set_coherent_mask(&xhci->dev, dwc->dev->coherent_dma_mask);

	xhci->dev.parent	= dwc->dev;
	xhci->dev.dma_mask	= dwc->dev->dma_mask;
	xhci->dev.dma_parms	= dwc->dev->dma_parms;

	dwc->xhci = xhci;

	memset(props, 0, sizeof(struct property_entry) * ARRAY_SIZE(props));

	if (dwc->usb3_lpm_capable)
		props[prop_idx++].name = "usb3-lpm-capable";

	if (dwc->usb3_slow_suspend)
		props[prop_idx++].name = "usb3-slow-suspend";

	/**
	 * WORKAROUND: dwc3 revisions <=3.00a have a limitation
	 * where Port Disable command doesn't work.
	 *
	 * The suggested workaround is that we avoid Port Disable
	 * completely.
	 *
	 * This following flag tells XHCI to do just that.
	 */
	if (dwc->revision <= DWC3_REVISION_300A)
		props[prop_idx++].name = "quirk-broken-port-ped";

	if (prop_idx) {
		ret = platform_device_add_properties(xhci, props);
		if (ret) {
			dev_err(dwc->dev, "failed to add properties to xHCI\n");
			goto err1;
		}
	}

	phy_create_lookup(dwc->usb2_generic_phy, "usb2-phy",
			  dev_name(dwc->dev));
	phy_create_lookup(dwc->usb3_generic_phy, "usb3-phy",
			  dev_name(dwc->dev));

	ret = platform_device_add(xhci);
	if (ret) {
		dev_err(dwc->dev, "failed to register xHCI device\n");
		goto err2;
	}
	dev_info(dwc->dev, "%s exit", __func__);
	return 0;
err2:
	phy_remove_lookup(dwc->usb2_generic_phy, "usb2-phy",
			  dev_name(dwc->dev));
	phy_remove_lookup(dwc->usb3_generic_phy, "usb3-phy",
			  dev_name(dwc->dev));
err1:
	platform_device_put(xhci);
	return ret;
}

void dwc3_host_exit(struct dwc3 *dwc)
{
	dev_info(dwc->dev, "%s enter", __func__);
	phy_remove_lookup(dwc->usb2_generic_phy, "usb2-phy",
			  dev_name(dwc->dev));
	phy_remove_lookup(dwc->usb3_generic_phy, "usb3-phy",
			  dev_name(dwc->dev));
	platform_device_unregister(dwc->xhci);
}

static int dwc3_host_suspend_child(struct device *dev, void *data)
{
	int cnt = DWC3_HOST_SUSPEND_COUNT;

	while (!pm_runtime_suspended(dev) && --cnt > 0)
		msleep(500);

	if (cnt <= 0) {
		dev_err(dev, "xHCI child device enters suspend failed!!!\n");
		return -EAGAIN;
	}

	return 0;
}

int dwc3_host_suspend(struct dwc3 *dwc)
{
	struct device *xhci = &dwc->xhci->dev;
	int ret;

	/*
	 * In the "WCN convert usb hub" project:
	 * 1. the xhci need keep resume state to check if usb device is inserted.
	 * 2. if the usbhub is poweron before xhci_plat_probe, xhci will received
	 *    the usb interrupt and the dwc3 cannot suspend xhci device,
	 *    so keep host_suspend_capable to 0.
	 */
	if (!dwc->host_suspend_capable)
		return 0;

	dev_info(dwc->dev, "%s enter", __func__);
	/*
	 * We need make sure the children of the xhci device had been into
	 * suspend state, or we will suspend xhci device failed.
	 */
	ret = device_for_each_child(xhci, NULL, dwc3_host_suspend_child);
	if (ret) {
		dev_err(xhci, "failed to suspend xHCI children device %d\n", ret);
		return ret;
	}

	/*
	 * If the xhci device had been into suspend state, thus just return.
	 */
	if (pm_runtime_suspended(xhci))
		return 0;

	/* Suspend the xhci device synchronously. */
	ret = pm_runtime_put_sync(xhci);
	if (ret) {
		dev_err(xhci, "failed to suspend xHCI device\n");
		return ret;
	}

	return 0;
}

int dwc3_host_resume(struct dwc3 *dwc)
{
	struct device *xhci = &dwc->xhci->dev;
	int ret;

	if (!dwc->host_suspend_capable)
		return 0;
	dev_info(dwc->dev, "%s enter", __func__);
	/* Resume the xhci device synchronously. */
	ret = pm_runtime_get_sync(xhci);
	if (ret) {
		dev_err(xhci, "failed to resume xHCI device\n");
		return ret;
	}

	return 0;
}
