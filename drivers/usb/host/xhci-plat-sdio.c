/*
 * xhci-plat.c - xHCI host controller driver platform Bus Glue.
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com
 * Author: Sebastian Andrzej Siewior <bigeasy@linutronix.de>
 *
 * A lot of code borrowed from the Linux xHCI driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */

#include <linux/clk.h>
#include <linux/cpumask.h>
#include <linux/dma-mapping.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/usb/phy.h>
#include <linux/slab.h>
#include <linux/usb/xhci_pdriver.h>
#include <linux/acpi.h>
#include <linux/kthread.h>

#include "xhci-sdio.h"
#include "xhci-mvebu-sdio.h"

unsigned long arg_watchgog;
static struct hc_driver __read_mostly xhci_plat_hc_driver;

#ifdef CONFIG_SPRD_MARLIN3E_USB3_PHY
struct usb_hcd		*hcd_global = NULL;

extern void xhci_usb_host_enqueue_work(struct xhci_hcd	 *xhci);
extern void xhci_usb_host_dequeue_work(struct xhci_hcd	 *xhci);
extern int xhci_handle_event(struct xhci_hcd *xhci);
extern void dwc3_config_wake_lock(int enable);
extern int usb_hub_notify_apcp_register_status(int status);
extern void usbhub_cp_slp_ctl(int status);
extern int usbhub_get_apcp_sleep_status(void);
extern int usbhub_get_status(void);

irqreturn_t xhci_handle_irq(void)
{

	if(hcd_global != NULL)
		return xhci_irq(hcd_global);

	return IRQ_NONE;
}

EXPORT_SYMBOL(xhci_handle_irq);

/*
 * host controller died, register read returns 0xffffffff
 * Complete pending commands, mark them ABORTED.
 * URBs need to be given back as usb core might be waiting with device locks
 * held for the URBs to finish during device disconnect, blocking host remove.
 *
 * Call with xhci->lock held.
 * lock is relased and re-acquired while giving back urb.
 */
void xhci_hc_died(struct xhci_hcd *xhci)
{
	int i, j;

	if (xhci->xhc_state & XHCI_STATE_DYING)
		return;

	xhci_err(xhci, "xHCI host controller not responding, assume dead\n");
	xhci->xhc_state |= XHCI_STATE_DYING;

	xhci_cleanup_command_queue(xhci);
	mutex_lock(&xhci->mutex_sdio);
	/* return any pending urbs, remove may be waiting for them */
	for (i = 0; i < MAX_HC_SLOTS; i++) {
		if (!xhci->devs[i])
			continue;
		for (j = 0; j < MAX_HC_EP_LIMIT; j++)
			xhci_kill_endpoint_urbs(xhci, i, j);
	}
	mutex_unlock(&xhci->mutex_sdio);
	/* inform usb core hc died if PCI remove isn't already handling it */
	//if (!(xhci->xhc_state & XHCI_STATE_REMOVING))
	//	usb_hc_died(xhci_to_hcd(xhci));
}

void xhci_state_set_dying(void)
{
	struct xhci_hcd	*xhci = hcd_to_xhci(hcd_global);

	xhci_hc_died(xhci);
	xhci_dbg(xhci, "xHCI set dying, state %d",xhci->xhc_state);

}
EXPORT_SYMBOL(xhci_state_set_dying);

void xhci_usb_host_work(struct work_struct *work)
{
	struct xhci_hcd	*xhci = container_of(work,struct xhci_hcd, usb_int_work);
	struct usb_hcd *hcd = xhci_to_hcd(xhci);
	xhci_handle_event_data(hcd);
}

#ifdef EVENT_PACKER_HANDLE_IN_QUEUEWORK
void xhci_usb_event_work(struct work_struct *work)
{
	struct xhci_hcd	*xhci = container_of(work,struct xhci_hcd, usb_event_work);
	struct xhci_event_packer_list *temp_node;
	struct xhci_event_packer *event_packer;
	int cnt = 0;
	u32 event_addr;
	unsigned long   flags;

	while (1) {
		spin_lock_irqsave(&xhci->event_packer_lock, flags);

		if (list_empty(&xhci->event_packer_list)){
			spin_unlock_irqrestore(&xhci->event_packer_lock, flags);
			break;
		}

		temp_node = list_first_entry(&xhci->event_packer_list, struct xhci_event_packer_list, list);
		event_packer = &temp_node->event_packer;
		list_del(&temp_node->list);

		spin_unlock_irqrestore(&xhci->event_packer_lock, flags);
		cnt ++;

		event_addr = lookup_sdio_by_virtual(xhci, (unsigned long)xhci->event_ring->dequeue);
		xhci->event_handled_cnt = 0;
		/* lost event*/
		//while (event_addr != event_packer->ep_ctx.reserved[1]) {
		//	xhci_handle_event(xhci);
		//	event_addr = lookup_sdio_by_virtual(xhci, (u64)xhci->event_ring->dequeue);
		//}

		mutex_lock(&xhci->mutex_sdio);
		xhci_handle_event_packer(xhci,event_packer);
		mutex_unlock(&xhci->mutex_sdio);
		kfree(temp_node);
		xhci_dbg(xhci, "%s %d exit \n",__func__, cnt);
	}
}
#endif

static int marline_hub_thread(void *data)
{
	struct usb_hcd *hcd = (struct usb_hcd *)data;
	struct xhci_hcd *xhci = hcd_to_xhci(hcd);

	do {

		if (test_and_clear_bit(CHECK_UNLINK, &xhci->todo))
			xhci_usb_host_dequeue_work(xhci);

		if (test_and_clear_bit(CMD_TIMEOUT, &xhci->todo))
			xhci_handle_command_timeout((unsigned long)xhci);

		if (test_and_clear_bit(CMD_WATCHDOG, &xhci->todo))
			 xhci_stop_endpoint_command_watchdog(arg_watchgog);

		set_current_state(TASK_INTERRUPTIBLE);
		if ((!kthread_should_stop()) && (xhci->todo == 0)){
			schedule();
		}
		set_current_state(TASK_RUNNING);

	} while (!kthread_should_stop());

	return 0;
}

static int marlin3e_enqueue_thread(void *data)
{
	struct usb_hcd *hcd = (struct usb_hcd *)data;
	struct xhci_hcd *xhci = hcd_to_xhci(hcd);

	do {
		xhci_usb_host_enqueue_work(xhci);

		set_current_state(TASK_INTERRUPTIBLE);
		if (!kthread_should_stop() && list_empty(&xhci->urb_enqueue_list)) {
#ifdef CONFIG_USBHUB_LIGHT_SLEEP
			if (schedule_timeout(5 * HZ) == 0) {
				if(usbhub_get_status()) {
					/* have no transfer in 5s, relax waklock */
					dwc3_config_wake_lock(0);
					usbhub_cp_slp_ctl(1);
				}
				set_current_state(TASK_UNINTERRUPTIBLE);
				if (!kthread_should_stop()) {
					schedule();
					if(usbhub_get_status()) {
						/*transfer happened, stay awake again */
						usbhub_cp_slp_ctl(0);
						dwc3_config_wake_lock(1);
					}
				}
			}
#else
			schedule();
#endif
		}
		set_current_state(TASK_RUNNING);

	} while (!kthread_should_stop());

	return 0;
}
#endif
static int xhci_plat_setup(struct usb_hcd *hcd);
static int xhci_plat_start(struct usb_hcd *hcd);

static const struct xhci_driver_overrides xhci_plat_overrides __initconst= {
	.extra_priv_size = sizeof(struct xhci_hcd),
	.reset = xhci_plat_setup,
	.start = xhci_plat_start,
};

static void xhci_plat_quirks(struct device *dev, struct xhci_hcd *xhci)
{
	/*
	 * As of now platform drivers don't provide MSI support so we ensure
	 * here that the generic code does not try to make a pci_dev from our
	 * dev struct in order to setup MSI
	 */
	xhci->quirks |= XHCI_PLAT;
}

/* called during probe() after chip reset completes */
static int xhci_plat_setup(struct usb_hcd *hcd)
{
	return xhci_gen_setup(hcd, xhci_plat_quirks);
}

static int xhci_plat_start(struct usb_hcd *hcd)
{
	return xhci_run(hcd);
}

static int xhci_plat_probe(struct platform_device *pdev)
{
	struct device_node	*node = pdev->dev.of_node;
	struct usb_xhci_pdata	*pdata = dev_get_platdata(&pdev->dev);
	const struct hc_driver	*driver;
	struct xhci_hcd		*xhci;
	//struct resource         *res;
	struct usb_hcd		*hcd;
	struct clk              *clk;
	int			ret;
	int			irq;

	if (usb_disabled())
		return -ENODEV;
	dev_info(&pdev->dev, "%s enter", __func__);
	driver = &xhci_plat_hc_driver;
#ifndef CONFIG_SPRD_MARLIN3E_USB3_PHY
	irq = platform_get_irq(pdev, 0);
	if (irq < 0)
		return -ENODEV;
#else
	irq = 0;
#endif

	/* Try to set 64-bit DMA first */
	if (WARN_ON(!pdev->dev.dma_mask))
		/* Platform did not initialize dma_mask */
		ret = dma_coerce_mask_and_coherent(&pdev->dev,
						   DMA_BIT_MASK(64));
	else
		ret = dma_set_mask_and_coherent(&pdev->dev, DMA_BIT_MASK(64));

	/* If seting 64-bit DMA mask fails, fall back to 32-bit DMA mask */
	if (ret) {
		printk("xhci_plat_probe dma_set_mask faild\n");
		ret = dma_set_mask_and_coherent(&pdev->dev, DMA_BIT_MASK(32));
		if (ret)
			return ret;
	}

	pm_runtime_set_active(&pdev->dev);
	//pm_runtime_enable(&pdev->dev);

	/* keep xhci resume state to check wether the usb device is inserted */
	pm_runtime_disable(&pdev->dev);
	pm_runtime_get_noresume(&pdev->dev);
	hcd = usb_create_hcd(driver, &pdev->dev, dev_name(&pdev->dev));
	if (!hcd) {
		ret = -ENOMEM;
		goto disable_runtime;
	}
	xhci = hcd_to_xhci(hcd);
#ifndef CONFIG_SPRD_MARLIN3E_USB3_PHY
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	hcd->regs = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(hcd->regs)) {
		ret = PTR_ERR(hcd->regs);
		goto put_hcd;
	}

	hcd->rsrc_start = res->start;
	hcd->rsrc_len = resource_size(res);
#else
	hcd->regs = (void *)MARLIN3E_USB_BASE;
	xhci->sdio_hub_thread =
				kthread_create(marline_hub_thread, hcd, "marline_hub_thread");

	hcd_global = hcd;
	xhci->sdio_enqueue_thread =
				kthread_create(marlin3e_enqueue_thread, hcd, "marlin3e_enqueue_thread");

	xhci->sdio_wq = create_workqueue("marlin3e_usb_host_wq");
	INIT_WORK(&xhci->usb_int_work, xhci_usb_host_work);

#ifdef EVENT_PACKER_HANDLE_IN_QUEUEWORK
	INIT_WORK(&xhci->usb_event_work, xhci_usb_event_work);
#endif
#endif

	/*
	 * Not all platforms have a clk so it is not an error if the
	 * clock does not exists.
	 */
	clk = devm_clk_get(&pdev->dev, NULL);
	if (!IS_ERR(clk)) {
		ret = clk_prepare_enable(clk);
		if (ret)
			goto put_hcd;
	}

	if (of_device_is_compatible(pdev->dev.of_node,
				    "marvell,armada-375-xhci") ||
	    of_device_is_compatible(pdev->dev.of_node,
				    "marvell,armada-380-xhci")) {
		ret = xhci_mvebu_mbus_init_quirk(pdev);
		if (ret)
			goto disable_clk;
	}

	device_wakeup_enable(hcd->self.controller);

	xhci->clk = clk;
	xhci->main_hcd = hcd;
	xhci->shared_hcd = usb_create_shared_hcd(driver, &pdev->dev,
			dev_name(&pdev->dev), hcd);
	if (!xhci->shared_hcd) {
		ret = -ENOMEM;
		goto disable_clk;
	}

	/*
	 * Set the xHCI pointer before xhci_plat_setup() (aka hcd_driver.reset)
	 * is called by usb_add_hcd().
	 */
	*((struct xhci_hcd **) xhci->shared_hcd->hcd_priv) = xhci;

	if (HCC_MAX_PSA(xhci->hcc_params) >= 4)
		xhci->shared_hcd->can_do_streams = 1;

	hcd->usb_phy = devm_usb_get_phy_by_phandle(&pdev->dev, "usb-phy", 0);
	if (IS_ERR(hcd->usb_phy)) {
		ret = PTR_ERR(hcd->usb_phy);
		if (ret == -EPROBE_DEFER)
			goto put_usb3_hcd;
		hcd->usb_phy = NULL;
	} else {
		ret = usb_phy_init(hcd->usb_phy);
		if (ret)
			goto put_usb3_hcd;
	}

	if (!hcd->usb_phy) {
		hcd->usb_phy = devm_usb_get_phy_by_phandle(pdev->dev.parent,
							   "usb-phy", 0);
		if (IS_ERR(hcd->usb_phy)) {
			ret = PTR_ERR(hcd->usb_phy);
			if (ret == -EPROBE_DEFER)
				goto put_usb3_hcd;
			hcd->usb_phy = NULL;
		} else {
			ret = usb_phy_init(hcd->usb_phy);
			if (ret)
				goto put_usb3_hcd;
		}
	}

	ret = usb_add_hcd(hcd, irq, IRQF_SHARED);
	if (ret)
		goto disable_usb_phy;

	ret = usb_add_hcd(xhci->shared_hcd, irq, IRQF_SHARED);
	if (ret)
		goto dealloc_usb2_hcd;

	if ((node && of_property_read_bool(node, "usb3-lpm-capable")) ||
			(pdata && pdata->usb3_lpm_capable))
		xhci->quirks |= XHCI_LPM_SUPPORT;

	if (pdata && pdata->usb3_slow_suspend)
		xhci->quirks |= XHCI_SLOW_SUSPEND;

	//pm_runtime_set_active(&pdev->dev);
	//pm_runtime_enable(&pdev->dev);

	device_enable_async_suspend(&pdev->dev);
	pm_runtime_put_noidle(&pdev->dev);
	wake_up_process(xhci->sdio_enqueue_thread);

	xhci->bulk_trans_buffer = kmalloc(TRB_MAX_BUFF_SIZE, GFP_KERNEL);
	if (xhci->bulk_trans_buffer == NULL)
		goto dealloc_usb2_hcd;

	xhci_dbg(xhci, "%s exit", __func__);
	return 0;


dealloc_usb2_hcd:
	usb_remove_hcd(hcd);

disable_usb_phy:
	usb_phy_shutdown(hcd->usb_phy);

put_usb3_hcd:
	usb_put_hcd(xhci->shared_hcd);

disable_clk:
	if (!IS_ERR(clk))
		clk_disable_unprepare(clk);

put_hcd:
	usb_put_hcd(hcd);

disable_runtime:
	pm_runtime_put_noidle(&pdev->dev);
	pm_runtime_disable(&pdev->dev);

	return ret;
}

static int xhci_plat_remove(struct platform_device *dev)
{
	struct usb_hcd	*hcd = platform_get_drvdata(dev);
	struct xhci_hcd	*xhci = hcd_to_xhci(hcd);
	struct clk *clk = xhci->clk;

	printk("%s : %d \n", __func__, __LINE__);
	xhci->xhc_state |= XHCI_STATE_REMOVING;
	usb_remove_hcd(xhci->shared_hcd);
	usb_phy_shutdown(hcd->usb_phy);

	usb_remove_hcd(hcd);
	usb_put_hcd(xhci->shared_hcd);

	if (!IS_ERR(clk))
		clk_disable_unprepare(clk);
	usb_put_hcd(hcd);

	if (xhci->bulk_trans_buffer)
		kfree(xhci->bulk_trans_buffer);

#ifdef CONFIG_SPRD_MARLIN3E_USB3_PHY
	if (xhci->sdio_hub_thread)
		kthread_stop(xhci->sdio_hub_thread);
	if (xhci->sdio_enqueue_thread)
		kthread_stop(xhci->sdio_enqueue_thread);
	if (xhci->sdio_wq)
		destroy_workqueue(xhci->sdio_wq);
#endif
	pm_runtime_set_suspended(&dev->dev);
	pm_runtime_disable(&dev->dev);
	return 0;
}

#ifdef CONFIG_PM_SLEEP
static int xhci_plat_suspend(struct device *dev)
{
	dev_info(dev, "%s ", __func__);
	return 0;
}

static int xhci_plat_resume(struct device *dev)
{
	dev_info(dev, "%s ", __func__);
	return 0;
}
#endif /* CONFIG_PM_SLEEP */

#ifdef CONFIG_PM
static int xhci_plat_runtime_suspend(struct device *dev)
{
	struct usb_hcd  *hcd = dev_get_drvdata(dev);
	struct xhci_hcd *xhci = hcd_to_xhci(hcd);

	dev_info(dev, "%s ", __func__);
	return xhci_suspend(xhci, device_may_wakeup(dev));
}

static int xhci_plat_runtime_resume(struct device *dev)
{
	struct usb_hcd  *hcd = dev_get_drvdata(dev);
	struct xhci_hcd *xhci = hcd_to_xhci(hcd);

	dev_info(dev, "%s ", __func__);
	return xhci_resume(xhci, 0);
}

static int xhci_plat_runtime_idle(struct device *dev)
{
	dev_info(dev, "%s ", __func__);
	return 0;
}
#endif /* CONFIG_PM */

static const struct dev_pm_ops xhci_plat_pm_ops = {
	SET_SYSTEM_SLEEP_PM_OPS(xhci_plat_suspend, xhci_plat_resume)

	SET_RUNTIME_PM_OPS(
		xhci_plat_runtime_suspend,
		xhci_plat_runtime_resume,
		xhci_plat_runtime_idle)
};

#ifdef CONFIG_OF
static const struct of_device_id usb_xhci_of_match[] = {
	{ .compatible = "generic-xhci" },
	{ .compatible = "xhci-platform" },
	{ .compatible = "marvell,armada-375-xhci"},
	{ .compatible = "marvell,armada-380-xhci"},
	{ .compatible = "renesas,xhci-r8a7790"},
	{ .compatible = "renesas,xhci-r8a7791"},
	{ },
};
MODULE_DEVICE_TABLE(of, usb_xhci_of_match);
#endif

static const struct acpi_device_id usb_xhci_acpi_match[] = {
	/* XHCI-compliant USB Controller */
	{ "PNP0D10", },
	{ }
};
MODULE_DEVICE_TABLE(acpi, usb_xhci_acpi_match);

static struct platform_driver usb_xhci_driver = {
	.probe	= xhci_plat_probe,
	.remove	= xhci_plat_remove,
	.driver	= {
		.name = "xhci-hcd",
		.pm = &xhci_plat_pm_ops,
		.of_match_table = of_match_ptr(usb_xhci_of_match),
		.acpi_match_table = ACPI_PTR(usb_xhci_acpi_match),
	},
};
MODULE_ALIAS("platform:xhci-hcd");

static int __init xhci_plat_init(void)
{
	xhci_init_driver(&xhci_plat_hc_driver, &xhci_plat_overrides);
	return platform_driver_register(&usb_xhci_driver);
}
module_init(xhci_plat_init);

static void __exit xhci_plat_exit(void)
{
	platform_driver_unregister(&usb_xhci_driver);
}
module_exit(xhci_plat_exit);


MODULE_DESCRIPTION("xHCI Platform Host Controller Driver");
MODULE_LICENSE("GPL");
