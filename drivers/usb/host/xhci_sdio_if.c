#include <linux/module.h>
#include <linux/debugfs.h>
#include <linux/file.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/vmalloc.h>

#include <misc/wcn_bus.h>

#include <linux/proc_fs.h>

#include "xhci-sdio.h"

#define USB_HUB_TX 7
#define USB_HUB_RX 21

#define USB_HUB_OUT 1
#define USB_HUB_IN 0

#define USBHUB_PDHLEN   sizeof(transfer_head_t)

#define ISOCH_RETRY_CNT     3
#define M3E_AP_CHECK_RETRY_CNT     2
#define ISOCH_WAIT_TIME      1000
#define M3E_AP_CHECK_WAIT_TIME      500

#if 0
struct usb_hubpd{
	unsigned short type;
	unsigned short len;
};
#endif
struct xhci_ring_m3e {
	u32 first_seg;
	u32 enqueue;
	u32 enq_seg;
	u32 dequeue;
	u32 deq_seg;
	u64 td_list;
	u32	cycle_state;
	u32	num_segs;
};
struct isoch_ep {
	u16 slot_id;
	u16 ep_index;
	u32 start_frame;
	u32 speed;
	u32 interval;
	struct xhci_ring_m3e ep_ring;
};

struct isoch_dbg_info {
	int cnt;
	int ret;
	int flag;
	int addr;
	int data_buffer;
	int transfer_trb;
	int trb_comp_code;
};

struct xhci_ctrl {
	u32 hccr;
    u32 hcor;
    u32 dba;
    u32 run_regs;
};

struct isoch_data_buffer {
	int status;
	int addr;
	u16 slot_id;
	u16 ep_index;
	int len;
};

static int isoch_ep_num = 0;

extern struct usb_hcd		*hcd_global;
extern void wcn_usb_switch_sdio_triger(int s_triger);

struct completion m3e_ap_check_complete;
struct mutex check_m3e_ap_mutex;

enum xhci_inform_cmd_type{
	SET_MEM_INFO = 0,
    SET_FLV_PACKAGE_INFO   = 1,
    SET_USBHUB_STATUS = 2,
    SET_ISOCH_EP = 3,
	CLE_ISOCH_EP = 4,
	CLE_MEM_INFO = 5,
	CHK_AP_READY = 6,
};

int usb_hub_tx_pop(int channel, struct mbuf_t *head,
		   struct mbuf_t *tail, int num)
{
	int i;
	struct mbuf_t *mbuf_node;

	pr_info("%s channel:%d head:%p tail:%p num:%d\n",
		__func__, channel, head, tail, num);

	for (i = 0, mbuf_node = head; i < num;
	     i++, mbuf_node = mbuf_node->next) {
		kfree(head->buf);
		head->buf = NULL;
	}

	sprdwcn_bus_list_free(channel, head, tail, num);

	return 0;
}

int usb_hub_rx_pop(int channel, struct mbuf_t *head,
		   struct mbuf_t *tail, int num)
{
	int i, ret;
	int main_type;
	int sub_type;
	transfer_head_t *transfer_head;
	struct xhci_event_packer *event_packer;
	struct mbuf_t *mbuf_node;
	unsigned char *tmp_buf;
	struct isoch_data_head *isoch_data_head;
	struct xhci_hcd *xhci;
	struct xhci_ring *ep_ring;
	int slot_id;
	int ep_index;
	struct urb *urb;
	struct usb_iso_packet_descriptor *frame;
	u32 len=0;
	unsigned long idx = 0;
	char *frame_buf, *iso_buf;
	unsigned long flags;
	int t_len = 0;
	char *e_buf;
	struct xhci_ring *event_ring;
	u32 event_start,event_end,event_addr;

	pr_info("<== %s entry!channel:%d num:%d len:%d\n", __func__,channel, num, head->len);

	mbuf_node = head;
	for(i=0; i<num; i++) {
		tmp_buf = mbuf_node->buf + PUB_HEAD_RSV;

		do {
			transfer_head = (transfer_head_t *)tmp_buf;
			main_type = transfer_head->sdio_ops_type;

			switch (main_type) {
				case XHCI_ISOCH_TX:
					xhci = hcd_to_xhci(hcd_global);
					isoch_data_head = (struct isoch_data_head *)tmp_buf;
					sub_type = transfer_head->sub_type;
					slot_id = isoch_data_head->slot_id;
					ep_index = isoch_data_head->ep_index;
					ep_ring = xhci->devs[slot_id]->eps[ep_index].ring;

					spin_lock_irqsave(&xhci->urb_lock, flags);

					switch(sub_type) {
						case ISOCH_DATA_S:
							if ((ep_ring != NULL) && (!list_empty(&ep_ring->td_list))) {
								urb = list_first_entry(&ep_ring->td_list, struct urb, urb_list);
								idx = (unsigned long)urb->hcpriv;
								frame = &urb->iso_frame_desc[idx];
								pr_info("******isoch recv S %d %d len %d  idx:%ld\n", slot_id,
								ep_index, isoch_data_head->len, idx);

								if(frame->length >= isoch_data_head->len)
									len = isoch_data_head->len & (~(0xff << 24));
								else
									len = frame->length;

								frame_buf = (char *)(urb->transfer_buffer + frame->offset);
								iso_buf = (char*)(tmp_buf + sizeof(struct isoch_data_head));
								memcpy(frame_buf,iso_buf,len);

								urb->actual_length += len;
								frame->actual_length = len;
								ep_ring->deq_updates = 1;
							} else
								pr_err("isoch no urb drop frame S !!!\n");
							break;
						case ISOCH_DATA_E:
							if ((ep_ring != NULL) && (!list_empty(&ep_ring->td_list)) && ep_ring->deq_updates) {
								urb = list_first_entry(&ep_ring->td_list, struct urb, urb_list);
								idx = (unsigned long)urb->hcpriv;
								frame = &urb->iso_frame_desc[idx];

								pr_info("******isoch recv E %d %d len %d  idx:%ld\n",
									slot_id, ep_index, isoch_data_head->len, idx);

								frame_buf = (char *)(urb->transfer_buffer + frame->offset);
								iso_buf = (char*)(tmp_buf +
									sizeof(struct isoch_data_head));
								e_buf = (char*)(tmp_buf +
									sizeof(struct isoch_data_head)+ISOCH_BUFF_E_SIZE - ISOCH_HEAD_OFFSET);

								len = frame->actual_length;
								frame_buf = (char *)(frame_buf + len);

								if(frame->length >= isoch_data_head->len + len)
									t_len = isoch_data_head->len;
								else if (frame->length > len)
									t_len = frame->length - len;
								else
									t_len = 0;

								if(t_len >= ISOCH_HEAD_OFFSET) {
									memcpy(frame_buf,e_buf,ISOCH_HEAD_OFFSET);  //32
									frame_buf = (char *)(frame_buf + ISOCH_HEAD_OFFSET);
									memcpy(frame_buf,iso_buf,t_len - ISOCH_HEAD_OFFSET); //1468 max
								} else
									memcpy(frame_buf,e_buf,ISOCH_HEAD_OFFSET);

								urb->actual_length += t_len;
								frame->actual_length += t_len;
								frame->status = 0;
								if (idx == 0)
									urb->start_frame = isoch_data_head->start_frame;
								xhci->start_frame = isoch_data_head->start_frame;
								idx ++;
								urb->hcpriv = (void *)idx;

								if ((idx == urb->number_of_packets) && (!urb->unlinked)) {
									pr_info("xhci isoch %d urb %p ,len %d idx %ld!!!\n",
										ep_ring->num_trbs_free_temp, urb, urb->actual_length, idx);
									list_del(&urb->urb_list);
									ep_ring->num_trbs_free_temp --;
									spin_unlock_irqrestore(&xhci->urb_lock, flags);
									usb_hcd_giveback_urb(bus_to_hcd(urb->dev->bus), urb, 0);
									spin_lock_irqsave(&xhci->urb_lock, flags);
								}
								ep_ring->deq_updates = 0;
							} else
								pr_err("isoch no urb drop frame E!!!\n");
							len = 0;

							break;
						case ISOCH_DATA_A:
							if ((ep_ring != NULL) && (!list_empty(&ep_ring->td_list))) {
								urb = list_first_entry(&ep_ring->td_list, struct urb, urb_list);
								idx = (unsigned long)urb->hcpriv;
								frame = &urb->iso_frame_desc[idx];

								pr_info("******isoch recv A %d %d len %d idx:%ld\n",
									slot_id, ep_index, isoch_data_head->len, idx);

								if(frame->length >= isoch_data_head->len)
									len = isoch_data_head->len;
								else
									len = frame->length;

								frame_buf = (char *)(urb->transfer_buffer + frame->offset);
								iso_buf = (char*)(tmp_buf +
									sizeof(struct isoch_data_head));

								if (len > 0)
									memcpy(frame_buf,iso_buf,len);

								urb->actual_length += len;
								frame->actual_length = len;

								frame->status = 0;
								if (idx == 0)
									urb->start_frame = isoch_data_head->start_frame;
								xhci->start_frame = isoch_data_head->start_frame;
								idx ++;
								urb->hcpriv = (void *)idx;
								if ((idx == urb->number_of_packets) && (!urb->unlinked)) {
									pr_info("xhci isoch %d urb %p ,len %d idx %ld!!!\n",
										ep_ring->num_trbs_free_temp, urb, urb->actual_length, idx);
									list_del(&urb->urb_list);
									ep_ring->num_trbs_free_temp --;
									spin_unlock_irqrestore(&xhci->urb_lock, flags);
									usb_hcd_giveback_urb(bus_to_hcd(urb->dev->bus), urb, 0);
									spin_lock_irqsave(&xhci->urb_lock, flags);
								}
								ep_ring->deq_updates = 0;
							}else
								pr_err("isoch no urb drop frame A !!!\n");

							break;
						default:
							pr_err("%s wrong transfer type !!!transfer main type:%d,sub type:%d\n",__func__,
															main_type,sub_type);
							break;
					}
					spin_unlock_irqrestore(&xhci->urb_lock, flags);
					break;
				case XHCI_EVENT_TX:
					if (CHECK_AP_READY_ACK == transfer_head->sub_type) {
						usb_hub_m3e_ap_ready_set((struct ap_status_packer *)tmp_buf);
						break;
					}

					xhci = hcd_to_xhci(hcd_global);
					event_ring = xhci->event_ring;
#if 0
					pr_info("%s transfer main_type:%d,sub_type:%d,index:%d\n",__func__,transfer_head->sdio_ops_type,
														transfer_head->sub_type,
														((struct xhci_event_packer*)transfer_head)->ep_ctx.reserved[2]);
#endif
					switch(transfer_head->sub_type) {
						case ISOCH_STATUS:
							usb_hub_isoch_set_status((struct isoch_status_packer *)tmp_buf);
							break;
						case EVENT_TRB:
							event_packer = (struct xhci_event_packer *)tmp_buf;
							event_addr = event_packer->ep_ctx.reserved[1];
							event_start = (u32)event_ring->first_seg->sdio_addr;
							event_end = (u32)event_ring->last_seg->sdio_addr + TRB_SEGMENT_SIZE;

							if ( (event_addr >= event_start) && (event_addr < event_end))
								xhci_handle_event_data_packer(event_packer);
							else
								pr_err("%s xhci wrong event packer data !!!\n",__func__);
							break;
						default:
							pr_err("%s wrong transfer type !!!transfer main type:%d,sub type:%d\n",__func__,
													main_type,transfer_head->sub_type);
							break;
					}

					break;
				default:
					pr_err("%s wrong transfer type !!!transfer main type:%d\n",__func__,
															main_type);
					break;

			}

			tmp_buf += transfer_head->len;
		}while(transfer_head->list_head[0]);

		mbuf_node = mbuf_node->next;
	}
	ret = sprdwcn_bus_push_list(channel, head, tail, num);
	if (ret < 0) {
		pr_err("%s, SDIO error=%d\n",__func__, ret);
		return ret;
	}

	return 0;
}

struct mchn_ops_t usb_hub_ops[2] = {
	{
		.channel = USB_HUB_TX,
		.inout = USB_HUB_OUT,
		.pool_size = 16,
		.pop_link = usb_hub_tx_pop,
	},

	{
		.channel = USB_HUB_RX,
		.inout = USB_HUB_IN,
		.pool_size = 128,
		.pop_link = usb_hub_rx_pop,
	},
};

#if 0
void usb_hub_prepare_flv_pack(struct xhci_flv_package *flv_pack)
{
	struct mbuf_t *head, *tail;
    struct mbuf_t *mbuf_node;
	int num = 1, i;
    u32     alloc_len;
    u32     node_num        = 0;
    u32     package_len     = 0;
    struct xhci_flv_package  *flv_node;
	//struct usb_hubpd *usb_head;
    transfer_head_t *transfer_head;
    unsigned char            *p_pos;

    flv_node    = flv_pack;
    for (i = 0; NULL != flv_node; i++) {
        node_num ++;
        package_len += flv_node->flv->len;
        flv_node = flv_node->next_flv;
    }

    WARN_ON(node_num > 11);

    /* sizeof(u32)  is  the memory of package_len */
	alloc_len = sizeof(u32) + node_num*sizeof(struct xhci_sdio_flv) + package_len;

	pr_info("==> xhci %s alloc_len=%d, node_num=%d, package_len =%d\n",
        __func__, alloc_len, node_num, package_len);

	if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num)) {
		mbuf_node = head;
		for (i = 0; i < num; i++) {
			mbuf_node->buf = kzalloc(alloc_len + PUB_HEAD_RSV + USBHUB_PDHLEN,
				GFP_KERNEL);

			transfer_head = (transfer_head_t *)(mbuf_node->buf + PUB_HEAD_RSV);
			transfer_head->sdio_ops_type = XHCI_RX;
			transfer_head->sub_type  = SET_FLV_PACKAGE_INFO;
			transfer_head->len   = USBHUB_PDHLEN;

            p_pos   =  mbuf_node->buf + PUB_HEAD_RSV + USBHUB_PDHLEN;
            *(u32*)p_pos = alloc_len;
            p_pos   += sizeof(u32);

            /* the flv_pack->package_len must be less 840 */
            flv_node    = flv_pack;
            for (i = 0; NULL != flv_node; i++) {
                memcpy(p_pos, flv_node->flv, sizeof(struct xhci_sdio_flv) + flv_node->flv->len);
                p_pos += sizeof(struct xhci_sdio_flv) + flv_node->flv->len;
                flv_node = flv_node->next_flv;
            }

			mbuf_node->len  = alloc_len + USBHUB_PDHLEN;

			if ((i+1) < num)
				mbuf_node = mbuf_node->next;
			else
				mbuf_node->next = NULL;

		}

		sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);

	}else{
		pr_info("%s:%d sprdwcn_bus_list_alloc fail\n", __func__, __LINE__);
	}

}
#endif

void usb_hub_init_xhci_mem(struct xhci_mem_info *mem_info)
{
	struct mbuf_t *head, *tail, *mbuf_node;
	int num = 1;
	int i;
	//struct usb_hubpd *usb_head;
	transfer_head_t *transfer_head;
	int ret;

	pr_info("==> %s entry! ", __func__);
	if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num))  {
		mbuf_node = head;
		for (i = 0; i < num; i++) {
			mbuf_node->buf = kzalloc(sizeof(*mem_info) + PUB_HEAD_RSV + USBHUB_PDHLEN, GFP_KERNEL);
			memcpy(mbuf_node->buf + PUB_HEAD_RSV + USBHUB_PDHLEN, mem_info, sizeof(*mem_info));
			transfer_head = (transfer_head_t *)(mbuf_node->buf + PUB_HEAD_RSV);
			transfer_head->sdio_ops_type = XHCI_RX;
			transfer_head->sub_type = SET_MEM_INFO;
			transfer_head->len = sizeof(*mem_info);
			mbuf_node->len = sizeof(*mem_info)+USBHUB_PDHLEN;
			if ((i+1) < num)
				mbuf_node = mbuf_node->next;
			else
				mbuf_node->next = NULL;
		}

		ret = sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);
		if (ret < 0) {
			pr_err("%s, SDIO error=%d\n",__func__, ret);
		}

	}
}

void usb_hub_clean_xhci_mem(struct xhci_mem_info *mem_info)
{
	struct mbuf_t *head, *tail, *mbuf_node;
	int num = 1;
	int i;
	//struct usb_hubpd *usb_head;
	transfer_head_t *transfer_head;
	int ret;

	pr_info("==> %s entry! ", __func__);
	if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num))  {
		mbuf_node = head;
		for (i = 0; i < num; i++) {
			mbuf_node->buf = kzalloc(sizeof(*mem_info) + PUB_HEAD_RSV + USBHUB_PDHLEN, GFP_KERNEL);
			memcpy(mbuf_node->buf + PUB_HEAD_RSV + USBHUB_PDHLEN, mem_info, sizeof(*mem_info));
			transfer_head = (transfer_head_t *)(mbuf_node->buf + PUB_HEAD_RSV);
			transfer_head->sdio_ops_type = XHCI_RX;
			transfer_head->sub_type = CLE_MEM_INFO;
			transfer_head->len = sizeof(*mem_info);
			mbuf_node->len = sizeof(*mem_info)+USBHUB_PDHLEN;
			if ((i+1) < num)
				mbuf_node = mbuf_node->next;
			else
				mbuf_node->next = NULL;
		}

		ret = sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);
		if (ret < 0) {
			pr_err("%s, SDIO error=%d\n",__func__, ret);
		}

	}
}

int usb_hub_isoch_set_status(struct isoch_status_packer *isoch_data_packer)
{
	struct isoch_ep_list *pos, *next;
	struct xhci_hcd *xhci = hcd_to_xhci(hcd_global);
#if 0
	if(ISOCH_INITED != isoch_data_packer->isoch_status &&
#endif
	if(ISOCH_DESTROYED != isoch_data_packer->isoch_status)
		return -1;

	pr_info(" ===> usb_hub_isoch_set_ep_status entry! \n");

	list_for_each_entry_safe (pos, next, &xhci->ep_list_head, list) {
		if ((pos->ep_info.slot_id == isoch_data_packer->slot_id) &&
			(pos->ep_info.ep_index == isoch_data_packer->ep_index)) {
			pos->ep_info.isoch_status = isoch_data_packer->isoch_status;
			pr_info(" %s ,isoch_status:0x%x, slot_id:%d, index:%d,ep_addr:0x%lx\n", __func__,
					isoch_data_packer->isoch_status,
					pos->ep_info.slot_id,
					pos->ep_info.ep_index,
					(unsigned long)&pos->ep_info);
#if 0
			if(ISOCH_INITED == isoch_data_packer->isoch_status) {
				//complete(&xhci->isoch_init_complete);
			}
			else
#endif
			if(ISOCH_DESTROYED == isoch_data_packer->isoch_status)
				complete(&xhci->isoch_destroy_complete);

			return 0;

		}
	}

	pr_err(" %s no find ep err !! isoch_status:0x%x, slot_id:%d, index:%d,ep_addr:0x%lx\n", __func__,
			isoch_data_packer->isoch_status,
			pos->ep_info.slot_id,
			pos->ep_info.ep_index,
			(unsigned long)&pos->ep_info);

	return -2;
}

int usb_hub_m3e_ap_ready_set(struct ap_status_packer *m3e_check_packer)
{
	if(M3E_AP_READY != m3e_check_packer->sleep_status) {
		pr_err("%s m3e ap status wrong!!!\n", __func__);
		return -1;
	}

	complete(&m3e_ap_check_complete);

	return 0;
}

int usb_hub_init_isoch_ep(struct isoch_ep_info *ep_info)
{
	struct mbuf_t *head, *tail, *mbuf_node;
	int num = 1;
	int i;
	//struct usb_hubpd *usb_head;
	transfer_head_t *transfer_head;
	int ret;

	pr_info("xhci ******==> %s entry! ", __func__);

	/* the max num of isoch ep supported is 2 */
	if (isoch_ep_num >= 2) {
		pr_info("xhci %s error, ISOCH EP LIMIT TO 2 !!!", __func__);
		return -1;
	}

	if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num))  {
		mbuf_node = head;
		for (i = 0; i < num; i++) {
			mbuf_node->buf = kzalloc(sizeof(*ep_info) + PUB_HEAD_RSV + USBHUB_PDHLEN, GFP_KERNEL);
			memcpy(mbuf_node->buf + PUB_HEAD_RSV + USBHUB_PDHLEN, ep_info, sizeof(*ep_info));
			transfer_head = (transfer_head_t *)(mbuf_node->buf + PUB_HEAD_RSV);
			transfer_head->sdio_ops_type = XHCI_RX;
			transfer_head->sub_type = SET_ISOCH_EP;
			transfer_head->len = sizeof(*ep_info);
			mbuf_node->len = sizeof(*ep_info)+USBHUB_PDHLEN;
			if ((i+1) < num)
				mbuf_node = mbuf_node->next;
			else
				mbuf_node->next = NULL;
		}

		ret = sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);
		if (ret < 0) {
			pr_err("%s, SDIO error=%d\n",__func__, ret);
			return ret;
		}
	}

	/* change sdio rx to pool mode */
	if (isoch_ep_num == 0)
		wcn_usb_switch_sdio_triger(1);
	isoch_ep_num ++;

	return 0;
}

int usb_hub_destory_isoch_ep(struct isoch_ep_info *ep_info)
{
	struct mbuf_t *head, *tail, *mbuf_node;
	int num = 1;
	int i;
	int j;
	//struct usb_hubpd *usb_head;
	transfer_head_t *transfer_head;
	struct xhci_hcd *xhci = hcd_to_xhci(hcd_global);
	int ret;

	mutex_lock(&xhci->isoch_destroy_mutex);
	pr_info("xhci ******==> %s entry! ", __func__);
	for(j = 0; j <= ISOCH_RETRY_CNT; j++ ) {
		if(ep_info->isoch_status == ISOCH_DESTROYED) {
			pr_info("%s destroy success !!status:0x%x,slot_id:%d,index:%d,ep_addr:0x%lx",__func__,
			ep_info->isoch_status,
			ep_info->slot_id,
			ep_info->ep_index,
			(unsigned long)ep_info);
			isoch_ep_num --;
			/* change sdio rx to pool mode */
			if (isoch_ep_num <= 0)
				wcn_usb_switch_sdio_triger(0);
			mutex_unlock(&xhci->isoch_destroy_mutex);
			return 0;
		} else {
				pr_info("%s destroy still fail !!status:0x%x,slot_id:%d,index:%d,ep_addr:0x%lx,cnt:%d",
					__func__,
					ep_info->isoch_status,
					ep_info->slot_id,
					ep_info->ep_index,
					(unsigned long)ep_info,
					j);

				if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num))  {
					mbuf_node = head;
					for (i = 0; i < num; i++) {
						mbuf_node->buf = kzalloc(sizeof(*ep_info) + PUB_HEAD_RSV + USBHUB_PDHLEN, GFP_KERNEL);
						memcpy(mbuf_node->buf + PUB_HEAD_RSV + USBHUB_PDHLEN, ep_info, sizeof(*ep_info));
						transfer_head = (transfer_head_t *)(mbuf_node->buf + PUB_HEAD_RSV);
						transfer_head->sdio_ops_type = XHCI_RX;
						transfer_head->sub_type = CLE_ISOCH_EP;
						transfer_head->len = sizeof(*ep_info);
						mbuf_node->len = sizeof(*ep_info)+USBHUB_PDHLEN;
						if ((i+1) < num)
							mbuf_node = mbuf_node->next;
						else
							mbuf_node->next = NULL;
					}

					reinit_completion(&xhci->isoch_destroy_complete);
					ret = sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);
					if (ret < 0) {
						pr_err("%s, SDIO error=%d\n",__func__, ret);
						mutex_unlock(&xhci->isoch_destroy_mutex);
						return ret;
					}

				}

				if (wait_for_completion_timeout(&xhci->isoch_destroy_complete,
						msecs_to_jiffies(ISOCH_WAIT_TIME)) == 0) {
					pr_info("%s timed out waiting for isoch destroy complete!cnt:%d\n", __func__,j);
				}
			}
	}

	pr_err("xhci %s isoc ep destroy no ack from m3e !!! total try count:%d", __func__,j);
	mutex_unlock(&xhci->isoch_destroy_mutex);
	return -2;
}

int usb_hub_m3e_ap_ready_check(void)
{
	struct mbuf_t *head, *tail, *mbuf_node;
	int num = 1;
	int i;
	int j;
	transfer_head_t *transfer_head;
	int ret;

	pr_info("%s entry to check m3e ap ready!", __func__);

	mutex_lock(&check_m3e_ap_mutex);
	for(j = 0; j < M3E_AP_CHECK_RETRY_CNT; j++ ) {
		if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num))  {
			mbuf_node = head;
			for (i = 0; i < num; i++) {
				mbuf_node->buf = kzalloc(PUB_HEAD_RSV + USBHUB_PDHLEN, GFP_KERNEL);
				transfer_head = (transfer_head_t *)(mbuf_node->buf + PUB_HEAD_RSV);
				transfer_head->sdio_ops_type = XHCI_RX;
				transfer_head->sub_type = CHK_AP_READY;
				transfer_head->len = 0;
				mbuf_node->len = USBHUB_PDHLEN;
				if ((i+1) < num)
					mbuf_node = mbuf_node->next;
				else
					mbuf_node->next = NULL;
			}

			reinit_completion(&m3e_ap_check_complete);
			ret = sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);
			if (ret < 0) {
				pr_err("%s, SDIO error=%d\n",__func__, ret);
				mutex_unlock(&check_m3e_ap_mutex);
				return ret;
			}
		}

		if (wait_for_completion_timeout(&m3e_ap_check_complete,
				msecs_to_jiffies(M3E_AP_CHECK_WAIT_TIME)) == 0) {
			pr_info("%s timed out waiting for check marlin3e ap complete!cnt:%d\n", __func__,j);
		} else {
			pr_info("%s marlin3e ap ready!\n", __func__);
			mutex_unlock(&check_m3e_ap_mutex);
			return 0;
		}
	}

	pr_err("xhci %s check marlin3e ap no ack from m3e !!! total try count:%d", __func__,j);
	mutex_unlock(&check_m3e_ap_mutex);
	return -2;
}
EXPORT_SYMBOL(usb_hub_m3e_ap_ready_check);

/* status : 1 usbhub active. 0 usbhub died*/
int usb_hub_notify_apcp_register_status(int status)
{
	struct mbuf_t *head, *tail, *mbuf_node;
	int num = 1;
	int i;
	//struct usb_hubpd *usb_head;
	transfer_head_t *transfer_head;
	int ret;

	pr_info("==> %s entry! status=%d\n", __func__, status);
	if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num))  {
		mbuf_node = head;
		for (i = 0; i < num; i++) {
			mbuf_node->buf = kzalloc(sizeof(int) + PUB_HEAD_RSV + USBHUB_PDHLEN, GFP_KERNEL);
			memcpy(mbuf_node->buf + PUB_HEAD_RSV + USBHUB_PDHLEN, &status, sizeof(int));
			transfer_head = (transfer_head_t *)(mbuf_node->buf + PUB_HEAD_RSV);
			transfer_head->sdio_ops_type = XHCI_RX;
			transfer_head->sub_type = SET_USBHUB_STATUS;
			transfer_head->len = sizeof(int);
			mbuf_node->len = sizeof(int)+USBHUB_PDHLEN;
			if ((i+1) < num)
				mbuf_node = mbuf_node->next;
			else
				mbuf_node->next = NULL;
		}

		ret = sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);
		if (ret < 0) {
			pr_err("%s, SDIO error=%d\n",__func__, ret);
			return ret;
		}

	}

	return 0;
}

EXPORT_SYMBOL(usb_hub_notify_apcp_register_status);

static void usb_hub_register(void)
{
	int i;

	for (i = 0; i < 2; i++)
		sprdwcn_bus_chn_init(&usb_hub_ops[i]);
}

/* for test */
static struct dentry *wcn_usb_hub;
static char test_buf[128];

static int hub_cmd_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static ssize_t hub_cmd_read(struct file *filp, char __user *user_buf,
			    size_t count, loff_t *pos)
{
	return count;
}

static ssize_t usb_hub_proc_read(struct file *filp, char __user *user_buf,
			    size_t count, loff_t *pos)
{
	return 0;
}

static ssize_t hub_cmd_write(struct file *filp, const char __user *user_buf,
			     size_t count, loff_t *pos)
{
	struct mbuf_t *head, *tail, *mbuf_node;
	int num = 1, i;
	int ret;

	memset(test_buf, 0, 128);
	if (copy_from_user(test_buf + PUB_HEAD_RSV, user_buf, count))
		return -EFAULT;

	pr_info("%s write :%s, usb_hub_ops[0].channel:%d, pool_size:%d\n",
		__func__, test_buf + PUB_HEAD_RSV,
		usb_hub_ops[0].channel, usb_hub_ops[0].pool_size);

	if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num)) {
		mbuf_node = head;
		for (i = 0; i < num; i++) {
			mbuf_node->buf = kzalloc(count + PUB_HEAD_RSV, GFP_KERNEL);
			memcpy(mbuf_node->buf, test_buf, count + PUB_HEAD_RSV);
			mbuf_node->len = count;
			if ((i+1) < num)
				mbuf_node = mbuf_node->next;
			else
				mbuf_node->next = NULL;
		}

		ret = sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);
		if (ret < 0) {
			pr_err("%s, SDIO error=%d\n",__func__, ret);
			return ret;
		}
	}

	return count;
}

static ssize_t usb_hub_proc_write(struct file *filp,
		const char __user *user_buf, size_t count, loff_t *pos)
{
	char command[64];
	ssize_t ret = -1;

	memset(command, '\0', 64);
	if(copy_from_user(&command, user_buf, count)) {
		return -EFAULT;
	}
	command[count] = '\0';

	if (strncmp(command, "start_usb", 9) == 0) {
		wcn_usb_driver_register();
		return count;
	}

	if (strncmp(command, "stop_usb", 8) == 0) {
		wcn_usb_driver_unregister();
		return count;
	}

	return ret;
}

static int hub_cmd_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static const struct file_operations usb_test_fops = {
	.open = hub_cmd_open,
	.read = hub_cmd_read,
	.write = hub_cmd_write,
	.release = hub_cmd_release,
};

static const struct file_operations usb_hub_proc_fops = {
	.read 		= usb_hub_proc_read,
	.write		= usb_hub_proc_write,
};

void usb_hub_test_init(void)
{
	/* create debugfs */
	wcn_usb_hub = debugfs_create_dir("usb_hub", NULL);
	if (!debugfs_create_file("cmd_test", 0444, wcn_usb_hub,
		NULL, &usb_test_fops)) {
		pr_err("%s debugfs_create_file fail!!\n", __func__);
		debugfs_remove_recursive(wcn_usb_hub);
	}

	/*creat proc node of usb_control*/
	proc_create_data("usb_control", 0666, NULL, &usb_hub_proc_fops, NULL);
}

int unisoc_hub_init(void)
{
	pr_info("%s entry!\n", __func__);

	usb_hub_test_init();

	usb_hub_register();

	return 0;
}

