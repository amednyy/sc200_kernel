// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2018 Spreadtrum Communications Inc.
 * Copyright (C) 2018 Linaro Ltd.
 */

#include <linux/bitops.h>
#include <linux/gpio/driver.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>
#include <linux/spinlock.h>
#include <linux/i2c.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/of_gpio.h>

#define HRTIMER_FRAME	20
#define AW9523B_DRIVER_VERSION		"driver version: 1.0.0"

#define AW9523_ID 0x23


#define AW9523_PORT_MAX 	(0x10) // 16
#define AW9523_KEY_PORT_MAX (0x10) // 16
#define AW9523_LED_MAX_NUMS (0x10) // 16

#define AWINIC_DEBUG		1
#if (defined (AWINIC_DEBUG)  &&  (AWINIC_DEBUG != 0))
#define AW_DEBUG(fmt, args...) \
	do {	\
		printk(fmt, ##args); \
	} while (0)
#else
#define AW_DEBUG(fmt, ...) \
	do {	\
	} while (0)
#endif

struct aw9523 {
	struct i2c_client *client;
	struct device *dev;
	struct gpio_chip gpio_chip;
	struct irq_chip *irq_chip;
	struct irq_domain *domain;
	struct pinctrl_dev *pctl_dev;

	unsigned int npins;

	unsigned int input_port_mask;
	unsigned int int_port_mask;

	unsigned int old_input_state;
	unsigned int new_input_state;

	int irq_gpio;
	int irq_num;
	int rst_gpio;
};

enum aw9523_gpio_dir {
	AW9523_GPIO_INPUT = 0,
	AW9523_GPIO_OUTPUT = 1,
};

enum aw9523_gpio_val {
	AW9523_GPIO_HIGH = 1,
	AW9523_GPIO_LOW = 0,
};

enum aw9523_gpio_output_mode {
	AW9523_OPEN_DRAIN_OUTPUT = 0,
	AW9523_PUSH_PULL_OUTPUT = 1,
};

/*register list */
#define P0_INPUT		0x00
#define P1_INPUT 		0x01
#define P0_OUTPUT 		0x02
#define P1_OUTPUT 		0x03
#define P0_CONFIG		0x04
#define P1_CONFIG 		0x05
#define P0_INT			0x06
#define P1_INT			0x07
#define ID_REG			0x10
#define CTL_REG			0x11
#define P0_LED_MODE		0x12
#define P1_LED_MODE		0x13
#define P1_0_DIM0		0x20
#define P1_1_DIM0		0x21
#define P1_2_DIM0		0x22
#define P1_3_DIM0		0x23
#define P0_0_DIM0		0x24
#define P0_1_DIM0		0x25
#define P0_2_DIM0		0x26
#define P0_3_DIM0		0x27
#define P0_4_DIM0		0x28
#define P0_5_DIM0		0x29
#define P0_6_DIM0		0x2A
#define P0_7_DIM0		0x2B
#define P1_4_DIM0		0x2C
#define P1_5_DIM0		0x2D
#define P1_6_DIM0		0x2E
#define P1_7_DIM0		0x2F
#define SW_RSTN			0x7F


struct pinctrl_pin_desc	aw9523_pin[] = {
	{.number = 0x00, .name = "P0_0"},
	{.number = 0x01, .name = "P0_1"},
	{.number = 0x02, .name = "P0_2"},
	{.number = 0x03, .name = "P0_3"},
	{.number = 0x04, .name = "P0_4"},
	{.number = 0x05, .name = "P0_5"},
	{.number = 0x06, .name = "P0_6"},
	{.number = 0x07, .name = "P0_7"},
	{.number = 0x08, .name = "P1_0"},
	{.number = 0x09, .name = "P1_1"},
	{.number = 0x0a, .name = "P1_2"},
	{.number = 0x0b, .name = "P1_3"},
	{.number = 0x0c, .name = "P1_4"},
	{.number = 0x0d, .name = "P1_5"},
	{.number = 0x0e, .name = "P1_6"},
	{.number = 0x0f, .name = "P1_7"},
};
#define AW9523_SLAVE_ADDR_BASE 0x58
#define AW9523_GPIO_CHIP_MAX 4
static const char *aw9523_gpio_label_name[AW9523_GPIO_CHIP_MAX] = {
	"aw9523_gpio_58", "aw9523_gpio_59", "aw9523_gpio_5a",
	"aw9523_gpio_5b",
};
/*********************************************************
 *
 * awxxxx i2c write/read byte
 *
 ********************************************************/
//i2c_transfer will lock the adapter, so no need to add other lock in our code
static unsigned int i2c_write_byte(struct i2c_client *client, unsigned char reg_addr, unsigned char reg_data)
{
	int ret = 0;
	unsigned char wdbuf[2] = {0};

	struct i2c_msg msgs[] = {
		{
			.addr	= client->addr,
			.flags	= 0,
			.len	= 2,
			.buf	= wdbuf,
		},
	};

	if (NULL == client) {
		dev_err(&client->dev, "msg %s i2c client is NULL\n", __func__);
		return -1;
	}
	wdbuf[0] = reg_addr;
	wdbuf[1] = reg_data;

	ret = i2c_transfer(client->adapter, msgs, 1);
	if (ret < 0) {
		dev_err(&client->dev, "msg %s i2c write error: %d\n", __func__, ret);
	}

	dev_info(&client->dev, "msg %s i2c write slave_addr=0x%x reg_addr= %d reg_data=%d/0x%x\n", __func__, client->addr, reg_addr, reg_data,reg_data);

	return ret;

}
static unsigned int i2c_read_byte(struct i2c_client *client, unsigned char reg_addr, unsigned char *val)
{
	int ret = 0;
	unsigned char rdbuf[2] = {0};

	struct i2c_msg msgs[] = {
		{
			.addr	= client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= &reg_addr,
		},
		{
			.addr	= client->addr,
			.flags	= I2C_M_RD,
			.len	= 1,
			.buf	= rdbuf,
		},
	};

	if (NULL == client) {
		dev_err(&client->dev, "msg %s i2c client is NULL\n", __func__);
		return -1;
	}

	ret = i2c_transfer(client->adapter, msgs, 2);
	if (ret < 0) {
		dev_err(&client->dev, "msg %s i2c read error: %d\n", __func__, ret);
	}
	*val = rdbuf[0];

	dev_info(&client->dev, "msg %s i2c read slave_addr=0x%x reg_addr= %d reg_data=%d/0x%x\n", __func__, client->addr, reg_addr, *val, *val);

	return ret;
}

/*********************************************************
 *
 * awxxxx i2c write bytes
 *
 ********************************************************/
/*
static int i2c_write_multi_byte(struct i2c_client *client, unsigned char reg_addr, unsigned char *buf, unsigned int len)
{
   int ret = 0;
   unsigned char *wdbuf;

   wdbuf = kmalloc(len+1, GFP_KERNEL);
   if (wdbuf == NULL) {
       dev_err(&client->dev,"%s: can not allocate memory\n", __func__);
       return  -ENOMEM;
   }

   wdbuf[0] = reg_addr;
   memcpy(&wdbuf[1], buf, len);

   ret = i2c_master_send(client, wdbuf, len+1);
   if (ret < 0) {
       dev_err(&client->dev,"%s: i2c master send error\n", __func__);
   }

   kfree(wdbuf);

   return ret;
}
*/
static unsigned int i2c_read_multi_byte(struct i2c_client *client, unsigned char reg_addr, unsigned char *buf, unsigned int len)
{
	int ret = 0;
	unsigned char *rdbuf = NULL;
	struct i2c_msg msgs[] = {
		{
			.addr	= client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= &reg_addr,
		},
		{
			.addr	= client->addr,
			.flags	= I2C_M_RD,
			.len	= len,
			//.buf	= rdbuf,
		},
	};

	rdbuf = kmalloc(len, GFP_KERNEL);
	if (rdbuf == NULL) {
		dev_err(&client->dev, "%s: can not allocate memory\n", __func__);
		return  -ENOMEM;
	}

	msgs[1].buf = rdbuf;

	if (NULL == client) {
		dev_err(&client->dev, "msg %s i2c client is NULL\n", __func__);
		return -1;
		kfree(rdbuf);
	}

	ret = i2c_transfer(client->adapter, msgs, sizeof(msgs) / sizeof(msgs[0]));
	if (ret < 0) {
		dev_err(&client->dev, "msg %s i2c read error: %d\n", __func__, ret);
	}
	if (buf != NULL) {
		memcpy(buf, rdbuf, len);
	}
	kfree(rdbuf);

	dev_info(&client->dev, "msg %s i2c read slave_addr=0x%x reg_addr= %d buf[0]=0x%x  buf[0]=0x%x\n", __func__, client->addr, reg_addr, buf[0], buf[1]);

	return ret;
}

/******************************************************
 *
 * aw9523 chip ctrol interface
 *
 ******************************************************/
/* val 1-> gpio mode  0->led mode */
static void aw9523_set_gpio_mode_by_mask(struct aw9523 *p_aw9523, unsigned int mask, int val)
{
	unsigned char reg_val[2] = {0};
	int i = 0;
	i2c_read_byte(p_aw9523->client, P0_LED_MODE, &reg_val[0]); //p0_0 -- p0_7
	i2c_read_byte(p_aw9523->client, P1_LED_MODE, &reg_val[1]); //p1_0 -- p1_7
	/* 0-output 1-input */
	for (i = 0; i < AW9523_KEY_PORT_MAX; i++) {
		if (mask & (0x01 << i)) {
			if (val) {
				if (i < 8) {
					reg_val[0] |=  0x1 << i;
				} else {
					reg_val[1] |=  0x1 << (i - 8);
				}
			} else {
				if (i < 8) {
					reg_val[0] &=  ~(0x1 << i);
				} else {
					reg_val[1] &=  ~(0x1 << (i - 8));
				}
			}
		}
	}
	i2c_write_byte(p_aw9523->client, P0_LED_MODE, reg_val[0]);
	i2c_write_byte(p_aw9523->client, P1_LED_MODE, reg_val[1]);
}

/******************************************************
 *
 * aw9523 chip ctrol interface
 *
 ******************************************************/
/* val 1-> output  0->input */
static void aw9523_set_port_direction_by_mask(struct aw9523 *p_aw9523, unsigned int mask, int val)
{
	unsigned char reg_val[2] = {0};
	int i = 0;
	i2c_read_byte(p_aw9523->client, P0_CONFIG, &reg_val[0]); //p0_0 -- p0_7
	i2c_read_byte(p_aw9523->client, P1_CONFIG, &reg_val[1]); //p1_0 -- p1_7
	/* 0-output 1-input */
	for (i = 0; i < AW9523_KEY_PORT_MAX; i++) {
		if (mask & (0x01 << i)) {
			if (!val) {
				if (i < 8) {
					reg_val[0] |=  0x1 << i;
				} else {
					reg_val[1] |=  0x1 << (i - 8);
				}
			} else {
				if (i < 8) {
					reg_val[0] &=  ~(0x1 << i);
				} else {
					reg_val[1] &=  ~(0x1 << (i - 8));
				}
			}
		}
	}
	i2c_write_byte(p_aw9523->client, P0_CONFIG, reg_val[0]);
	i2c_write_byte(p_aw9523->client, P1_CONFIG, reg_val[1]);
}
/* return  0=out, 1=in */
static int aw9523_get_port_direction_by_mask(struct aw9523 *p_aw9523, unsigned int mask)
{
	unsigned char reg_val[2] = {0};
	unsigned int dir_reg = 0;
	i2c_read_multi_byte(p_aw9523->client, P0_CONFIG, reg_val, sizeof(reg_val) / sizeof(reg_val[0]));
	dir_reg = (reg_val[1] << 8) | reg_val[0];
	return (dir_reg & mask) ? 1 : 0;
}

/* val 1-> output high  0->output low */
static void aw9523_set_port_output_state_by_mask(struct aw9523 *p_aw9523, unsigned int mask, int val)
{
	unsigned char reg_val[2] = {0};
	int i = 0;
	i2c_read_byte(p_aw9523->client, P0_OUTPUT, &reg_val[0]); //p0_0 -- p0_7
	i2c_read_byte(p_aw9523->client, P1_OUTPUT, &reg_val[1]); //p1_0 -- p1_7
	for (i = 0; i < AW9523_KEY_PORT_MAX; i++) {
		if (mask & (0x01 << i)) {
			if (val) {
				if (i < 8) {
					reg_val[0] |=  0x1 << i;
				} else {
					reg_val[1] |=  0x1 << (i - 8);
				}
			} else {
				if (i < 8) {
					reg_val[0] &=  ~(0x1 << i);
				} else {
					reg_val[1] &=  ~(0x1 << (i - 8));
				}
			}
		}
	}
	i2c_write_byte(p_aw9523->client, P0_OUTPUT, reg_val[0]);
	i2c_write_byte(p_aw9523->client, P1_OUTPUT, reg_val[1]);
}

/* val 1-> input high  0->input low */
static unsigned int aw9523_get_port_input_state(struct aw9523 *p_aw9523)
{
	unsigned char reg_val[2] = {0};
	i2c_read_multi_byte(p_aw9523->client, P0_INPUT, &reg_val[0], sizeof(reg_val) / sizeof(reg_val[0]));
	return reg_val[0] | (reg_val[1] << 8);
}

/* val 1-> input high  0->input low */
static unsigned int aw9523_get_port_output_state(struct aw9523 *p_aw9523)
{
	unsigned char reg_val[2] = {0};
	i2c_read_multi_byte(p_aw9523->client, P0_OUTPUT, &reg_val[0], sizeof(reg_val) / sizeof(reg_val[0]));
	return reg_val[0] | (reg_val[1] << 8);
}

/*
static int aw9523_reset(struct aw9523 *p_aw9523)
{
	int ret = 0;
	struct device_node *node = p_aw9523->dev->of_node;
	p_aw9523->rst_gpio = of_get_named_gpio(node, "reset-gpio", 0);
	if ((!gpio_is_valid(p_aw9523->rst_gpio))){
		dev_err(p_aw9523->dev, "%s: dts don't provide reset-gpio\n",__func__);
		return -EINVAL;
	}

	ret = gpio_request(p_aw9523->rst_gpio, "aw9523-reset");
	if (ret) {
		dev_err(&p_aw9523->client->dev, "%s: unable to request gpio [%d]\n",__func__, p_aw9523->rst_gpio);
		return ret;
	}

	ret = gpio_direction_output(p_aw9523->rst_gpio, 1);
	if(ret) {
		gpio_free(p_aw9523->rst_gpio);
		dev_err(p_aw9523->dev, "%s: unable to set direction of gpio\n",__func__);
		return ret;
	}
	gpio_set_value(p_aw9523->rst_gpio, 1);
	msleep(1);
	gpio_set_value(p_aw9523->rst_gpio, 0);
	msleep(1);
	gpio_set_value(p_aw9523->rst_gpio, 1);
	return ret;


}
*/

/*********************************************************
 *
 * aw9523 reg
 *
 ********************************************************/
static ssize_t aw9523_get_reg(struct device *cd, struct device_attribute *attr, char *buf)
{
	unsigned char reg_val = 0;
	unsigned char i = 0;
	int j = 0;
	ssize_t len = 0;
	struct aw9523 *p_aw9523 = dev_get_drvdata(cd);
	struct i2c_client *client = p_aw9523->client;
	for (j = 0; j < 0x30; j++) {
		i2c_read_byte(client, j, &reg_val);
		len += snprintf(buf + len, PAGE_SIZE - len, "reg%2x = 0x%2x, \n", j, reg_val);
		switch (j) {
		case P0_LED_MODE:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d gpio mode\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d led mode\n", i);
				}
			}
			break;
		case P1_LED_MODE:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d gpio mode\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d led mode\n", i);
				}
			}
			len += snprintf(buf + len, PAGE_SIZE - len, "\r\n");
			break;
		case P0_CONFIG:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d input mode \n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d output mode\n", i);
				}
			}
			break;
		case P1_CONFIG:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d input mode\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d output mode\n", i);
				}
			}
			len += snprintf(buf + len, PAGE_SIZE - len, "\r\n");
			break;
		case P0_INT:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d interrrupt disable\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d interrrupt enable\n", i);
				}
			}
			break;
		case P1_INT:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d interrrupt disable\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d interrrupt enable\n", i);
				}
			}
			len += snprintf(buf + len, PAGE_SIZE - len, "\r\n");
			break;
		case P0_OUTPUT:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d output high\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d output low\n", i);
				}
			}
			break;
		case P1_OUTPUT:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d output high\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d output low\n", i);
				}
			}
			len += snprintf(buf + len, PAGE_SIZE - len, "\r\n");
			break;

		case P0_INPUT:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d input high\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P0_%d intput low\n", i);
				}
			}
			break;
		case P1_INPUT:
			for (i = 0; i < 8; i++) {
				if (reg_val & (0x01 << i)) {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d input high\n", i);
				} else {
					len += snprintf(buf + len, PAGE_SIZE - len, "P1_%d input low\n", i);
				}
			}
			len += snprintf(buf + len, PAGE_SIZE - len, "\r\n");
			break;
		case CTL_REG:
			if (reg_val & (0x1 << 7)) {
				len += snprintf(buf + len, PAGE_SIZE - len, "gpio Pull-Push mode\n");
			} else {
				len += snprintf(buf + len, PAGE_SIZE - len, "gpio OD mode\n");
			}
			len += snprintf(buf + len, PAGE_SIZE - len, "\r\n");
			break;
		default:
			break;
		}

	}

	return len;
}

static ssize_t aw9523_set_reg(struct device *cd, struct device_attribute *attr, const char *buf, size_t len)
{
	unsigned int databuf[2];
	struct aw9523 *p_aw9523 = dev_get_drvdata(cd);
	struct i2c_client *client = p_aw9523->client;
	if (2 == sscanf(buf, "%x %x", &databuf[0], &databuf[1])) {
		i2c_write_byte(client, databuf[0], databuf[1]);
	}
	return len;
}


#if 0
/******************************************************
 *
 * gpio feature start
 *
 ******************************************************/
/* gpio sys node */

/* echo gpio_idx dirction state > aw9523_gpio */
static ssize_t aw9523_gpio_store(struct device *dev, struct device_attribute *attr,
			const char *buf, size_t len)
{
	unsigned int databuf[3];
	int i = 0;
	struct aw9523 *p_aw9523 = dev_get_drvdata(dev);
	struct aw9523_gpio *p_gpio_data =  p_aw9523->gpio_data;
	struct aw9523_singel_gpio *p_single_gpio_data =  p_gpio_data->single_gpio_data;
	sscanf(buf, "%d %d %d", &databuf[0], &databuf[1], &databuf[2]);
	AW_DEBUG("aw9523 gpio cmd param: %d %d %d\n", databuf[0], databuf[1], databuf[2]);
	//if (p_gpio_data->gpio_mask & (0x01 << databuf[0])) {
	for (i = 0; i < p_gpio_data->gpio_num; i++) {
		if (p_single_gpio_data[i].gpio_idx == databuf[0]) {
			if (p_single_gpio_data[i].gpio_direction != databuf[1]) {
				p_single_gpio_data[i].gpio_direction = databuf[1];
				aw9523_set_port_direction_by_mask(p_aw9523, 0x1 << p_single_gpio_data[i].gpio_idx,
								p_single_gpio_data[i].gpio_direction);
			}
			if (p_single_gpio_data[i].gpio_direction == 0x01) { // output
				if (p_single_gpio_data[i].state != databuf[2]) {
					p_single_gpio_data[i].state = databuf[2];
					aw9523_set_port_output_state_by_mask(p_aw9523,
										0x1 << p_single_gpio_data[i].gpio_idx,
										p_single_gpio_data[i].state);
				}
			}
		}
	}

	//}
	return len;
}

static ssize_t aw9523_gpio_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	ssize_t len = 0;
	int i = 0;
	struct aw9523 *p_aw9523 = dev_get_drvdata(dev);
	struct aw9523_singel_gpio *p_single_gpio_data = p_aw9523->gpio_data->single_gpio_data;
	len += snprintf(buf + len, PAGE_SIZE - len, "Uasge: echo gpio_idx dirction state > aw9523_gpio\n");
	for (i = 0; i < p_aw9523->gpio_data->gpio_num; i++) {
		len += snprintf(buf + len, PAGE_SIZE - len, "aw9523 gpio idx = %d, dir = %d, state = %d", p_single_gpio_data[i].gpio_idx,
				p_single_gpio_data[i].gpio_direction,
				p_single_gpio_data[i].state);
	}
	return len;
}
#endif

static DEVICE_ATTR(reg, 0660, aw9523_get_reg,  aw9523_set_reg);
//static DEVICE_ATTR(aw9523_gpio, S_IWUSR | S_IRUGO, aw9523_gpio_show, aw9523_gpio_store);


static struct attribute *aw9523_attributes[] = {
	&dev_attr_reg.attr,
	//&dev_attr_aw9523_gpio.attr,
	NULL
};
static struct attribute_group aw9523_ttribute_group = {
	.attrs = aw9523_attributes
};

static int aw9523_create_sysfs(struct i2c_client *client)
{
	int err;
	struct device *dev = &(client->dev);

	err = sysfs_create_group(&dev->kobj, &aw9523_ttribute_group);

	return err;
}

/*********************************************************
 *
 * aw9523 check chipid
 *
 ********************************************************/
static int aw9523_read_chipid(struct i2c_client *client)
{
	unsigned char val;
	unsigned char cnt;

	for (cnt = 5; cnt > 0; cnt--) {
		i2c_read_byte(client, ID_REG, &val);
		AW_DEBUG("aw9523_read_chipid val=0x%x\n", val); //0x80:aw9523a,0x23:aw9523b
		if (val == AW9523_ID) {
			return 0;
		}
		msleep(5);
	}
	return -EINVAL;
}


// return	0=out, 1=in
static int aw9523_gpio_get_direction(struct gpio_chip *chip, unsigned offset)
{
	struct aw9523 *p_aw9523 = gpiochip_get_data(chip);
	dev_info(chip->parent, "%s p_aw9523 =0x%x offset=%d\n", __func__, p_aw9523, offset);
	if (p_aw9523 == NULL) {
		//dump_stack();
		return 1;
	} else {
		return aw9523_get_port_direction_by_mask(p_aw9523, 0x01 << offset);
	}
}

static int aw9523_gpio_direction_input(struct gpio_chip *chip, unsigned offset)
{
	struct aw9523 *p_aw9523 =  gpiochip_get_data(chip);
	dev_info(chip->parent, "%s p_aw9523 =0x%x  offset=%d  \n", __func__, p_aw9523, offset);
	p_aw9523->input_port_mask |= 0x01 << offset;
	aw9523_set_port_direction_by_mask(p_aw9523, 0x01 << offset, 0);
	return 0;

}

static int aw9523_gpio_direction_output(struct gpio_chip *chip, unsigned offset, int value)
{
	struct aw9523 *p_aw9523 = gpiochip_get_data(chip);
	dev_info(chip->parent, "%s p_aw9523 =0x%x offset=%d value=0x%x \n", __func__, p_aw9523,  offset, value);
	aw9523_set_port_direction_by_mask(p_aw9523, 0x01 << offset, 1);
	aw9523_set_port_output_state_by_mask(p_aw9523, 0x01 << offset, !(!value));
	return 0;

}

static int aw9523_gpio_get(struct gpio_chip *chip, unsigned offset)
{
	struct aw9523 *p_aw9523 =  gpiochip_get_data(chip);
	unsigned int ret = 0;
	if (aw9523_gpio_get_direction(chip, offset)) {
		ret = aw9523_get_port_input_state(p_aw9523);
	} else {
		ret = aw9523_get_port_output_state(p_aw9523);
	}
	dev_info(chip->parent, "%s p_aw9523 =0x%x offset=%d raw_state=0x%x\n", __func__, p_aw9523,offset, ret);
	return !!(ret &( 1<<offset));
}

static void aw9523_gpio_set(struct gpio_chip *chip, unsigned offset, int value)
{
	struct aw9523 *p_aw9523 =  gpiochip_get_data(chip);
	dev_info(chip->parent, "%s p_aw9523 =0x%x offset=%d value=0x%x\n", __func__, p_aw9523, offset, value);
	aw9523_set_port_output_state_by_mask(p_aw9523, 0x01 << offset, !(!value));
}



static int aw9523_gpio_request(struct gpio_chip *chip, unsigned int offset)
{
	struct aw9523 *p_aw9523 = gpiochip_get_data(chip);
	dev_info(chip->parent, "%s p_aw9523 =0x%x \n", __func__, p_aw9523);
	//aw9523_set_port_direction_by_mask(p_aw9523, 0x01<<offset, 1);
	aw9523_set_gpio_mode_by_mask(p_aw9523, 0x01<<offset, 1);
	return 0;

}


static void aw9523_gpio_free(struct gpio_chip *chip, unsigned int offset)
{
	struct aw9523 *p_aw9523 = gpiochip_get_data(chip);
	dev_info(chip->parent, "%s p_aw9523 =0x%x \n", __func__, p_aw9523);
	//aw9523_set_port_direction_by_mask(p_aw9523, 0x01<<offset, 1);
	return;

}

static int aw9523_gpio_to_irq(struct gpio_chip *chip, unsigned offset)
{
	struct aw9523 *p_aw9523 =  gpiochip_get_data(chip);
	int irq = 0;
	dev_info(chip->parent, "%s p_aw9523 =0x%x \n", __func__, p_aw9523);
	irq = irq_find_mapping(p_aw9523->domain, offset);
	if (!irq)
		return -EINVAL;
	return irq;
}

static int aw9523_gpio_pctl_setup(struct aw9523 *p_aw9523)
{
	struct i2c_client *client = p_aw9523->client;
	int aw9523_idx = 0;
	int i = 0;
//	int gpio_base = 0;
//	struct device_node *node = p_aw9523->dev->of_node;
	dev_info(&client->dev, "%s client->addr=0x%x \n",__func__, client->addr);

	p_aw9523->npins = sizeof(aw9523_pin) / sizeof(aw9523_pin[0]);

	for (i = 0; i < sizeof(aw9523_pin) / sizeof(aw9523_pin[0]); i++) {
		aw9523_pin[i].drv_data = p_aw9523;
	}

	p_aw9523->gpio_chip.owner			= THIS_MODULE;
	p_aw9523->gpio_chip.request		=  aw9523_gpio_request;
	p_aw9523->gpio_chip.free			=  aw9523_gpio_free;
	p_aw9523->gpio_chip.get_direction		= aw9523_gpio_get_direction;
	p_aw9523->gpio_chip.direction_input	= aw9523_gpio_direction_input;
	p_aw9523->gpio_chip.direction_output	= aw9523_gpio_direction_output;
	p_aw9523->gpio_chip.get			= aw9523_gpio_get;
	p_aw9523->gpio_chip.set			= aw9523_gpio_set;
	p_aw9523->gpio_chip.to_irq			= aw9523_gpio_to_irq;
	p_aw9523->gpio_chip.of_gpio_n_cells	= 2;
	p_aw9523->gpio_chip.ngpio = AW9523_PORT_MAX;
	aw9523_idx = client->addr-AW9523_SLAVE_ADDR_BASE;
	if (aw9523_idx < AW9523_GPIO_CHIP_MAX)
		p_aw9523->gpio_chip.label = aw9523_gpio_label_name[aw9523_idx];
	aw9523_idx++;
	p_aw9523->gpio_chip.parent = p_aw9523->dev;
	p_aw9523->gpio_chip.base = -1;

	return 0;
}



/*********************************************************
 *
 * aw9523 driver
 *
 ********************************************************/
static int aw9523_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct aw9523 *p_aw9523 = NULL;
	int ret = 0;
	dev_info(&client->dev, "%s enter,%s  addr=0x%x\n", __func__, AW9523B_DRIVER_VERSION, client->addr);
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		dev_err(&client->dev, "I2C transfer not Supported\n");
		return -EIO;
	}
	p_aw9523 = devm_kzalloc(&client->dev, sizeof(struct aw9523), GFP_KERNEL);
	dev_info(&client->dev, "p_aw9523 =0x%x \n", p_aw9523);
	if (!p_aw9523) {
		dev_err(&client->dev, "Failed to allocate memory\n");
		return -ENOMEM;
	}
	p_aw9523->dev = &client->dev;
	p_aw9523->client = client;
	i2c_set_clientdata(client, p_aw9523);

	//	ret = aw9523_reset(p_aw9523);
	//	if (ret) {
	//		goto err_free_mem;
	//	}
	if (aw9523_read_chipid(client)) {
		dev_err(&client->dev, "%s: read_chipid error\n", __func__);
		//		goto err_free_rst;
	}


	ret = aw9523_gpio_pctl_setup(p_aw9523);
	if (ret < 0) {
		dev_err(&client->dev, "%s: aw9523_gpio_pctl_setup error\n", __func__);
		goto err_free_rst;
	}
	//	platform_set_drvdata(pdev, tgi);


	ret = devm_gpiochip_add_data(p_aw9523->dev, &p_aw9523->gpio_chip, p_aw9523);

	/* create debug dev node - reg */
	ret = aw9523_create_sysfs(client);
	if (ret < 0) {
		dev_err(&client->dev, "%s: aw9523_create_sysfs error\n", __func__);
		goto free_gpio_pctl;
	}
	AW_DEBUG("aw9523_key_i2c_probe success!\n");

	i2c_write_byte(client, P0_LED_MODE, 0xFF);	// gpio mode
	i2c_write_byte(client, P1_LED_MODE, 0xFF);	// gpio mode
	i2c_write_byte(client, P0_CONFIG, 0x00);		// output mode
	i2c_write_byte(client, P1_CONFIG, 0x00);		// output mode
	i2c_write_byte(client, CTL_REG, 0x11);		// P0 push pull mode
	return 0;
free_gpio_pctl:
	gpiochip_remove(&p_aw9523->gpio_chip);
err_free_rst:
	gpio_free(p_aw9523->rst_gpio);
	//err_free_mem:
	devm_kfree(&client->dev, p_aw9523);
	AW_DEBUG("aw9523_key_i2c_probe fail!\n");
	return ret;
}

static int aw9523_i2c_remove(struct i2c_client *client)
{
	struct aw9523 *p_aw9523 = i2c_get_clientdata(client);
	gpiochip_remove(&p_aw9523->gpio_chip);
	gpio_free(p_aw9523->rst_gpio);
	gpio_free(p_aw9523->irq_gpio);
	devm_kfree(p_aw9523->dev, p_aw9523);
	return 0;
}

static const struct of_device_id aw9523_keypad_of_match[] = {
	{ .compatible = "awinic,aw9523b",},
	{},
};

static const struct i2c_device_id aw9523_key_i2c_id[] = {
	{"awinic,aw9523b", 0},
	{},
};
MODULE_DEVICE_TABLE(i2c, aw9523_key_i2c_id);

static struct i2c_driver aw9523_key_i2c_driver = {
	.driver = {
		.name = "aw9523-gpio",
		.owner = THIS_MODULE,
		.of_match_table = aw9523_keypad_of_match,
	},
	.probe = aw9523_i2c_probe,
	.remove = aw9523_i2c_remove,
	.id_table = aw9523_key_i2c_id,
};

//module_i2c_driver(aw9523_key_i2c_driver);

static int __init aw9523_key_i2c_init(void)
{
	int ret = 0;

	AW_DEBUG("%s enter\n", __func__);

	ret = i2c_add_driver(&aw9523_key_i2c_driver);
	if (ret) {
		printk("fail to add aw9523 device into i2c\n");
		return ret;
	}

	AW_DEBUG("%s exit\n", __func__);

	return 0;
}
module_init(aw9523_key_i2c_init);

static void __exit aw9523_key_i2c_exit(void)
{
	AW_DEBUG("%s enter\n", __func__);

	i2c_del_driver(&aw9523_key_i2c_driver);

	AW_DEBUG("%s exit\n", __func__);
}
module_exit(aw9523_key_i2c_exit);

MODULE_DESCRIPTION("AW9523B driver");
MODULE_LICENSE("GPL v2");
