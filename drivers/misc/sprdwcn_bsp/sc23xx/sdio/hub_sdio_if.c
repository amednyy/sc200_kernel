#include <linux/module.h>
#include <linux/debugfs.h>
#include <linux/file.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/vmalloc.h>

#include <soc/sprd/wcn_bus.h>

#define USB_HUB_TX 7
#define USB_HUB_RX 21

#define USB_HUB_OUT 1
#define USB_HUB_IN 0

int usb_hub_tx_pop(int channel, struct mbuf_t *head,
		   struct mbuf_t *tail, int num)
{
	int i;
	struct mbuf_t *mbuf_node;

	pr_info("%s channel:%d head:%p tail:%p num:%d\n",
		__func__, channel, head, tail, num);

	for (i = 0; i < (head->len < 80 ? head->len:80); i++)
		pr_info("%s i%d 0x%x\n", __func__, i, head->buf[i]);

	for (i = 0, mbuf_node = head; i < num;
	     i++, mbuf_node = mbuf_node->next) {
		kfree(head->buf);
		head->buf = NULL;
	}

	sprdwcn_bus_list_free(channel, head, tail, num);

	return 0;
}

int usb_hub_rx_pop(int channel, struct mbuf_t *head,
		   struct mbuf_t *tail, int num)
{
	int i;

	pr_info("%s channel:%d head:%p tail:%p num:%d\n",
		__func__, channel, head, tail, num);

	for (i = 0; i < (head->len < 80 ? head->len:80); i++)
		pr_info("%s i%d 0x%x\n", __func__, i, head->buf[i]);

	sprdwcn_bus_push_list(channel, head, tail, num);

	return 0;
}

struct mchn_ops_t usb_hub_ops[2] = {
	{
		.channel = USB_HUB_TX,
		.inout = USB_HUB_OUT,
		.pool_size = 5,
		.pop_link = usb_hub_tx_pop,
	},

	{
		.channel = USB_HUB_RX,
		.inout = USB_HUB_IN,
		.pool_size = 5,
		.pop_link = usb_hub_rx_pop,
	},
};

static void usb_hub_register(void)
{
	int i;

	for (i = 0; i < 2; i++)
		sprdwcn_bus_chn_init(&usb_hub_ops[i]);
}

/* for test */
static struct dentry *wcn_usb_hub;
static char test_buf[128];

static int hub_cmd_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static ssize_t hub_cmd_read(struct file *filp, char __user *user_buf,
			    size_t count, loff_t *pos)
{
	return count;
}

static ssize_t hub_cmd_write(struct file *filp, const char __user *user_buf,
			     size_t count, loff_t *pos)
{
	struct mbuf_t *head, *tail, *mbuf_node;
	int num = 1, i;


	memset(test_buf, 0, 128);
	if (copy_from_user(test_buf + PUB_HEAD_RSV, user_buf, count))
		return -EFAULT;

	pr_info("%s write :%s, usb_hub_ops[0].channel:%d, pool_size:%d\n",
		__func__, test_buf + PUB_HEAD_RSV, usb_hub_ops[0].channel, usb_hub_ops[0].pool_size);

	if (!sprdwcn_bus_list_alloc(usb_hub_ops[0].channel, &head, &tail, &num)) {
		mbuf_node = head;
		for (i = 0; i < num; i++) {
			mbuf_node->buf = kzalloc(count + PUB_HEAD_RSV, GFP_KERNEL);
			memcpy(mbuf_node->buf, test_buf, count + PUB_HEAD_RSV);
			mbuf_node->len = count;
			if ((i+1) < num)
				mbuf_node = mbuf_node->next;
			else
				mbuf_node->next = NULL;
		}

		sprdwcn_bus_push_list(usb_hub_ops[0].channel, head, tail, num);
	}

	return count;
}

static int hub_cmd_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static const struct file_operations usb_test_fops = {
	.open = hub_cmd_open,
	.read = hub_cmd_read,
	.write = hub_cmd_write,
	.release = hub_cmd_release,
};

void usb_hub_test_init(void)
{
	/* create debugfs */
	wcn_usb_hub = debugfs_create_dir("usb_hub", NULL);
	if (!debugfs_create_file("cmd_test", 0444, wcn_usb_hub, NULL, &usb_test_fops)) {
		pr_err("%s debugfs_create_file fail!!\n", __func__);
		debugfs_remove_recursive(wcn_usb_hub);
	}
}

int unisoc_hub_init(void)
{
	pr_info("%s entry!\n", __func__);
	usb_hub_test_init();
	usb_hub_register();

	return 0;
}

