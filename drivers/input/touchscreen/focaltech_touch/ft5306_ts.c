/*
 * drivers/input/touchscreen/ft5x0x_ts.c
 *
 * FocalTech ft5x0x TouchScreen driver.
 *
 * Copyright (c) 2010  Focal tech Ltd.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * VERSION      	DATE			AUTHOR
 *    1.0		  2010-01-05			WenFS
 *
 * note: only support mulititouch	Wenfs 2010-10-01
 */
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/wait.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <linux/input.h>

#include <asm/uaccess.h>
#include <linux/proc_fs.h>	/*proc */
#include <linux/fs.h>

#include <asm/ioctl.h>

#include <linux/firmware.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <asm/uaccess.h>
#include <linux/gpio.h>
#if defined(CONFIG_HAS_EARLYSUSPEND)
#include <linux/earlysuspend.h>
#endif
#include <linux/regulator/consumer.h>
//#include <linux/i2c/ft5306_ts.h>
//#include <mach/regulator.h>
//#include <mach/i2c-sprd.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/of_gpio.h>
// #include <linux/wakelock.h>
#include <linux/irq.h>
#ifdef CONFIG_PM_SLEEP
#include <linux/notifier.h>
// #include <video/adf_notifier.h>
#endif

#include <linux/stringify.h>

#include "generic.h"
#include "ft5306_ts.h"

#define I2C_BOARD_INFO_METHOD   1
#define TS_DATA_THRESHOLD_CHECK	0

#define TOUCH_VIRTUAL_KEYS
#define CONFIG_FT5X0X_MULTITOUCH 1
#define FT6206_PROXIMITY	0

//static int debug_level=0;
static int suspend_flag = 0;
//static int irq_flag = 0;

#ifdef CONFIG_TP_GESTURE
static int poll=1;
struct wake_lock ft_poll_wake_lock;
#endif

#ifdef CONFIG_TP_GESTURE
#define GESTURE_LEFT		0x20
#define GESTURE_RIGHT		0x21
#define GESTURE_UP		    0x22
#define GESTURE_DOWN		0x23
#define GESTURE_DOUBLECLICK	0x24
#define GESTURE_O		    0x30
#if defined(CONFIG_PROJECT_TC18B_SP062)
#define GESTURE_W		    0x54
#else
#define GESTURE_W		    0x31
#endif
#define GESTURE_M		    0x32
#define GESTURE_E		    0x33
#define GESTURE_C		    0x34
#define GESTURE_Z		    0x41
#define GESTURE_Z2		    0x65
#define GESTURE_S		    0x46
#define GESTURE_V		    0x54
#define FTS_GESTRUE_POINTS	 255

#include "ft_gesture_lib.h"
short pointnum = 0;
unsigned long time_stamp = 0;
bool is_one_finger = true;
int data_xx[150] = {0};
int data_yy[150] = {0};
//static int *datax = NULL;
//static int *datay = NULL;
#endif

static struct regulator *vdd_ana;
struct ft5x0x_ts_platform_data *pdata_t;
#define attrify(propname) (&dev_attr_##propname.attr)

struct sprd_i2c_setup_data {
	unsigned i2c_bus;  //the same number as i2c->adap.nr in adapter probe function
	unsigned short i2c_address;
	int irq;
	char type[I2C_NAME_SIZE];
};

#if defined(CONFIG_PROJECT_TG66_V1_F5054)||defined(CONFIG_PROJECT_TG68B_4001)||defined(CONFIG_PROJECT_GE63_H5006_HD)||defined(CONFIG_PROJECT_GE63_H5006)
#define TOUCH_AUTO_UPDATE
#endif

#ifdef TOUCH_AUTO_UPDATE
#define FTS_REG_ID 0x64
#define FTS_REG_ECC		0xCC
#define FTS_REG_FW_VER		0xA6
#define FTS_ERASE_APP_REG	0x61
#define FTS_UPGRADE_LOOP		30
#define FTS_RST_CMD_REG2		0xBC
#define FTS_UPGRADE_AA		0xAA
#define FTS_UPGRADE_55		0x55
#define FTS_READ_ID_REG		0x90
#define FTS_FW_WRITE_CMD		0xBF
#define FTS_MAX_POINTS_2                        		2
#define AUTO_CLB_NONEED                         		0
#define FTS_PACKET_LENGTH      	128
#define FTS_REG_FIRMID						 0xa6

static DEFINE_MUTEX(i2c_rw_access);


static unsigned char CTPM_FW[] = {
#if defined(CONFIG_PROJECT_TG68B_4001)
	#include "TG68B_4001_KGF04506A_0x02_20170822_app.i"
#elif defined(CONFIG_PROJECT_GE63_H5006_HD)
#include "ge63_h5006_hd_0x04_20180829_app.i"
#elif defined(CONFIG_PROJECT_GE63_H5006)
#include "ge63_h5006_0x03_20180829_app.i"
#else
	#include "FT6336U_F5054_HD_SC_N10L_20170316_app.i"
#endif
};

struct fts_Upgrade_Info {
	 u8 CHIP_ID;
	 u8 TPD_MAX_POINTS;
	 u8 AUTO_CLB;
	 u16 delay_aa;		 /*delay of write FT_UPGRADE_AA */
	 u16 delay_55;		 /*delay of write FT_UPGRADE_55 */
	 u8 upgrade_id_1;	 /*upgrade id 1 */
	 u8 upgrade_id_2;	 /*upgrade id 2 */
	 u16 delay_readid;	 /*delay of read id */
	 u16 delay_earse_flash; /*delay of earse flash*/
 };

 struct fts_Upgrade_Info fts_updateinfo[] =
{
	{0x64,FTS_MAX_POINTS_2,AUTO_CLB_NONEED,10, 10, 0x79, 0x1c, 10, 2000}, //,"FT6x36UG"
};

struct fts_Upgrade_Info fts_updateinfo_curr;
static int fts_6x36_ctpm_fw_upgrade(struct i2c_client *client, u8 *pbt_buf, u32 dw_lenth);
#endif


#define FTS_CTL_IIC

#ifdef FTS_CTL_IIC
#include "focaltech_ctl.h"
#endif

#if(FT6206_PROXIMITY)
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/ioctl.h>


#define PROXIMITY_DEVICE   "ft_proximity"

/******************confirm the parameter that hal use**********************/
#define LTR_IOCTL_MAGIC        	0X38
#define LTR_IOCTL_PROX_ON		_IOW(LTR_IOCTL_MAGIC, 7,int)
#define LTR_IOCTL_PROX_OFF		_IOW(LTR_IOCTL_MAGIC, 8,int)
/****************************************/

static int misc_opened;
static int ft6206_proximity_start = 0;	/* 0 is stop, 1 is start */

#endif

#define PS_DEBUG_ON 	0
#define PS_DBG(fmt, arg...)				do{\
							   if(PS_DEBUG_ON)\
							   printk("<<PS-DBG>>[%d]"fmt"\n", __LINE__, ##arg);\
							}while(0)


struct ft5x0x_ts_data *g_ft5x0x_ts;
static struct i2c_client *this_client;
/* Attribute */
#if 0
static ssize_t ft5x0x_show_suspend(struct device* cd,struct device_attribute *attr, char* buf);
static ssize_t ft5x0x_store_suspend(struct device* cd, struct device_attribute *attr,const char* buf, size_t len);
static ssize_t ft5x0x_show_version(struct device* cd,struct device_attribute *attr, char* buf);
static ssize_t ft5x0x_show_vendor_version(struct device* cd,struct device_attribute *attr, char* buf);
static ssize_t ft5x0x_update(struct device* cd, struct device_attribute *attr, const char* buf, size_t len);
static ssize_t ft5x0x_show_debug(struct device* cd,struct device_attribute *attr, char* buf);
static ssize_t ft5x0x_store_debug(struct device* cd, struct device_attribute *attr,const char* buf, size_t len);
#endif
static unsigned char ft5x0x_read_fw_ver(void);
//static unsigned char ft5x0x_read_vendor_ver(void);
#if defined(CONFIG_HAS_EARLYSUSPEND)
static void ft5x0x_ts_suspend(struct early_suspend *handler);
static void ft5x0x_ts_resume(struct early_suspend *handler);
#elif defined(CONFIG_PM_SLEEP)
static int ft5x0x_ts_suspend(struct device *dev);
static int ft5x0x_ts_resume(struct device *dev);
#endif

//static int fts_ctpm_fw_update(void);
//static int fts_ctpm_fw_upgrade_with_i_file_old(void);

struct ts_event {
	u16	x1;
	u16	y1;
	u16	x2;
	u16	y2;
	u16	x3;
	u16	y3;
	u16	x4;
	u16	y4;
	u16	x5;
	u16	y5;
	u16	pressure;
    u8  touch_point;
};

struct ft5x0x_ts_data {
	struct input_dev	*input_dev;
	struct i2c_client	*client;
	struct ts_event	event;
	struct work_struct	pen_event_work;
	struct workqueue_struct	*ts_workqueue;
#if defined(CONFIG_HAS_EARLYSUSPEND)
	struct early_suspend	early_suspend;
#endif
	struct ft5x0x_ts_platform_data	*platform_data;
//	struct timer_list touch_timer;
	//bool key_down;  //add by blithe
};

static int ft5x0x_ts_hw_init(struct ft5x0x_ts_data *ft5x0x_ts);

#if CFG_DETECT_UP_EVENT
static struct timer_list _st_up_evnet_timer;
static unsigned int  _sui_last_point_cnt = 0;
#endif
#if 0
static DEVICE_ATTR(suspend, S_IRUGO | S_IWUSR, ft5x0x_show_suspend, ft5x0x_store_suspend);
static DEVICE_ATTR(update, S_IRUGO | S_IWUSR, ft5x0x_show_version, ft5x0x_update);
static DEVICE_ATTR(vendor_id, S_IRUGO | S_IWUSR, ft5x0x_show_vendor_version, NULL);
static DEVICE_ATTR(debug, S_IRUGO | S_IWUSR, ft5x0x_show_debug, ft5x0x_store_debug);
#endif
#ifdef TOUCH_AUTO_UPDATE

int fts_i2c_read(struct i2c_client *client, char *writebuf, int writelen, char *readbuf, int readlen)
{
	int ret;

	mutex_lock(&i2c_rw_access);

	if (writelen > 0) {
		struct i2c_msg msgs[] = {
			{
				 .addr = client->addr,
				 .flags = 0,
				 .len = writelen,
				 .buf = writebuf,
			 },
			{
				 .addr = client->addr,
				 .flags = I2C_M_RD,
				 .len = readlen,
				 .buf = readbuf,
			 },
		};
		ret = i2c_transfer(client->adapter, msgs, 2);
		if (ret < 0)
			dev_err(&client->dev, "%s: i2c read error.\n", __func__);
	} else {
		struct i2c_msg msgs[] = {
			{
				 .addr = client->addr,
				 .flags = I2C_M_RD,
				 .len = readlen,
				 .buf = readbuf,
			 },
		};
		ret = i2c_transfer(client->adapter, msgs, 1);
		if (ret < 0)
			dev_err(&client->dev, "%s:i2c read error.\n", __func__);
	}

	mutex_unlock(&i2c_rw_access);

	return ret;
}

int fts_i2c_write(struct i2c_client *client, char *writebuf, int writelen)
{
	int ret;

	struct i2c_msg msgs[] = {
		{
			 .addr = client->addr,
			 .flags = 0,
			 .len = writelen,
			 .buf = writebuf,
		 },
	};
	mutex_lock(&i2c_rw_access);
	ret = i2c_transfer(client->adapter, msgs, 1);
	if (ret < 0)
		dev_err(&client->dev, "%s: i2c write error.\n", __func__);

	mutex_unlock(&i2c_rw_access);

	return ret;
}


int fts_write_reg(struct i2c_client *client, u8 addr, const u8 val)
{
	u8 buf[2] = {0};

	buf[0] = addr;
	buf[1] = val;

	return fts_i2c_write(client, buf, sizeof(buf));
}


int fts_read_reg(struct i2c_client *client, u8 addr, u8 *val)
{
	return fts_i2c_read(client, &addr, 1, val, 1);
}

#if 0
static unsigned char fts_read_fw_ver(struct i2c_client *client)
{
	unsigned char ver;
	fts_read_reg(client, FTS_REG_FIRMID, &ver);
	return(ver);
}
#endif

/************************************************************************
* Name: fts_6x36_ctpm_fw_upgrade
* Brief:  fw upgrade
* Input: i2c info, file buf, file len
* Output: no
* Return: fail <0
***********************************************************************/

void fts_get_upgrade_array(struct i2c_client *client)
{

	u8 chip_id;
	//u32 i;
	int ret = 0;

	ret = fts_read_reg(client, FTS_REG_ID,&chip_id);
	//chip_id = 0x64;
	if (ret<0)
	{
		printk("lcl_add_fts_read_reg fail\n");
	}
	PS_DBG("%s chip_id = %x\n", __func__, chip_id);

		if(chip_id==fts_updateinfo[0].CHIP_ID
#if defined(CONFIG_PROJECT_TC28B_I9556) || defined(CONFIG_PROJECT_TC28B_I9556B)||defined(CONFIG_PROJECT_GE63_H5006_HD)||defined(CONFIG_PROJECT_GE63_H5006)
                || fts_updateinfo[0].CHIP_ID==0x64
#endif
                )
		{
			memcpy(&fts_updateinfo_curr, &fts_updateinfo[0], sizeof(struct fts_Upgrade_Info));
			PS_DBG("lcl_add chip_id=%d , CHIP_ID=%d\n",chip_id,fts_updateinfo_curr.CHIP_ID);
		}
}

static int fts_6x36_ctpm_fw_upgrade(struct i2c_client *client, u8 *pbt_buf, u32 dw_lenth)
{
	u8 reg_val[2] = {0};
	u32 i = 0;
	u32 packet_number;
	u32 j;
	u32 temp;
	u32 lenght;
	u32 fw_length;
	u8 packet_buf[FTS_PACKET_LENGTH + 6];
	u8 auc_i2c_write_buf[10];
	u8 bt_ecc;

	if(pbt_buf[0] != 0x02)
	{
		printk("[FTS] FW first byte is not 0x02. so it is invalid \n");
		return -1;
	}

	if(dw_lenth > 0x11f)
	{
		fw_length = ((u32)pbt_buf[0x100]<<8) + pbt_buf[0x101];
		if(dw_lenth < fw_length)
		{
			printk("[FTS] Fw length is invalid \n");
			return -1;
		}
	}
	else
	{
		printk("[FTS] Fw length is invalid \n");
		return -1;
	}

	for (i = 0; i < FTS_UPGRADE_LOOP; i++)
	{
		/*********Step 1:Reset  CTPM *****/
		PS_DBG("lcl_add==fts_6x36_ctpm_fw_upgrade==Step 1\n");
		fts_write_reg(client, FTS_RST_CMD_REG2, FTS_UPGRADE_AA);
		msleep(fts_updateinfo_curr.delay_aa);
		fts_write_reg(client, FTS_RST_CMD_REG2, FTS_UPGRADE_55);
		msleep(fts_updateinfo_curr.delay_55);
		/*********Step 2:Enter upgrade mode *****/
		PS_DBG("lcl_add==fts_6x36_ctpm_fw_upgrade==Step 2\n");
		auc_i2c_write_buf[0] = FTS_UPGRADE_55;
		fts_i2c_write(client, auc_i2c_write_buf, 1);
		auc_i2c_write_buf[0] = FTS_UPGRADE_AA;
		fts_i2c_write(client, auc_i2c_write_buf, 1);
		msleep(fts_updateinfo_curr.delay_readid);
		/*********Step 3:check READ-ID***********************/
		PS_DBG("lcl_add==fts_6x36_ctpm_fw_upgrade==Step 3\n");
		auc_i2c_write_buf[0] = FTS_READ_ID_REG;
		auc_i2c_write_buf[1] = auc_i2c_write_buf[2] = auc_i2c_write_buf[3] =0x00;
		reg_val[0] = 0x00;
		reg_val[1] = 0x00;
		fts_i2c_read(client, auc_i2c_write_buf, 4, reg_val, 2);


		if (reg_val[0] == fts_updateinfo_curr.upgrade_id_1
			&& reg_val[1] == fts_updateinfo_curr.upgrade_id_2)
		{
			PS_DBG("lcl_add_ok==[FTS] Step 3: GET CTPM ID OK,ID1 = 0x%x,ID2 = 0x%x\n",
				reg_val[0], reg_val[1]);
			break;
		}
		else
		{
			dev_err(&client->dev, "[FTS] Step 3: GET CTPM ID FAIL,ID1 = 0x%x,ID2 = 0x%x\n",
				reg_val[0], reg_val[1]);
		}
	}
	if (i >= FTS_UPGRADE_LOOP)
		return -EIO;

	auc_i2c_write_buf[0] = FTS_READ_ID_REG;
	auc_i2c_write_buf[1] = 0x00;
	auc_i2c_write_buf[2] = 0x00;
	auc_i2c_write_buf[3] = 0x00;
	auc_i2c_write_buf[4] = 0x00;
	fts_i2c_write(client, auc_i2c_write_buf, 5);

	/*Step 4:erase app and panel paramenter area*/
	PS_DBG("Step 4:erase app and panel paramenter area\n");
	auc_i2c_write_buf[0] = FTS_ERASE_APP_REG;
	fts_i2c_write(client, auc_i2c_write_buf, 1);
	msleep(fts_updateinfo_curr.delay_earse_flash);

	for(i = 0;i < 200;i++)
	{
		auc_i2c_write_buf[0] = 0x6a;
		auc_i2c_write_buf[1] = 0x00;
		auc_i2c_write_buf[2] = 0x00;
		auc_i2c_write_buf[3] = 0x00;
		reg_val[0] = 0x00;
		reg_val[1] = 0x00;
		fts_i2c_read(client, auc_i2c_write_buf, 4, reg_val, 2);
		if(0xb0 == reg_val[0] && 0x02 == reg_val[1])
		{
			PS_DBG("lcl_add==[FTS] erase app finished \n");
			break;
		}
		msleep(50);
	}

	/*********Step 5:write firmware(FW) to ctpm flash*********/
	bt_ecc = 0;
	PS_DBG("lcl_add==Step 5:write firmware(FW) to ctpm flash\n");
	dw_lenth = fw_length;
	packet_number = (dw_lenth) / FTS_PACKET_LENGTH;
	packet_buf[0] = FTS_FW_WRITE_CMD;
	packet_buf[1] = 0x00;

	for (j = 0; j < packet_number; j++)
	{
		temp = j * FTS_PACKET_LENGTH;
		packet_buf[2] = (u8) (temp >> 8);
		packet_buf[3] = (u8) temp;
		lenght = FTS_PACKET_LENGTH;
		packet_buf[4] = (u8) (lenght >> 8);
		packet_buf[5] = (u8) lenght;

		for (i = 0; i < FTS_PACKET_LENGTH; i++)
		{
			packet_buf[6 + i] = pbt_buf[j * FTS_PACKET_LENGTH + i];
			bt_ecc ^= packet_buf[6 + i];
		}

		fts_i2c_write(client, packet_buf, FTS_PACKET_LENGTH + 6);

		for(i = 0;i < 30;i++)
		{
			auc_i2c_write_buf[0] = 0x6a;
			auc_i2c_write_buf[1] = 0x00;
			auc_i2c_write_buf[2] = 0x00;
			auc_i2c_write_buf[3] = 0x00;
			reg_val[0] = 0x00;
			reg_val[1] = 0x00;
			fts_i2c_read(client, auc_i2c_write_buf, 4, reg_val, 2);
			if(0xb0 == (reg_val[0] & 0xf0) && (0x03 + (j % 0x0ffd)) == (((reg_val[0] & 0x0f) << 8) |reg_val[1]))
			{
				PS_DBG("lcl_add==[FTS] write a block data finished \n");
				break;
			}
			msleep(1);
		}
	}
	PS_DBG("lcl_add==fts_6x36_ctpm_fw_upgrade==FTS_PACKET_LENGTH =%d\n",FTS_PACKET_LENGTH);
	if ((dw_lenth) % FTS_PACKET_LENGTH > 0)
	{
		temp = packet_number * FTS_PACKET_LENGTH;
		packet_buf[2] = (u8) (temp >> 8);
		packet_buf[3] = (u8) temp;
		temp = (dw_lenth) % FTS_PACKET_LENGTH;
		packet_buf[4] = (u8) (temp >> 8);
		packet_buf[5] = (u8) temp;

		for (i = 0; i < temp; i++)
		{
			packet_buf[6 + i] = pbt_buf[packet_number * FTS_PACKET_LENGTH + i];
			bt_ecc ^= packet_buf[6 + i];
		}

		fts_i2c_write(client, packet_buf, temp + 6);

		for(i = 0;i < 30;i++)
		{
			auc_i2c_write_buf[0] = 0x6a;
			auc_i2c_write_buf[1] = 0x00;
			auc_i2c_write_buf[2] = 0x00;
			auc_i2c_write_buf[3] = 0x00;
			reg_val[0] = 0x00;
			reg_val[1] = 0x00;
			fts_i2c_read(client, auc_i2c_write_buf, 4, reg_val, 2);
			if(0xb0 == (reg_val[0] & 0xf0) && (0x03 + (j % 0x0ffd)) == (((reg_val[0] & 0x0f) << 8) |reg_val[1]))
			{
				PS_DBG("lcl_add==[FTS] write a block data finished \n");
				break;
			}
			msleep(1);
		}
	}


	/*********Step 6: read out checksum***********************/
	PS_DBG("Step 6: read out checksum\n");
	auc_i2c_write_buf[0] = FTS_REG_ECC;
	fts_i2c_read(client, auc_i2c_write_buf, 1, reg_val, 1);
	if (reg_val[0] != bt_ecc)
	{
		dev_err(&client->dev, "[FTS]--ecc error! FW=%02x bt_ecc=%02x\n",reg_val[0],bt_ecc);
		return -EIO;
	}

	/*********Step 7: reset the new FW***********************/
	PS_DBG("Step 7: reset the new FW\n");
	auc_i2c_write_buf[0] = 0x07;
	fts_i2c_write(client, auc_i2c_write_buf, 1);
	msleep(300);
	return 0;
}

/************************************************************************
* Name: fts_ctpm_get_i_file_ver
* Brief:  get .i file version
* Input: no
* Output: no
* Return: fw version
***********************************************************************/
static int fts_ctpm_get_i_file_ver(void)
{
	u16 ui_sz;

	ui_sz = sizeof(CTPM_FW);
	if (ui_sz > 2)
	{
	    if((fts_updateinfo_curr.CHIP_ID==0x36)||(fts_updateinfo_curr.CHIP_ID==0x64))
			{
                return CTPM_FW[0x10a];
			}
	    else if(fts_updateinfo_curr.CHIP_ID==0x58)
                return CTPM_FW[0x1D0A];
	    else
		return CTPM_FW[ui_sz - 2];
	}

	return 0x00;
}

/************************************************************************
* Name: fts_ctpm_fw_upgrade_with_i_file
* Brief:  upgrade with *.i file
* Input: i2c info
* Output: no
* Return: fail <0
***********************************************************************/
static int fts_ctpm_fw_upgrade_with_i_file(struct i2c_client *client)
{
	u8 *pbt_buf = NULL;
	int i_ret=0;
	int fw_len = sizeof(CTPM_FW);

	/*judge the fw that will be upgraded
	* if illegal, then stop upgrade and return.
	*/
 if ((fts_updateinfo_curr.CHIP_ID==0x36)||(fts_updateinfo_curr.CHIP_ID==0x64))
	{
		PS_DBG("lcl_add ==fts_ctpm_fw_upgrade_with_i_file == CHIP_ID=%d\n",fts_updateinfo_curr.CHIP_ID);
		PS_DBG("lcl_add ==fts_ctpm_fw_upgrade_with_i_file == fw_len=%d\n",fw_len);
		/*
		if (fw_len < 8 || fw_len > 32 * 1024)
		{
			dev_err(&client->dev, "%s:FW length error\n", __func__);
			return -EIO;
		}
		*/
		pbt_buf = CTPM_FW;
		i_ret = fts_6x36_ctpm_fw_upgrade(client, pbt_buf, sizeof(CTPM_FW));
		if (i_ret != 0)
			dev_err(&client->dev, "%s:upgrade failed. err.\n",__func__);
	}
	return i_ret;
}
/************************************************************************
* Name: fts_ctpm_auto_upgrade
* Brief:  auto upgrade
* Input: i2c info
* Output: no
* Return: 0
***********************************************************************/
static int  fts_ctpm_auto_upgrade(struct i2c_client *client)
{
	u8 uc_host_fm_ver = FTS_REG_FW_VER;
	u8 uc_tp_fm_ver;
	int i_ret;

	fts_read_reg(client, FTS_REG_FW_VER, &uc_tp_fm_ver);
	uc_host_fm_ver = fts_ctpm_get_i_file_ver();

	if (uc_tp_fm_ver == FTS_REG_FW_VER ||	uc_tp_fm_ver < uc_host_fm_ver )
	{
		msleep(100);
		dev_dbg(&client->dev, "[FTS] uc_tp_fm_ver = 0x%x, uc_host_fm_ver = 0x%x\n",uc_tp_fm_ver, uc_host_fm_ver);
		i_ret = fts_ctpm_fw_upgrade_with_i_file(client);
		if (i_ret == 0)
		{
			msleep(300);
			uc_host_fm_ver = fts_ctpm_get_i_file_ver();
		}
		else
		{
			PS_DBG("[FTS] upgrade failed ret=%d.\n", i_ret);
			return -EIO;
		}
	}

	return 0;
}
#endif

#if 0
static ssize_t ft5x0x_show_debug(struct device* cd,struct device_attribute *attr, char* buf)
{
	ssize_t ret = 0;

	sprintf(buf, "FT5206 Debug %d\n",debug_level);

	ret = strlen(buf) + 1;

	return ret;
}

static ssize_t ft5x0x_store_debug(struct device* cd, struct device_attribute *attr,
		       const char* buf, size_t len)
{
	unsigned long on_off = simple_strtoul(buf, NULL, 10);
	debug_level = on_off;

	PS_DBG("%s: debug_level=%d\n",__func__, debug_level);

	return len;
}

static ssize_t ft5x0x_show_suspend(struct device* cd,
				     struct device_attribute *attr, char* buf)
{
	ssize_t ret = 0;

	if(suspend_flag==1)
		sprintf(buf, "FT5206 Suspend\n");
	else
		sprintf(buf, "FT5206 Resume\n");

	ret = strlen(buf) + 1;

	return ret;
}

static ssize_t ft5x0x_store_suspend(struct device* cd, struct device_attribute *attr,
		       const char* buf, size_t len)
{
	unsigned long on_off = simple_strtoul(buf, NULL, 10);
	suspend_flag = on_off;

	if(on_off==1)
	{
		PS_DBG("FT5206 Entry Suspend\n");
		ft5x0x_ts_suspend(NULL);
	}
	else
	{
		PS_DBG("FT5206 Entry Resume\n");
		ft5x0x_ts_resume(NULL);
	}

	return len;
}

static ssize_t ft5x0x_show_version(struct device* cd,
				     struct device_attribute *attr, char* buf)
{
	ssize_t ret = 0;
	unsigned char uc_reg_value;

	//get some register information
	uc_reg_value = ft5x0x_read_fw_ver();

	sprintf(buf, "ft5x0x firmware id is V%x\n", uc_reg_value);

	ret = strlen(buf) + 1;

	return ret;
}

static ssize_t ft5x0x_show_vendor_version(struct device* cd,
				     struct device_attribute *attr, char* buf)
{
	ssize_t ret = 0;
	unsigned char uc_reg_value;

	//get some register information
	uc_reg_value = ft5x0x_read_vendor_ver();

	sprintf(buf, "ft5x0x vendor id is V%x\n", uc_reg_value);

	ret = strlen(buf) + 1;

	return ret;
}

static ssize_t ft5x0x_update(struct device* cd, struct device_attribute *attr,
		       const char* buf, size_t len)
{
	unsigned long on_off = simple_strtoul(buf, NULL, 10);
	unsigned char uc_reg_value;

	//get some register information
	uc_reg_value = ft5x0x_read_fw_ver();

	if(on_off==1)
	{
		PS_DBG("ft5x0x update, current firmware id is V%x\n", uc_reg_value);
		//fts_ctpm_fw_update();
//		fts_ctpm_fw_upgrade_with_i_file_old();
	}

	return len;
}

static int ft5x0x_create_sysfs(struct i2c_client *client)
{
	int err;
	struct device *dev = &(client->dev);

	PS_DBG("%s", __func__);

	err = device_create_file(dev, &dev_attr_suspend);
	err = device_create_file(dev, &dev_attr_update);
	err = device_create_file(dev, &dev_attr_vendor_id);
	err = device_create_file(dev, &dev_attr_debug);

	return err;
}
#endif

static int ft5x0x_i2c_rxdata(char *rxdata, int length)
{
	int ret;

	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= rxdata,
		},
		{
			.addr	= this_client->addr,
			.flags	= I2C_M_RD,
			.len	= length,
			.buf	= rxdata,
		},
	};

	ret = i2c_transfer(this_client->adapter, msgs, 2);
	if (ret < 0)
		pr_err("msg %s i2c read error: %d\n", __func__, ret);

	return ret;
}

static int ft5x0x_i2c_txdata(char *txdata, int length)
{
	int ret;

	struct i2c_msg msg[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= length,
			.buf	= txdata,
		},
	};

	ret = i2c_transfer(this_client->adapter, msg, 1);
	if (ret < 0)
		pr_err("%s i2c write error: %d\n", __func__, ret);

	return ret;
}
/***********************************************************************************************
Name	:	 ft5x0x_write_reg

Input	:	addr -- address
                     para -- parameter

Output	:

function	:	write register of ft5x0x

***********************************************************************************************/
static int ft5x0x_write_reg(u8 addr, u8 para)
{
	u8 buf[3];
	int ret = -1;

	buf[0] = addr;
	buf[1] = para;
	ret = ft5x0x_i2c_txdata(buf, 2);
	if (ret < 0) {
		pr_err("write reg failed! %#x ret: %d", buf[0], ret);
		return -1;
	}

	return 0;
}


/***********************************************************************************************
Name	:	ft5x0x_read_reg

Input	:	addr
                     pdata

Output	:

function	:	read register of ft5x0x

***********************************************************************************************/
static int ft5x0x_read_reg(u8 addr, u8 *pdata)
{
	int ret;
	u8 buf[2] = {0};
	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= buf,
		},
		{
			.addr	= this_client->addr,
			.flags	= I2C_M_RD,
			.len	= 1,
			.buf	= buf,
		},
	};
	buf[0] = addr;
	ret = i2c_transfer(this_client->adapter, msgs, 2);
	if (ret < 0){
		pr_err("msg %s i2c read error: %d\n", __func__, ret);
		return ret;
	}
	*pdata = buf[0];
	return ret;
}

int ft5x0x_i2c_read(struct i2c_client *client, char *writebuf,
            int writelen, char *readbuf, int readlen)
{
    int ret;

    if (writelen > 0) {
        struct i2c_msg msgs[] = {
            {
             .addr = client->addr,
             .flags = 0,
             .len = writelen,
             .buf = writebuf,
             },
            {
             .addr = client->addr,
             .flags = I2C_M_RD,
             .len = readlen,
             .buf = readbuf,
             },
        };
        ret = i2c_transfer(client->adapter, msgs, 2);
        if (ret < 0)
            dev_err(&client->dev, "f%s: i2c read error.\n",
                __func__);
    } else {
        struct i2c_msg msgs[] = {
            {
             .addr = client->addr,
             .flags = I2C_M_RD,
             .len = readlen,
             .buf = readbuf,
             },
        };
        ret = i2c_transfer(client->adapter, msgs, 1);
        if (ret < 0)
            dev_err(&client->dev, "%s:i2c read error.\n", __func__);
    }
    return ret;
}

#ifdef CONFIG_TP_GESTURE
static int tpd_gesture_handle(void)
{
    //poll=0;
    //unsigned char buf[FTS_GESTRUE_POINTS * 2] = { 0 };
    unsigned char buf[FTS_GESTRUE_POINTS * 4] = { 0 };
    int ret = -1;
    //int i = 0;
	int keycode = 0;
	int gesture_id = 0;
    unsigned char ruby;
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	PS_DBG("ruby:%s\n",__func__);
	buf[0] = 0xd3;
	//ft5x0x_write_reg(0xd0, 0x1);

	ft5x0x_read_reg(0xd0, &ruby);
	PS_DBG("==ruby==   ruby=0x%x\n",ruby);


    //ret = ft5x0x_i2c_read(data->client, buf, 1, buf, FTS_GESTRUE_POINTS);
    ret = ft5x0x_i2c_read(data->client, buf, 1, buf, 8);

    if (ret < 0)
    {
        dev_err(&data->client->dev, "===error===   %s read touchdata failed.\n",            __func__);
        return ret;
    }
    /* FW ֱ\BDӸ\F8\B3\F6\CA\D6\CA\C6 */

	//for(i=0;i<10;i++)
	//	PS_DBG("[ruby][tpd_gesture_handle]  buf[%d]=%x\n",i,buf[i]);


    if (0x24 == buf[0])
    {
		PS_DBG("double click\n");
        gesture_id = 0x24;
	   input_report_key(data->input_dev, KEY_U, 1);
       input_sync(data->input_dev);
	   input_report_key(data->input_dev, KEY_U, 0);
       input_sync(data->input_dev);
    	//poll=0;
        return 0;
    }

    pointnum = (short)(buf[1]) & 0xff;
    buf[0] = 0xd3;
    ret = ft5x0x_i2c_read(data->client, buf, 1, buf,(pointnum * 4 + 8));

	printk(KERN_EMERG "ft6336G gscode=%d ", buf[0]);
	switch(buf[0])
	{
		case GESTURE_LEFT:
		case GESTURE_RIGHT:
		case GESTURE_UP:
		case GESTURE_DOWN:
		case GESTURE_O:
		case GESTURE_W:
		case GESTURE_M:
		case GESTURE_E:
		case GESTURE_C:
  		case GESTURE_Z:
		case GESTURE_Z2:
		case GESTURE_S:
		case GESTURE_V:
		case KEY_POWER:
			gesture_id = buf[0];
			break;
		default:
			gesture_id = fetch_object_sample(buf,pointnum);
			break;
	}

	switch(gesture_id)
	{
		case GESTURE_LEFT:
			keycode =KEY_F12;
			PS_DBG("xie KEY_LEFT ");
			break;
		case GESTURE_RIGHT:
			keycode = KEY_F11;
			PS_DBG("xie KEY_RIGHT ");
			break;
		case GESTURE_UP:
			keycode = KEY_F10;
			PS_DBG("xie KEY_UP");
			break;
		case GESTURE_DOWN:
			keycode = KEY_F9;
			PS_DBG("xie KEY_DOWN ");
			break;
		case GESTURE_O:
			keycode = KEY_O;
			PS_DBG("xie KEY_O ");
			break;
		case GESTURE_W:
			keycode = KEY_W;
			PS_DBG("xie KEY_W");
			break;
		case GESTURE_M:
			keycode = KEY_M;
			PS_DBG("xie KEY_M");
			break;
		case GESTURE_E:
			keycode = KEY_E;
			PS_DBG("xie KEY_E");
			break;
		case GESTURE_C:
			keycode = KEY_C;
			PS_DBG("xie KEY_C");
			break;
		case KEY_POWER:
			keycode = KEY_POWER;
			PS_DBG("xie KEY_POWER");
			break;
		case GESTURE_Z:
		case GESTURE_Z2:
			keycode = KEY_Z;
			PS_DBG("xie KEY_Z");
			break;
		case GESTURE_S:
			keycode = KEY_S;
			PS_DBG("xie KEY_S");
			break;
            	case GESTURE_V:
			keycode = KEY_V;
			PS_DBG("xie KEY_V");
			break;

		default:
			keycode = KEY_F8;
			PS_DBG("xie default");
			break;
	}

	PS_DBG("[ruby] keycode=%d,gesture_id=%x\n",keycode,gesture_id);

	if(keycode > 0 && (KEY_F8 != keycode)){

	   input_report_key(data->input_dev, keycode, 1);
       input_sync(data->input_dev);
	   input_report_key(data->input_dev, keycode, 0);
       input_sync(data->input_dev);
    }
	msleep(300);
      //  poll=1;
	return 0;
}
#endif


#ifdef TOUCH_VIRTUAL_KEYS

static ssize_t virtual_keys_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	struct ft5x0x_ts_platform_data *pdata = data->platform_data;
	#if defined(CONFIG_TOUCH_VIRTUAL_KEYS_APPSELECT)
	return sprintf(buf,"%s:%s:%d:%d:%d:%d:%s:%s:%d:%d:%d:%d:%s:%s:%d:%d:%d:%d\n"
		,__stringify(EV_KEY), __stringify(KEY_APPSELECT),pdata ->virtualkeys[0],pdata ->virtualkeys[1],pdata ->virtualkeys[2],pdata ->virtualkeys[3]
		,__stringify(EV_KEY), __stringify(KEY_HOMEPAGE),pdata ->virtualkeys[4],pdata ->virtualkeys[5],pdata ->virtualkeys[6],pdata ->virtualkeys[7]
		,__stringify(EV_KEY), __stringify(KEY_BACK),pdata ->virtualkeys[8],pdata ->virtualkeys[9],pdata ->virtualkeys[10],pdata ->virtualkeys[11]);
	#else
	return sprintf(buf,"%s:%s:%d:%d:%d:%d:%s:%s:%d:%d:%d:%d:%s:%s:%d:%d:%d:%d\n"
		,__stringify(EV_KEY), __stringify(KEY_MENU),pdata ->virtualkeys[0],pdata ->virtualkeys[1],pdata ->virtualkeys[2],pdata ->virtualkeys[3]
		,__stringify(EV_KEY), __stringify(KEY_HOMEPAGE),pdata ->virtualkeys[4],pdata ->virtualkeys[5],pdata ->virtualkeys[6],pdata ->virtualkeys[7]
		,__stringify(EV_KEY), __stringify(KEY_BACK),pdata ->virtualkeys[8],pdata ->virtualkeys[9],pdata ->virtualkeys[10],pdata ->virtualkeys[11]);
	#endif
}

static struct kobj_attribute virtual_keys_attr = {
    .attr = {
        .name = "virtualkeys.ft5x0x_ts",
        .mode = S_IRUGO,
    },
    .show = &virtual_keys_show,
};

static struct attribute *properties_attrs[] = {
    &virtual_keys_attr.attr,
    NULL
};

static struct attribute_group properties_attr_group = {
    .attrs = properties_attrs,
};

static void ft5x0x_ts_virtual_keys_init(void)
{
    int ret;
    struct kobject *properties_kobj;

//    TS_DBG("%s\n",__func__);

    properties_kobj = kobject_create_and_add("board_properties", NULL);
    if (properties_kobj)
        ret = sysfs_create_group(properties_kobj,
                     &properties_attr_group);
    if (!properties_kobj || ret)
        pr_err("failed to create board_properties\n");
}

#endif

/***********************************************************************************************
Name	:	 ft5x0x_read_fw_ver

Input	:	 void

Output	:	 firmware version

function	:	 read TP firmware version

***********************************************************************************************/
static unsigned char ft5x0x_read_fw_ver(void)
{
	unsigned char ver = -1;
	ft5x0x_read_reg(FT5X0X_REG_FIRMID, &ver);
	return(ver);
}
 #if 0
static unsigned char ft5x0x_read_vendor_ver(void)
{
	unsigned char vensor_ver = -1;
	ft5x0x_read_reg(FT5X0X_REG_FT5201ID, &vensor_ver);
	return(vensor_ver);
}
#endif

#if(FT6206_PROXIMITY)
static int ft6206_proximity_open(void)
{
	int ret;

	PS_DBG("2013.10.11 wsy %s misc_opened =%d\n", __func__,misc_opened);
	if (misc_opened)
		return -EBUSY;
	misc_opened = 1;
	ft6206_proximity_start = 1;

	ft5x0x_write_reg(0xb0,0x01);

	return ret;
}

static int ft6206_proximity_release(void)
{
	int ret = 0;

	PS_DBG("2013.10.11 wsy %s misc_opened =%d\n", __func__,misc_opened);
	////if (0 == misc_opened)
	////	return -EBUSY;

	misc_opened = 0;
	ft6206_proximity_start = 0;

	ft5x0x_write_reg(0xb0,0x00);

	return ret;
}

static long ft6206_proximity_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	PS_DBG("%s cmd %d\n", __func__, _IOC_NR(cmd));
	PS_DBG("2013.10.11 wsy ft6206_proximity_ioctl cmd =0x%x\n",cmd);
	switch (cmd) {
	case LTR_IOCTL_PROX_ON:
		ft6206_proximity_open();
		break;
	case LTR_IOCTL_PROX_OFF:
		ft6206_proximity_release();
		break;
	default:
		pr_err("%s: invalid cmd %d\n", __func__, _IOC_NR(cmd));
		return -EINVAL;
	}
	return 0;
}
static struct file_operations ft6206_proximity_fops = {
	.owner = THIS_MODULE,
	.open = NULL,//gtp_proximity_open,
	.release = NULL,//gtp_proximity_release,
	.unlocked_ioctl = ft6206_proximity_ioctl
};

struct miscdevice ft6206_proximity_misc = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = PROXIMITY_DEVICE,						//match the hal's name
	.fops = &ft6206_proximity_fops
};

static int ft6206_report_psensor_value(void)
{
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	u8 buf[32] = {0};
	int ret = -1;
	u8 psensor_data_status = 0;

	#ifdef CONFIG_FT5X0X_MULTITOUCH
	ret = ft5x0x_i2c_rxdata(buf, POINT_READ_BUF);
	#else
	ret = ft5x0x_i2c_rxdata(buf, 7);
	#endif
	PS_DBG("2013.10.10 wsy 0 report_psensor ret=%d\n",ret);
	if (ret < 0)
	{
		PS_DBG("%s failed: %d\n", __func__, ret);
		return ret;
	}

	psensor_data_status= buf[1]&0xf0;

	PS_DBG("%s,line=%d,psensor_data_status=%x\n",__func__,__LINE__,psensor_data_status);

	ft5x0x_read_reg(0x01, &psensor_data_status);

	PS_DBG("%s,line=%d,psensor_data_status=%x\n",__func__,__LINE__,psensor_data_status);

	PS_DBG("2013.10.10 wsy 2 report_psensor psensor_data_status=%x\n",psensor_data_status);
	switch (psensor_data_status) {
		case 0xe0: //far
			PS_DBG("proximity is far\n");
			input_report_abs(data->input_dev, ABS_DISTANCE, 1);
			//2013.10.15 wsy edit for remove it in func:ft5x0x_ts_pen_irq_work input_sync(data->input_dev);
			ret = 0;
			break;

		case 0xc0: //near
			PS_DBG("proximity is near\n");
			input_report_abs(data->input_dev, ABS_DISTANCE, 0);
			//2013.10.15 wsy edit for remove it in func:ft5x0x_ts_pen_irq_work input_sync(data->input_dev);
			ret = 0;
			break;
		default:
			break;
		}

	return ret;
}

#endif



//#define CONFIG_SUPPORT_FTS_CTP_UPG


#ifdef CONFIG_SUPPORT_FTS_CTP_UPG

typedef enum
{
    ERR_OK,
    ERR_MODE,
    ERR_READID,
    ERR_ERASE,
    ERR_STATUS,
    ERR_ECC,
    ERR_DL_ERASE_FAIL,
    ERR_DL_PROGRAM_FAIL,
    ERR_DL_VERIFY_FAIL
}E_UPGRADE_ERR_TYPE;

typedef unsigned char         FTS_BYTE;     //8 bit
typedef unsigned short        FTS_WORD;    //16 bit
typedef unsigned int          FTS_DWRD;    //16 bit
typedef unsigned char         FTS_BOOL;    //8 bit

#define FTS_NULL                0x0
#define FTS_TRUE                0x01
#define FTS_FALSE              0x0

#define I2C_CTPM_ADDRESS       0x70

/*
[function]:
    callback: read data from ctpm by i2c interface,implemented by special user;
[parameters]:
    bt_ctpm_addr[in]    :the address of the ctpm;
    pbt_buf[out]        :data buffer;
    dw_lenth[in]        :the length of the data buffer;
[return]:
    FTS_TRUE     :success;
    FTS_FALSE    :fail;
*/
FTS_BOOL i2c_read_interface(FTS_BYTE bt_ctpm_addr, FTS_BYTE* pbt_buf, FTS_DWRD dw_lenth)
{
    int ret;

    ret=i2c_master_recv(this_client, pbt_buf, dw_lenth);

    if(ret<=0)
    {
        printk("[TSP]i2c_read_interface error\n");
        return FTS_FALSE;
    }

    return FTS_TRUE;
}

/*
[function]:
    callback: write data to ctpm by i2c interface,implemented by special user;
[parameters]:
    bt_ctpm_addr[in]    :the address of the ctpm;
    pbt_buf[in]        :data buffer;
    dw_lenth[in]        :the length of the data buffer;
[return]:
    FTS_TRUE     :success;
    FTS_FALSE    :fail;
*/
FTS_BOOL i2c_write_interface(FTS_BYTE bt_ctpm_addr, FTS_BYTE* pbt_buf, FTS_DWRD dw_lenth)
{
    int ret;
    ret=i2c_master_send(this_client, pbt_buf, dw_lenth);
    if(ret<=0)
    {
        printk("[TSP]i2c_write_interface error line = %d, ret = %d\n", __LINE__, ret);
        return FTS_FALSE;
    }

    return FTS_TRUE;
}

/*
[function]:
    send a command to ctpm.
[parameters]:
    btcmd[in]        :command code;
    btPara1[in]    :parameter 1;
    btPara2[in]    :parameter 2;
    btPara3[in]    :parameter 3;
    num[in]        :the valid input parameter numbers, if only command code needed and no parameters followed,then the num is 1;
[return]:
    FTS_TRUE    :success;
    FTS_FALSE    :io fail;
*/
FTS_BOOL cmd_write(FTS_BYTE btcmd,FTS_BYTE btPara1,FTS_BYTE btPara2,FTS_BYTE btPara3,FTS_BYTE num)
{
    FTS_BYTE write_cmd[4] = {0};

    write_cmd[0] = btcmd;
    write_cmd[1] = btPara1;
    write_cmd[2] = btPara2;
    write_cmd[3] = btPara3;
    return i2c_write_interface(I2C_CTPM_ADDRESS, write_cmd, num);
}

/*
[function]:
    write data to ctpm , the destination address is 0.
[parameters]:
    pbt_buf[in]    :point to data buffer;
    bt_len[in]        :the data numbers;
[return]:
    FTS_TRUE    :success;
    FTS_FALSE    :io fail;
*/
FTS_BOOL byte_write(FTS_BYTE* pbt_buf, FTS_DWRD dw_len)
{
    return i2c_write_interface(I2C_CTPM_ADDRESS, pbt_buf, dw_len);
}

/*
[function]:
    read out data from ctpm,the destination address is 0.
[parameters]:
    pbt_buf[out]    :point to data buffer;
    bt_len[in]        :the data numbers;
[return]:
    FTS_TRUE    :success;
    FTS_FALSE    :io fail;
*/
FTS_BOOL byte_read(FTS_BYTE* pbt_buf, FTS_BYTE bt_len)
{
    return i2c_read_interface(I2C_CTPM_ADDRESS, pbt_buf, bt_len);
}


/*
[function]:
    burn the FW to ctpm.
[parameters]:(ref. SPEC)
    pbt_buf[in]    :point to Head+FW ;
    dw_lenth[in]:the length of the FW + 6(the Head length);
    bt_ecc[in]    :the ECC of the FW
[return]:
    ERR_OK        :no error;
    ERR_MODE    :fail to switch to UPDATE mode;
    ERR_READID    :read id fail;
    ERR_ERASE    :erase chip fail;
    ERR_STATUS    :status error;
    ERR_ECC        :ecc error.
*/


#define    FTS_PACKET_LENGTH        128

#if 1
static unsigned char CTPM_FW[]=
{
#include "ft5306_qHD.i"
};
#endif

E_UPGRADE_ERR_TYPE  fts_ctpm_fw_upgrade(FTS_BYTE* pbt_buf, FTS_DWRD dw_lenth)
{
    FTS_BYTE reg_val[2] = {0};
    FTS_DWRD i = 0;

    FTS_DWRD  packet_number;
    FTS_DWRD  j;
    FTS_DWRD  temp;
    FTS_DWRD  lenght;
    FTS_BYTE  packet_buf[FTS_PACKET_LENGTH + 6];
    FTS_BYTE  auc_i2c_write_buf[10];
    FTS_BYTE bt_ecc;
    int      i_ret;

    /*********Step 1:Reset  CTPM *****/
    /*write 0xaa to register 0xfc*/
    ft5x0x_write_reg(0xfc,0xaa);
    msleep(50);
     /*write 0x55 to register 0xfc*/
    ft5x0x_write_reg(0xfc,0x55);
    PS_DBG("[TSP] Step 1: Reset CTPM test, bin-length=%d\n",dw_lenth);

    msleep(10);

    /*********Step 2:Enter upgrade mode *****/
    auc_i2c_write_buf[0] = 0x55;
    auc_i2c_write_buf[1] = 0xaa;
    do
    {
        i ++;
        i_ret = ft5x0x_i2c_txdata(auc_i2c_write_buf, 2);
        msleep(5);
    }while(i_ret <= 0 && i < 5 );
    /*********Step 3:check READ-ID***********************/
    cmd_write(0x90,0x00,0x00,0x00,4);
    byte_read(reg_val,2);
    if (reg_val[0] == 0x79 && reg_val[1] == 0x3)
    {
        PS_DBG("[TSP] Step 3: CTPM ID,ID1 = 0x%x,ID2 = 0x%x\n",reg_val[0],reg_val[1]);
    }
    else
    {
        printk("%s: ERR_READID, ID1 = 0x%x,ID2 = 0x%x\n", __func__,reg_val[0],reg_val[1]);
        return ERR_READID;
        //i_is_new_protocol = 1;
    }

     /*********Step 4:erase app*******************************/
    cmd_write(0x61,0x00,0x00,0x00,1);

    msleep(20);
    PS_DBG("[TSP] Step 4: erase.\n");

    /*********Step 5:write firmware(FW) to ctpm flash*********/
    bt_ecc = 0;
    PS_DBG("[TSP] Step 5: start upgrade.\n");
    dw_lenth = dw_lenth - 8;
    packet_number = (dw_lenth) / FTS_PACKET_LENGTH;
    packet_buf[0] = 0xbf;
    packet_buf[1] = 0x00;
    for (j=0;j<packet_number;j++)
    {
        temp = j * FTS_PACKET_LENGTH;
        packet_buf[2] = (FTS_BYTE)(temp>>8);
        packet_buf[3] = (FTS_BYTE)temp;
        lenght = FTS_PACKET_LENGTH;
        packet_buf[4] = (FTS_BYTE)(lenght>>8);
        packet_buf[5] = (FTS_BYTE)lenght;

        for (i=0;i<FTS_PACKET_LENGTH;i++)
        {
            packet_buf[6+i] = pbt_buf[j*FTS_PACKET_LENGTH + i];
            bt_ecc ^= packet_buf[6+i];
        }

        byte_write(&packet_buf[0],FTS_PACKET_LENGTH + 6);
        msleep(FTS_PACKET_LENGTH/6 + 1);
        if ((j * FTS_PACKET_LENGTH % 1024) == 0)
        {
              PS_DBG("[TSP] upgrade the 0x%x th byte.\n", ((unsigned int)j) * FTS_PACKET_LENGTH);
        }
    }

    if ((dw_lenth) % FTS_PACKET_LENGTH > 0)
    {
        temp = packet_number * FTS_PACKET_LENGTH;
        packet_buf[2] = (FTS_BYTE)(temp>>8);
        packet_buf[3] = (FTS_BYTE)temp;

        temp = (dw_lenth) % FTS_PACKET_LENGTH;
        packet_buf[4] = (FTS_BYTE)(temp>>8);
        packet_buf[5] = (FTS_BYTE)temp;

        for (i=0;i<temp;i++)
        {
            packet_buf[6+i] = pbt_buf[ packet_number*FTS_PACKET_LENGTH + i];
            bt_ecc ^= packet_buf[6+i];
        }

        byte_write(&packet_buf[0],temp+6);
        msleep(20);
    }

    //send the last six byte
    for (i = 0; i<6; i++)
    {
        temp = 0x6ffa + i;
        packet_buf[2] = (FTS_BYTE)(temp>>8);
        packet_buf[3] = (FTS_BYTE)temp;
        temp =1;
        packet_buf[4] = (FTS_BYTE)(temp>>8);
        packet_buf[5] = (FTS_BYTE)temp;
        packet_buf[6] = pbt_buf[ dw_lenth + i];
        bt_ecc ^= packet_buf[6];

        byte_write(&packet_buf[0],7);
        msleep(20);
    }

    /*********Step 6: read out checksum***********************/
    /*send the opration head*/
    cmd_write(0xcc,0x00,0x00,0x00,1);
    byte_read(reg_val,1);
    PS_DBG("[TSP] Step 6:  ecc read 0x%x, new firmware 0x%x. \n", reg_val[0], bt_ecc);
    if(reg_val[0] != bt_ecc)
    {
        printk("%s: ERR_ECC\n", __func__);
        return ERR_ECC;
    }

    /*********Step 7: reset the new FW***********************/
    cmd_write(0x07,0x00,0x00,0x00,1);

    return ERR_OK;
}

int fts_ctpm_auto_clb(void)
{
    unsigned char uc_temp;
    unsigned char i ;

    PS_DBG("[FTS] start auto CLB.\n");
    msleep(20);
    ft5x0x_write_reg(0, 0x40);
    msleep(10);   //make sure already enter factory mode
    ft5x0x_write_reg(2, 0x4);  //write command to start calibration
    msleep(30);
    for(i=0;i<100;i++)
    {
        ft5x0x_read_reg(0,&uc_temp);
        if ( ((uc_temp&0x70)>>4) == 0x0)  //return to normal mode, calibration finish
        {
            break;
        }
        msleep(20);
        PS_DBG("[FTS] waiting calibration %d\n",i);
    }
    PS_DBG("[FTS] calibration OK.\n");

    msleep(30);
    ft5x0x_write_reg(0, 0x40);  //goto factory mode
    msleep(10);   //make sure already enter factory mode
    ft5x0x_write_reg(2, 0x5);  //store CLB result
    msleep(30);
    ft5x0x_write_reg(0, 0x0); //return to normal mode
    msleep(30);
    PS_DBG("[FTS] store CLB result OK.\n");
    return 0;
}

#if 0
static int fts_ctpm_fw_upgrade_with_i_file_old(void)
{
   FTS_BYTE*     pbt_buf = FTS_NULL;
   int i_ret;

    //=========FW upgrade========================*/
   pbt_buf = CTPM_FW;
   /*call the upgrade function*/
   i_ret =  fts_ctpm_fw_upgrade(pbt_buf,sizeof(CTPM_FW));
   if (i_ret != 0)
   {
	printk("[FTS] upgrade failed i_ret = %d.\n", i_ret);
       //error handling ...
       //TBD
   }
   else
   {
       PS_DBG("[FTS] upgrade successfully.\n");
       fts_ctpm_auto_clb();  //start auto CLB
   }

   return i_ret;
}
#endif

#if 0
static int fts_ctpm_fw_update(void)
{
    int ret = 0;
    const struct firmware *fw;
    unsigned char *fw_buf;
    struct platform_device *pdev;

    pdev = platform_device_register_simple("ft5206_ts", 0, NULL, 0);
    if (IS_ERR(pdev)) {
        printk("%s: Failed to register firmware\n", __func__);
        return -1;
    }

    ret = request_firmware(&fw, "ft5306_fw.bin", &pdev->dev);
    if (ret) {
		printk("%s: request_firmware error\n",__func__);
		platform_device_unregister(pdev);
        return -1;
    }

    platform_device_unregister(pdev);
    PS_DBG("%s:fw size=%d\n", __func__,fw->size);

    fw_buf = kzalloc(fw->size, GFP_KERNEL | GFP_DMA);
    memcpy(fw_buf, fw->data, fw->size);

    fts_ctpm_fw_upgrade(fw_buf, fw->size);

    PS_DBG("%s: update finish\n", __func__);
    release_firmware(fw);
    kfree(fw_buf);

    return 0;
}
#endif
#endif

static void ft5x0x_ts_release(void)
{
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
#ifdef CONFIG_FT5X0X_MULTITOUCH
	//input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, 0);
	input_report_key(data->input_dev, BTN_TOUCH, 0);
#else
	input_report_abs(data->input_dev, ABS_PRESSURE, 0);
	input_report_key(data->input_dev, BTN_TOUCH, 0);
#endif
	input_mt_sync(data->input_dev);
	PS_DBG("%s",__func__);
}

#define QHD2FWVA(x) {x <<= 3; x /= 9;}

static int ft5x0x_read_data(void)
{
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	struct ts_event *event = &data->event;
//	u8 buf[14] = {0};
	u8 buf[32] = {0};
	int ret = -1;

#ifdef CONFIG_FT5X0X_MULTITOUCH
//	ret = ft5x0x_i2c_rxdata(buf, 13);
	ret = ft5x0x_i2c_rxdata(buf, 31);
#else
    ret = ft5x0x_i2c_rxdata(buf, 7);
#endif
    if (ret < 0) {
		printk("%s read_data i2c_rxdata failed: %d\n", __func__, ret);
		return ret;
	}

	memset(event, 0, sizeof(struct ts_event));
//	event->touch_point = buf[2] & 0x03;// 0000 0011
	event->touch_point = buf[2] & 0x07;// 000 0111

#if CFG_DETECT_UP_EVENT
_sui_last_point_cnt = event->touch_point;
#endif

    if (event->touch_point == 0) {
        ft5x0x_ts_release();
        return 1;
    }

#ifdef CONFIG_FT5X0X_MULTITOUCH
    switch (event->touch_point) {
		case 5:
			event->x5 = (s16)(buf[0x1b] & 0x0F)<<8 | (s16)buf[0x1c];
			event->y5 = (s16)(buf[0x1d] & 0x0F)<<8 | (s16)buf[0x1e];
			PS_DBG("===x5 = %d,y5 = %d ====",event->x5,event->y5);
		case 4:
			event->x4 = (s16)(buf[0x15] & 0x0F)<<8 | (s16)buf[0x16];
			event->y4 = (s16)(buf[0x17] & 0x0F)<<8 | (s16)buf[0x18];
			PS_DBG("===x4 = %d,y4 = %d ====",event->x4,event->y4);
		case 3:
			event->x3 = (s16)(buf[0x0f] & 0x0F)<<8 | (s16)buf[0x10];
			event->y3 = (s16)(buf[0x11] & 0x0F)<<8 | (s16)buf[0x12];
			PS_DBG("===x3 = %d,y3 = %d ====",event->x3,event->y3);
		case 2:
			event->x2 = (s16)(buf[9] & 0x0F)<<8 | (s16)buf[10];
			event->y2 = (s16)(buf[11] & 0x0F)<<8 | (s16)buf[12];
			PS_DBG("===x2 = %d,y2 = %d ====",event->x2,event->y2);
		case 1:
			event->x1 = (s16)(buf[3] & 0x0F)<<8 | (s16)buf[4];
			event->y1 = (s16)(buf[5] & 0x0F)<<8 | (s16)buf[6];
		#if defined(TC4_PROJECT_OLED_LCD)
			if(event->y1 < 800){
				event->y1 = event->y1 * 639 / 799;
			}
		#endif
			PS_DBG("===x1 = %d,y1 = %d ====",event->x1,event->y1);
            break;
		default:
		    return -1;
	}
#else
    if (event->touch_point == 1) {
        event->x1 = (s16)(buf[3] & 0x0F)<<8 | (s16)buf[4];
        event->y1 = (s16)(buf[5] & 0x0F)<<8 | (s16)buf[6];
    }
#endif
    event->pressure = 200;

	//TS_DBG("%d (%d, %d), (%d, %d)\n", event->touch_point, event->x1, event->y1, event->x2, event->y2);
#if TS_DATA_THRESHOLD_CHECK
	#ifdef CONFIG_FT5X0X_MULTITOUCH
		if((event->x1>TS_WIDTH_MAX || event->y1>TS_HEIGHT_MAX)||
		   (event->x2>TS_WIDTH_MAX || event->y2>TS_HEIGHT_MAX)||
		   (event->x3>TS_WIDTH_MAX || event->y3>TS_HEIGHT_MAX)||
		   (event->x4>TS_WIDTH_MAX || event->y4>TS_HEIGHT_MAX)||
		   (event->x5>TS_WIDTH_MAX || event->y5>TS_HEIGHT_MAX)) {
				//TS_DBG("%s: get dirty data x1=%d,y1=%d\n",__func__, event->x1, event->y1);
				return -1;
		}
	#else
		if(event->x1>TS_WIDTH_MAX || event->y1>TS_HEIGHT_MAX){
				//TS_DBG("%s: get dirty data x1=%d,y1=%d\n",__func__, event->x1, event->y1);
				return -1;
		}
	#endif
#endif

	return 0;
}

static void ft5x0x_report_value(void)
{
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	struct ts_event *event = &data->event;
	int report_back=0;
	int report_home=0;
	int report_menu=0;
	if(event->x1 > 90 && event->x1 < 210 && event->y1 > 1270 && event->y1 < 1370)
		report_menu=1;

	if(event->x1 > 300 && event->x1 < 420 && event->y1 > 1270 && event->y1 < 1370)
		report_home=1;

	if(event->x1 > 600 && event->x1 < 720 && event->y1 > 1270 && event->y1 < 1370)
		report_back=1;

	if(report_back==1)
	{
		report_back=0;
		input_report_key(data->input_dev, KEY_BACK, 1);
		input_sync(data->input_dev);
		input_report_key(data->input_dev, KEY_BACK, 0);
		input_sync(data->input_dev);
		event->x1 =0;
		event->y1 =0;
		msleep(200);
		return;
	}

	if(report_home==1)
	{
		report_home=0;
		input_report_key(data->input_dev, KEY_HOMEPAGE, 1);
		input_sync(data->input_dev);
		input_report_key(data->input_dev, KEY_HOMEPAGE, 0);
		input_sync(data->input_dev);
		event->x1 =0;
		event->y1 =0;
		msleep(200);
		return;
	}

	if(report_menu==1)
	{
		report_menu=0;
		input_report_key(data->input_dev, KEY_MENU, 1);
		input_sync(data->input_dev);
		input_report_key(data->input_dev, KEY_MENU, 0);
		input_sync(data->input_dev);
		event->x1 =0;
		event->y1 =0;
		msleep(200);
		return;
	}
	if(event->touch_point)
		input_report_key(data->input_dev, BTN_TOUCH, 1);

#ifdef CONFIG_FT5X0X_MULTITOUCH
	switch(event->touch_point) {
		case 5:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x5);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y5);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);

		case 4:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x4);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y4);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);

		case 3:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x3);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y3);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);

		case 2:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x2);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y2);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);
		case 1:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x1);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y1);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);
		default:
//			TS_DBG("==touch_point default =\n");
			break;
	}
#else	/* CONFIG_FT5X0X_MULTITOUCH*/
	if (event->touch_point == 1) {
		input_report_abs(data->input_dev, ABS_X, event->x1);
		input_report_abs(data->input_dev, ABS_Y, event->y1);
		input_report_abs(data->input_dev, ABS_PRESSURE, event->pressure);
	}
	input_report_key(data->input_dev, BTN_TOUCH, 1);
#endif	/* CONFIG_FT5X0X_MULTITOUCH*/
	input_sync(data->input_dev);
}	/*end ft5x0x_report_value*/

static void ft5x0x_ts_pen_irq_work(struct work_struct *work)
{
	struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	//struct ts_event *event = &data->event;

	int ret = -1;

//	TS_DBG("==work 1=\n");
	if(0 ==suspend_flag ){
		ret = ft5x0x_read_data();
		if (ret == 0) {
			ft5x0x_report_value();
			}
		else printk("data package read error\n");

//2013.10.11 wsy add for p-sensor control
#if(FT6206_PROXIMITY)
		if (1 == ft6206_proximity_start)
		{
			ft6206_report_psensor_value();
		}

#endif
		input_sync(data->input_dev);
	}
	else{
#ifdef CONFIG_TP_GESTURE
	mdelay(200);
	tpd_gesture_handle();
	poll=1;
//	TS_DBG("========[ruby1]=======  irq\n");
	mdelay(300);

    wake_unlock(&ft_poll_wake_lock);
#endif
	}

	enable_irq(this_client->irq);
	msleep(10);

}

static irqreturn_t ft5x0x_ts_interrupt(int irq, void *dev_id)
{
	//int ret = -1;
	struct ft5x0x_ts_data *ft5x0x_ts = (struct ft5x0x_ts_data *)dev_id;


	#if CFG_DETECT_UP_EVENT
	del_timer(&_st_up_evnet_timer);
	_st_up_evnet_timer.expires = jiffies + 3*HZ/10; //300ms
	add_timer(&_st_up_evnet_timer);
	#endif

	disable_irq_nosync(this_client->irq);

	if(suspend_flag == 1)
	{
//disable_irq(irq);
#ifdef CONFIG_TP_GESTURE
		if(poll==1)
		{
    			wake_lock(&ft_poll_wake_lock);
			queue_work(ft5x0x_ts->ts_workqueue,&(ft5x0x_ts->pen_event_work));
		}
#else
	enable_irq(irq);
#endif
//enable_irq(irq);
	}
	else{
		if (!work_pending(&ft5x0x_ts->pen_event_work)) {
			queue_work(ft5x0x_ts->ts_workqueue, &ft5x0x_ts->pen_event_work);
		}
		else{
			enable_irq(this_client->irq);
//			msleep(10);
		}
		//TS_DBG("==int=, 11irq=%d\n", this_client->irq);
	}

	return IRQ_HANDLED;

}


#if CFG_DETECT_UP_EVENT
static void ft5x06_force_read_up_event(unsigned long data)
{
    if (_sui_last_point_cnt > 0)
    {
        PS_DBG("[FTS] miss up envent, force send here.\n");
        ft5x0x_ts_release();
    }
}
#endif

void ft5x0x_ts_reset(void)
{
	struct ft5x0x_ts_platform_data *pdata = g_ft5x0x_ts->platform_data;

	gpio_direction_output(pdata->reset_gpio_number, 1);
	printk("QHS rst_gpio : %d...\n",pdata->reset_gpio_number);
	msleep(30);
	gpio_direction_output(pdata->reset_gpio_number, 0);
	msleep(30);
	gpio_direction_output(pdata->reset_gpio_number, 1);
	msleep(30);
}

#ifdef CONFIG_TP_GESTURE
static int s_gesture_switch = 1;
static int ft5x0x_ts_check_gesture(void)
{
	struct file *fp = NULL;
	mm_segment_t old_fs;
	int err = 0;
	old_fs = get_fs();
	set_fs(KERNEL_DS);

	fp = filp_open("/productinfo/gesture_switch", O_RDONLY , 0);
	if (IS_ERR(fp))
	{
		err = PTR_ERR(fp);
		printk("open file gesture_switch error=%d!\n",err);
	}
	else{
		char buf[1]={0};
		loff_t pos = 0;
		ssize_t m_reads=0;

		m_reads = vfs_read(fp,buf,sizeof(buf),&pos);
		//printk(KERN_EMERG"open file %s success! buf[0]=%x pos:%d m_reads=%d\n", GESTURE_FUNCTION_SWITCH,buf[0],pos,m_reads);
		printk("open file gesture_switch success! buf[0]=0x%x \n", buf[0]);
		if(m_reads > 0){
			if(buf[0] == '1'){
				printk("tp gseture opened!");
				s_gesture_switch = 1;
			}
			else{
				printk("tp gseture closed!");
				s_gesture_switch = 0;
			}
		}
		else{
			s_gesture_switch = 1;
		}
		filp_close(fp, NULL);
	}

	set_fs(old_fs);
	return s_gesture_switch;
}
#endif
#if defined(CONFIG_PM_SLEEP)
static int ft5x0x_ts_suspend(struct device *dev)
{
	printk("QHS ft5x0x_ts_suspend\n");

	#if(FT6206_PROXIMITY)
    if (ft6206_proximity_start == 1){
		printk(KERN_EMERG "ft5x0x_ts_suspend,ft6206_proximity_start\n");
		return 0;
	}
#endif
	msleep(300);
#ifdef CONFIG_TP_GESTURE
	if(ft5x0x_ts_check_gesture() == 1){
		printk("ft5x0x_ts_gesture open\n");
		ft5x0x_write_reg(0xd0, 0x1);
		irq_set_irq_type(this_client->irq,IRQF_TRIGGER_LOW | IRQF_NO_SUSPEND);
	}
	else{
		printk("ft5x0x_ts_gesture close\n");
		ft5x0x_write_reg(FT5X0X_REG_PMODE, PMODE_HIBERNATE);
		disable_irq(this_client->irq);
		msleep(1);
	}
#else
		ft5x0x_write_reg(FT5X0X_REG_PMODE, PMODE_HIBERNATE);
#endif

#ifndef CONFIG_TP_GESTURE
	disable_irq(this_client->irq);
	msleep(1);
#endif
	suspend_flag = 1;
	return 0;
}

static int ft5x0x_ts_resume(struct device *dev)
{
	int i = 0;
	int ret = 0;
	unsigned char uc_reg_value;
	printk("QHS ft5x0x_ts_resume\n");
#ifdef CONFIG_TP_GESTURE
	irq_set_irq_type(this_client->irq,IRQF_TRIGGER_FALLING);
	if(s_gesture_switch == 0){
		enable_irq(this_client->irq);
	}
#endif

#if(FT6206_PROXIMITY)
    if ((ft6206_proximity_start == 1) && (0 == suspend_flag)){
		msleep(50);
		return 0;
    }
#endif

#ifdef CONFIG_TP_GESTURE
	disable_irq(this_client->irq);
#endif

	i = 0;
	while(i<5){
		ft5x0x_ts_reset();
		msleep(100);
		ret = ft5x0x_read_reg(FT5X0X_REG_CIPHER, &uc_reg_value);
		if(ret >= 0){break;}

		i++;
	}

//	ft5x0x_write_reg(FT5X0X_REG_PERIODACTIVE, 7);//about 70HZ
//	irq_set_irq_type(this_client->irq,IRQF_TRIGGER_FALLING);
//	msleep(20);
	enable_irq(this_client->irq);
	msleep(10);
	suspend_flag = 0;
#if(FT6206_PROXIMITY)
    if (ft6206_proximity_start == 1){

		msleep(20);
		for(i = 0;i < 3;i++){
			ft5x0x_write_reg(0xb0,0x01);
			msleep(20);
		}
    }
#endif
	return 0;
}

/* bus control the suspend/resume procedure */
static const struct dev_pm_ops ft5x0x_ts_pm_ops = {
/* 	.suspend = ft5x0x_ts_pm_suspend, */
/* 	.resume = ft5x0x_ts_pm_resume, */
	SET_RUNTIME_PM_OPS(ft5x0x_ts_suspend, ft5x0x_ts_resume, NULL)
};

/* static struct notifier_block adf_event_block;
static int ft_adf_event_handler(struct notifier_block *nb, unsigned long action, void *data)
{
	struct adf_notifier_event *event = data;
	int adf_event_data;
	struct device *pdev;

	if (action != ADF_EVENT_BLANK)
		return NOTIFY_DONE;

	adf_event_data = *(int *)event->data;

	switch (adf_event_data) {
	case DRM_MODE_DPMS_ON:
		ft5x0x_ts_resume(pdev);
		break;
	case DRM_MODE_DPMS_OFF:
		ft5x0x_ts_suspend(pdev);
		break;
	default:
		break;
	}

	return NOTIFY_OK;
} */
#else
static void ft5x0x_ts_suspend(struct early_suspend *handler)
{
#if(FT6206_PROXIMITY)
    if (ft6206_proximity_start == 1){
		return;
	}
#endif
	msleep(300);
#ifdef CONFIG_TP_GESTURE

	ft5x0x_write_reg(0xd0, 0x1);
	irq_set_irq_type(this_client->irq,IRQF_TRIGGER_LOW | IRQF_NO_SUSPEND);
#else
		ft5x0x_write_reg(FT5X0X_REG_PMODE, PMODE_HIBERNATE);
#endif

#ifndef CONFIG_TP_GESTURE
	disable_irq(this_client->irq);
	msleep(1);
#endif
	suspend_flag = 1;


}

static void ft5x0x_ts_resume(struct early_suspend *handler)
{
	int i = 0;
	int ret = 0;
	unsigned char uc_reg_value;
#ifdef CONFIG_TP_GESTURE
	irq_set_irq_type(this_client->irq,IRQF_TRIGGER_FALLING);
#endif

#if(FT6206_PROXIMITY)
    if ((ft6206_proximity_start == 1) && (0 == suspend_flag)){
		msleep(50);
		return;
    }
#endif

#ifdef CONFIG_TP_GESTURE
	disable_irq(this_client->irq);
#endif

	i = 0;
	while(i<5){
		ft5x0x_ts_reset();
		msleep(100);
		ret = ft5x0x_read_reg(FT5X0X_REG_CIPHER, &uc_reg_value);
		if(ret >= 0){break;}

		i++;
	}

//	ft5x0x_write_reg(FT5X0X_REG_PERIODACTIVE, 7);//about 70HZ
//	irq_set_irq_type(this_client->irq,IRQF_TRIGGER_FALLING);
//	msleep(20);
	enable_irq(this_client->irq);
	msleep(10);
	suspend_flag = 0;
#if(FT6206_PROXIMITY)
    if (ft6206_proximity_start == 1){

		msleep(20);
		for(i = 0;i < 3;i++){
			ft5x0x_write_reg(0xb0,0x01);
			msleep(20);
		}
    }
#endif

}
#endif

static int ft5x0x_ts_hw_init(struct ft5x0x_ts_data *ft5x0x_ts)
{
	//struct regulator *reg_vdd;
	//struct i2c_client *client = ft5x0x_ts->client;
	struct ft5x0x_ts_platform_data *pdata = ft5x0x_ts->platform_data;
	int ret = 0;

	//vdd power on

	// reg_vdd = regulator_get(&client->dev, pdata->vdd_name);
	// regulator_set_voltage(reg_vdd, 2800000, 2800000);
	//regulator_enable(reg_vdd);

	msleep(10);

	PS_DBG(KERN_INFO "%s [irq=%d];[rst=%d]\n",__func__,pdata->irq_gpio_number,pdata->reset_gpio_number);
	ret = gpio_request(pdata->irq_gpio_number, "ts_irq_pin");
	if(ret < 0){
		printk("QHS %s request irq fail,ret:%d\n",__func__,ret);
		return -1;
	}
	ret = gpio_request(pdata->reset_gpio_number, "ts_rst_pin");
	if(ret < 0){
		printk("QHS %s request rst fail,ret:%d\n",__func__,ret);
		return -1;
	}
	gpio_direction_output(pdata->reset_gpio_number, 1);
	gpio_direction_input(pdata->irq_gpio_number);


	ft5x0x_ts_reset();
	return ret;
}

#ifdef CONFIG_OF
static struct ft5x0x_ts_platform_data *ft5x0x_ts_parse_dt(struct device *dev)
{
	struct ft5x0x_ts_platform_data *pdata;
	struct device_node *np = dev->of_node;
	int ret;

	pdata = kzalloc(sizeof(*pdata), GFP_KERNEL);
	if (!pdata) {
		dev_err(dev, "Could not allocate struct ft5x0x_ts_platform_data");
		return NULL;
	}
	pdata->reset_gpio_number = of_get_gpio(np, 0);
	if(pdata->reset_gpio_number < 0){
		dev_err(dev, "fail to get reset_gpio_number\n");
		goto fail;
	}
	pdata->irq_gpio_number = of_get_gpio(np, 1);
	if(pdata->irq_gpio_number < 0){
		dev_err(dev, "fail to get irq_gpio_number\n");
		goto fail;
	}

	vdd_ana = regulator_get(dev, "vdd_name");
	if (IS_ERR(vdd_ana)) {
		pr_err("regulator get of vdd_ana failed");
		vdd_ana = NULL;
	}
    /*ret = of_property_read_string(np, "vdd_name", &pdata->vdd_name);
	if(ret){
		dev_err(dev, "fail to get vdd_name\n");
		goto fail;
	}*/
	ret = of_property_read_u32_array(np, "virtualkeys", pdata->virtualkeys,12);
	if(ret){
		dev_err(dev, "fail to get virtualkeys\n");
		goto fail;
	}
	ret = of_property_read_u32(np, "TP_MAX_X", &pdata->TP_MAX_X);
	if(ret){
		dev_err(dev, "fail to get TP_MAX_X\n");
		goto fail;
	}
	ret = of_property_read_u32(np, "TP_MAX_Y", &pdata->TP_MAX_Y);
	if(ret){
		dev_err(dev, "fail to get TP_MAX_Y\n");
		goto fail;
	}

	return pdata;
fail:
	kfree(pdata);
	return NULL;
}
#endif

#if 0
//adb by blithe -->
static int fts_input_report_key(struct ft5x0x_ts_data *data, int index)
{
	int i;
	struct ts_event *event = &data->event;
	int x = data->events[index].x;
	int y = data->events[index].y;

	int *x_dim = pdata ->virtualkeys[i*4];
	int *y_dim= pdata ->virtualkeys[1+(i*4)];

	for(i = 0; i < 3; i++){
		if ((x >= x_dim[i] - 10) && (x <= x_dim[i] + 10) &&
		    (y >= y_dim[i] - 10) && (y <= y_dim[i] + 10)) {

			if(event->touch_point> 0){
				data->key_down = true;

				input_report_key(data->input_dev, BTN_TOUCH, 1);
				input_sync(data->input_dev);
			}
			else{
				data->key_down = false;

				input_report_key(data->input_dev, BTN_TOUCH, 0);
				input_sync(data->input_dev);
			}
		}
	}
	return 0;
}
//adb by blithe <--
#endif

static int resume_flag = 0;
static void ft_ts_resume(void)
{
	int ret = 0;

	 struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	 struct ft5x0x_ts_platform_data *pdata = data->platform_data;
	if(resume_flag != 0){
		if(vdd_ana){
			ret = regulator_enable(vdd_ana);
		}
	}
	ret = gpio_request(pdata->reset_gpio_number, "ts_rst_pin");
	if(ret < 0){
		printk("QHS %s request rst fail,ret:%d\n",__func__,ret);
		//return -1;
	}
	ft5x0x_ts_reset();

	ret = gpio_request(pdata->irq_gpio_number, "ts_irq_pin");
	if(ret < 0){
		printk("QHS %s request irq fail,ret:%d\n",__func__,ret);
		//return -1;
	}
	 gpio_direction_output(pdata->irq_gpio_number, 1);

   this_client->irq = gpio_to_irq(pdata->irq_gpio_number);
    gpio_direction_input(pdata->irq_gpio_number);

	resume_flag = 1;
	// enable_irq(this_client->irq);

}

static void ft_ts_suspend(void)
{
	int ret = 0;

	 struct ft5x0x_ts_data *data = i2c_get_clientdata(this_client);
	 struct ft5x0x_ts_platform_data *pdata = data->platform_data;
	 unsigned char ruby;

	//gpio_direction_output(pdata->reset_gpio_number, 0);

	if(vdd_ana){
		ret = regulator_disable(vdd_ana);
	}

	// disable_irq(this_client->irq);


	 gpio_direction_output(pdata->irq_gpio_number, 0);
	 gpio_free(pdata->irq_gpio_number);

	  ft5x0x_write_reg(0xa5,0x3);
	  ft5x0x_read_reg(0xa5, &ruby);
	  pr_err("==ruby==   ruby=0x%x\n",ruby);

}

static ssize_t ft_suspend_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned int input;

	if (kstrtouint(buf, 10, &input))
		return -EINVAL;

	if (input == 1) {
	    pr_err("gt1x go to suspend");
		ft_ts_suspend();
	}
	else if (input == 0) {
		pr_err("gt1x go to resume");
		ft_ts_resume();
	}
	else
		return -EINVAL;

	return count;
}

static DEVICE_ATTR(ts_suspend, 0664, NULL, ft_suspend_store);

static struct attribute *ft_attrs[] = {
	attrify(ts_suspend),
	NULL,
};

static struct attribute_group attr_group = {
	.attrs = ft_attrs,
};


#if defined(CONFIG_TP_COMPATIBLE)
extern short m_tp_adaptation;
#endif

bool quectel_flag = true;
static int
ft5x0x_ts_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct ft5x0x_ts_data *ft5x0x_ts;
	struct input_dev *input_dev;
	struct ft5x0x_ts_platform_data *pdata = client->dev.platform_data;
	struct device_node *np = client->dev.of_node;
	int err = 0;
	int ret = 0;
	unsigned char uc_reg_value;


#ifdef CONFIG_OF


	printk("QHS: %s: probe enter.....\n",__func__);

	if (np && !pdata){
		pdata = ft5x0x_ts_parse_dt(&client->dev);
		if(pdata){
			client->dev.platform_data = pdata;
		}
		else{
			err = -ENOMEM;
			goto exit_alloc_platform_data_failed;
		}
	}
#endif

#if defined(CONFIG_TP_COMPATIBLE)
	if(m_tp_adaptation == 1){
		PS_DBG("m_tp_adaptation:%d\n",m_tp_adaptation);
		goto exit_alloc_data_failed;
	}
#endif
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		err = -ENODEV;
		goto exit_check_functionality_failed;
	}

	ft5x0x_ts = kzalloc(sizeof(*ft5x0x_ts), GFP_KERNEL);
	if (!ft5x0x_ts)	{
		err = -ENOMEM;
		goto exit_alloc_data_failed;
	}

	PS_DBG("ft5x0x tp_MAX_X=%d,tp_MAX_Y=%d\n", pdata->TP_MAX_X, pdata->TP_MAX_Y);
#ifdef CONFIG_TP_GESTURE
	init_para(pdata->TP_MAX_X,pdata->TP_MAX_Y,60,0,0);
#endif

	g_ft5x0x_ts = ft5x0x_ts;

	ft5x0x_ts->platform_data = pdata;

	this_client = client;
	ft5x0x_ts->client = client;

	err = ft5x0x_ts_hw_init(ft5x0x_ts);
	if(err < 0)
	{
		err = -ENOMEM;
		goto exit_alloc_data_failed;
	}

	i2c_set_clientdata(client, ft5x0x_ts);
	client->irq = gpio_to_irq(pdata->irq_gpio_number);

//	sprd_i2c_ctl_chg_clk(client->adapter->nr, 400000);

	ft5x0x_read_reg(FT5X0X_REG_CIPHER, &uc_reg_value);
	if(uc_reg_value != 0x55)
	{
		if(uc_reg_value == 0xa3) {
			msleep(10);
//			fts_ctpm_fw_upgrade_with_i_file_old();
		}
		else {
			printk(KERN_EMERG "chip id error %x\n",uc_reg_value);
			//err = -ENODEV;
			//goto exit_create_singlethread;
		}
	}


	ft5x0x_write_reg(FT5X0X_REG_PERIODACTIVE, 7);//about 70HZ

	INIT_WORK(&ft5x0x_ts->pen_event_work, ft5x0x_ts_pen_irq_work);

	ft5x0x_ts->ts_workqueue = create_singlethread_workqueue(dev_name(&client->dev));
	if (!ft5x0x_ts->ts_workqueue) {
		err = -ESRCH;
		goto exit_create_singlethread;
	}

#if CFG_DETECT_UP_EVENT
    PS_DBG("%s: add up event timer \n",__func__);
    init_timer(&_st_up_evnet_timer);
    _st_up_evnet_timer.function = ft5x06_force_read_up_event;
    _st_up_evnet_timer.data = 1;
    _sui_last_point_cnt = 0;
	msleep(10);
#endif

	input_dev = input_allocate_device();
	if (!input_dev) {
		err = -ENOMEM;
		dev_err(&client->dev, "failed to allocate input device\n");
		goto exit_input_dev_alloc_failed;
	}
	ft5x0x_ts->input_dev = input_dev;

#ifdef CONFIG_FT5X0X_MULTITOUCH
	__set_bit(ABS_MT_TOUCH_MAJOR, input_dev->absbit);
	__set_bit(ABS_MT_POSITION_X, input_dev->absbit);
	__set_bit(ABS_MT_POSITION_Y, input_dev->absbit);
	__set_bit(ABS_MT_WIDTH_MAJOR, input_dev->absbit);
#if defined(CONFIG_PROJECT_TC4_SP081A)
	__set_bit(KEY_APPSELECT,  input_dev->keybit);
#else
	__set_bit(KEY_MENU,  input_dev->keybit);
#endif
	__set_bit(KEY_BACK,  input_dev->keybit);
	__set_bit(KEY_HOMEPAGE,  input_dev->keybit);
	__set_bit(BTN_TOUCH, input_dev->keybit);

	__set_bit(INPUT_PROP_DIRECT, input_dev->propbit);
	input_set_capability(input_dev, EV_KEY, KEY_LEFT);
	input_set_capability(input_dev, EV_KEY, KEY_RIGHT);
	input_set_capability(input_dev, EV_KEY, KEY_UP);
	input_set_capability(input_dev, EV_KEY, KEY_DOWN);
	input_set_capability(input_dev, EV_KEY, KEY_U);
	input_set_capability(input_dev, EV_KEY, KEY_O);
    input_set_capability(input_dev, EV_KEY, KEY_F8);
    input_set_capability(input_dev, EV_KEY, KEY_F9);
    input_set_capability(input_dev, EV_KEY, KEY_F10);
    input_set_capability(input_dev, EV_KEY, KEY_F11);
    input_set_capability(input_dev, EV_KEY, KEY_F12);
	input_set_capability(input_dev, EV_KEY, KEY_W);
	input_set_capability(input_dev, EV_KEY, KEY_M);
	input_set_capability(input_dev, EV_KEY, KEY_E);
	input_set_capability(input_dev, EV_KEY, KEY_C);
    input_set_capability(input_dev, EV_KEY, KEY_Z);
    input_set_capability(input_dev, EV_KEY, KEY_S);
    input_set_capability(input_dev, EV_KEY, KEY_V);
	input_set_capability(input_dev, EV_KEY, KEY_POWER);
	input_set_abs_params(input_dev,
			     ABS_MT_POSITION_X, 0, pdata->TP_MAX_X, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_MT_POSITION_Y, 0, pdata->TP_MAX_Y, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_MT_TOUCH_MAJOR, 0, PRESS_MAX, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_MT_WIDTH_MAJOR, 0, 200, 0, 0);
#else
	__set_bit(ABS_X, input_dev->absbit);
	__set_bit(ABS_Y, input_dev->absbit);
	__set_bit(ABS_PRESSURE, input_dev->absbit);
	__set_bit(BTN_TOUCH, input_dev->keybit);
#if defined(CONFIG_PROJECT_TC4_SP081A)
	__set_bit(KEY_APPSELECT,  input_dev->keybit);
#else
	__set_bit(KEY_MENU,  input_dev->keybit);
#endif
	__set_bit(KEY_BACK,  input_dev->keybit);
	__set_bit(KEY_HOMEPAGE,  input_dev->keybit);

	input_set_abs_params(input_dev, ABS_X, 0, pdata->TP_MAX_X, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, pdata->TP_MAX_Y, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_PRESSURE, 0, PRESS_MAX, 0 , 0);
#endif

#if(FT6206_PROXIMITY)
	//__set_bit(EV_ABS, input_dev->evbit);
	input_set_abs_params(input_dev, ABS_DISTANCE, 0, 1, 0, 0);
#endif

	set_bit(EV_ABS, input_dev->evbit);
	set_bit(EV_KEY, input_dev->evbit);

	input_dev->name		= FT5X0X_NAME;		//dev_name(&client->dev)
	err = input_register_device(input_dev);
	if (err) {
		dev_err(&client->dev,
		"ft5x0x_ts_probe: failed to register input device: %s\n",
		dev_name(&client->dev));
		goto exit_input_register_device_failed;
	}

	err = request_threaded_irq(client->irq, NULL, ft5x0x_ts_interrupt, IRQF_TRIGGER_FALLING | IRQF_ONESHOT | IRQF_NO_SUSPEND, client->name, ft5x0x_ts);
	if (err < 0) {
		dev_err(&client->dev, "ft5x0x_probe: request irq failed %d\n",err);
		goto exit_irq_request_failed;
	}

	disable_irq_nosync(client->irq);

    //get some register information
    uc_reg_value = ft5x0x_read_fw_ver();
	if((uc_reg_value < 0) || (0xff == uc_reg_value)){
		#if !defined(CONFIG_BOARD_TS66B)
		goto exit_ft6206_tp_check_failed;
		#endif
	}

#ifdef TOUCH_VIRTUAL_KEYS
	ft5x0x_ts_virtual_keys_init();
#endif
#if defined(CONFIG_HAS_EARLYSUSPEND)
	PS_DBG("==register_early_suspend =");
	ft5x0x_ts->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN + 1;
	ft5x0x_ts->early_suspend.suspend = ft5x0x_ts_suspend;
	ft5x0x_ts->early_suspend.resume	= ft5x0x_ts_resume;
	register_early_suspend(&ft5x0x_ts->early_suspend);
#endif
#if(FT6206_PROXIMITY)

    err = misc_register(&ft6206_proximity_misc);


    if (err < 0)
    {
    	printk("%s: could not register misc device\n", __func__);
    }

#endif
	#if defined(CONFIG_TP_COMPATIBLE)
	m_tp_adaptation = 1;
	#endif

	msleep(10);
	//get some register information
//	uc_reg_value = ft5x0x_read_fw_ver();
//	TS_DBG("[FST] Firmware version = 0x%x\n", uc_reg_value);
//	TS_DBG("[FST] New Firmware version = 0x%x\n", CTPM_FW[sizeof(CTPM_FW)-2]);

#if 0
	if(uc_reg_value != CTPM_FW[sizeof(CTPM_FW)-2])
	{
		fts_ctpm_fw_upgrade_with_i_file_old();
	}
#endif
#if 0
	ft5x0x_ts->touch_timer.function = ft5x0x_tpd_polling;
	ft5x0x_ts->touch_timer.data = 0;
	init_timer(&ft5x0x_ts->touch_timer);
	ft5x0x_ts->touch_timer.expires = jiffies + HZ*3;
	add_timer(&ft5x0x_ts->touch_timer);
#endif

#ifdef TOUCH_AUTO_UPDATE
	fts_get_upgrade_array(client);
	fts_ctpm_auto_upgrade(client);
#endif
	//ft5x0x_create_sysfs(client);

#ifdef FTS_CTL_IIC
		if (ft_rw_iic_drv_init(client) < 0)
			dev_err(&client->dev, "%s:[FTS] create fts control iic driver failed\n",
					__func__);
#endif

	enable_irq(client->irq);
	msleep(10);

#ifdef CONFIG_TP_GESTURE
	wake_lock_init(&ft_poll_wake_lock, WAKE_LOCK_SUSPEND, "poll-wake-lock");
#endif
/* #ifdef CONFIG_PM_SLEEP
	adf_event_block.notifier_call = ft_adf_event_handler;
	adf_register_client(&adf_event_block);
#endif */

	quectel_flag = false;


	ret = sysfs_create_group(&client->dev.kobj, &attr_group);
	if (ret < 0) {
		printk("%s: Failed to create sysfs attributes\n",__func__);
		// goto err_sysfs;
	}

	ret = sysfs_create_link(NULL, &client->dev.kobj, "touchscreen");
	if (ret < 0) {
		printk("Failed to create link!");
		// goto err_sysfs;
	}

	printk("QHS: %s: probe exit.....\n",__func__);
	return 0;
exit_ft6206_tp_check_failed:
	input_unregister_device(ft5x0x_ts->input_dev);
exit_input_register_device_failed:
exit_input_dev_alloc_failed:
	free_irq(client->irq, ft5x0x_ts);
exit_irq_request_failed:
	cancel_work_sync(&ft5x0x_ts->pen_event_work);
	destroy_workqueue(ft5x0x_ts->ts_workqueue);
exit_create_singlethread:
	i2c_set_clientdata(client, NULL);
	gpio_free(pdata->reset_gpio_number);
	gpio_free(pdata->irq_gpio_number);
	kfree(ft5x0x_ts);
exit_alloc_data_failed:
	if(vdd_ana){
		ret = regulator_disable(vdd_ana);
		regulator_put(vdd_ana);
	}
exit_check_functionality_failed:
exit_alloc_platform_data_failed:
	return err;
}

static int  ft5x0x_ts_remove(struct i2c_client *client)
{

	struct ft5x0x_ts_data *ft5x0x_ts = i2c_get_clientdata(client);

//	TS_DBG("==ft5x0x_ts_remove=\n");
#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&ft5x0x_ts->early_suspend);
#endif
	free_irq(client->irq, ft5x0x_ts);
	input_unregister_device(ft5x0x_ts->input_dev);
	kfree(ft5x0x_ts);
	cancel_work_sync(&ft5x0x_ts->pen_event_work);
	destroy_workqueue(ft5x0x_ts->ts_workqueue);
	i2c_set_clientdata(client, NULL);

	#ifdef FTS_CTL_IIC
	ft_rw_iic_drv_exit();
	#endif

	return 0;
}

static const struct i2c_device_id ft5x0x_ts_id[] = {
       { FT5X0X_NAME, 0 },{ }
};

MODULE_DEVICE_TABLE(i2c, ft5x0x_ts_id);

static const struct of_device_id ft5x0x_of_match[] = {
       { .compatible = "ft5x0x,ft5x0x_ts", },
       { }
};
MODULE_DEVICE_TABLE(of, ft5x0x_of_match);

static struct i2c_driver ft5x0x_ts_driver = {
	.probe		= ft5x0x_ts_probe,
	.remove		= ft5x0x_ts_remove,
	.id_table	= ft5x0x_ts_id,
	.driver	= {
		.name	= FT5X0X_NAME,
		.owner	= THIS_MODULE,
		.of_match_table = ft5x0x_of_match,
#if defined(CONFIG_PM_SLEEP)
		.pm = &ft5x0x_ts_pm_ops,
#endif
	},

};

#if I2C_BOARD_INFO_METHOD
static int __init ft5x0x_ts_init(void)
{
	int ret;
//	printk("==ft5x0x_ts_init==\n");
	ret = i2c_add_driver(&ft5x0x_ts_driver);
	return ret;
//	return i2c_add_driver(&ft5x0x_ts_driver);
}

static void __exit ft5x0x_ts_exit(void)
{
//	printk("==ft5x0x_ts_exit==\n");
	i2c_del_driver(&ft5x0x_ts_driver);
}
#else //register i2c device&driver dynamicly

int sprd_add_i2c_device(struct sprd_i2c_setup_data *i2c_set_data, struct i2c_driver *driver)
{
	struct i2c_board_info info;
	struct i2c_adapter *adapter;
	struct i2c_client *client;
	int ret,err;


	PS_DBG("%s : i2c_bus=%d; slave_address=0x%x; i2c_name=%s",__func__,i2c_set_data->i2c_bus, \
		    i2c_set_data->i2c_address, i2c_set_data->type);

	memset(&info, 0, sizeof(struct i2c_board_info));
	info.addr = i2c_set_data->i2c_address;
	strlcpy(info.type, i2c_set_data->type, I2C_NAME_SIZE);
	if(i2c_set_data->irq > 0)
		info.irq = i2c_set_data->irq;

	adapter = i2c_get_adapter( i2c_set_data->i2c_bus);
	if (!adapter) {
		printk("%s: can't get i2c adapter %d\n",
			__func__,  i2c_set_data->i2c_bus);
		err = -ENODEV;
		goto err_driver;
	}

	client = i2c_new_device(adapter, &info);
	if (!client) {
		printk("%s:  can't add i2c device at 0x%x\n",
			__func__, (unsigned int)info.addr);
		err = -ENODEV;
		goto err_driver;
	}

	i2c_put_adapter(adapter);

	ret = i2c_add_driver(driver);
	if (ret != 0) {
		printk("%s: can't add i2c driver\n", __func__);
		err = -ENODEV;
		goto err_driver;
	}

	return 0;

err_driver:
	return err;
}

void sprd_del_i2c_device(struct i2c_client *client, struct i2c_driver *driver)
{
	PS_DBG("%s : slave_address=0x%x; i2c_name=%s",__func__, client->addr, client->name);
	i2c_unregister_device(client);
	i2c_del_driver(driver);
}

static int __init ft5x0x_ts_init(void)
{
	int ft5x0x_irq;

	ft5x0x_irq=ft5x0x_ts_config_pins();
	ft5x0x_ts_setup.i2c_bus = 1;
	ft5x0x_ts_setup.i2c_address = FT5206_TS_ADDR;
	strcpy (ft5x0x_ts_setup.type,FT5206_TS_NAME);
	ft5x0x_ts_setup.irq = ft5x0x_irq;
	return sprd_add_i2c_device(&ft5x0x_ts_setup, &ft5x0x_ts_driver);
}

static void __exit ft5x0x_ts_exit(void)
{
//	TS_DBG("%s\n", __func__);
	sprd_del_i2c_device(this_client, &ft5x0x_ts_driver);
}
#endif

module_init(ft5x0x_ts_init);
module_exit(ft5x0x_ts_exit);

MODULE_AUTHOR("<wenfs@Focaltech-systems.com>");
MODULE_DESCRIPTION("FocalTech ft5x0x TouchScreen driver");
MODULE_LICENSE("GPL");
