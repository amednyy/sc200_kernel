// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2018 Spreadtrum Communications Inc.
 */

#include <linux/module.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>
#include <linux/regmap.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>



struct unisoc_gpio_acc {
	struct device *dev;
	struct input_dev	*input_dev;
	int	gpio;
	int    acc_on_keycode;
	int    acc_off_keycode;
	int	acc_status;
	int	acc_irq;
};

#ifdef CONFIG_UMW2653_USB_DWC3_HOST_PHY
#if (defined CONFIG_REGUNREG_USBHUB_BY_SCREEN_ONOFF) || \
	(defined CONFIG_REGUNREG_USBHUB_BY_ACC_ONOFF)
extern void sprd_change_panel_count(int cmd);
extern struct task_struct *xhci_sleep_thread;
#endif
#endif

static irqreturn_t unisoc_gpio_acc_handler(int irq, void *dev_id)
{
	int value;
	struct unisoc_gpio_acc *gpio_acc = (struct unisoc_gpio_acc *)dev_id;

	value = !!gpio_get_value(gpio_acc->gpio);
	dev_info(gpio_acc->dev, "%s called gpio=%d value=%d gpio_acc->input_dev=%x \n", __func__, gpio_acc->gpio, value, gpio_acc->input_dev);
	irq_set_irq_type(irq,value ? IRQ_TYPE_LEVEL_LOW : IRQ_TYPE_LEVEL_HIGH);
	gpio_acc->acc_status = value;
	//	wake_lock_timeout(&system_wakeup_lock,msecs_to_jiffies(3000));
	if(gpio_acc->acc_status == 1){
		dev_info(gpio_acc->dev, "acc on\n");
		input_report_key(gpio_acc->input_dev, gpio_acc->acc_on_keycode, 1);
		input_sync(gpio_acc->input_dev);
		input_report_key(gpio_acc->input_dev, gpio_acc->acc_on_keycode, 0);
		input_sync(gpio_acc->input_dev);
		#ifdef CONFIG_UMW2653_USB_DWC3_HOST_PHY
		#ifdef CONFIG_REGUNREG_USBHUB_BY_ACC_ONOFF
		sprd_change_panel_count(1);
		if (NULL != xhci_sleep_thread)
			wake_up_process(xhci_sleep_thread);
		#endif
		#endif
	}
	else{
		dev_info(gpio_acc->dev, "acc off\n");
		input_report_key(gpio_acc->input_dev, gpio_acc->acc_off_keycode, 1);
		input_sync(gpio_acc->input_dev);
		input_report_key(gpio_acc->input_dev, gpio_acc->acc_off_keycode, 0);
		input_sync(gpio_acc->input_dev);
		#ifdef CONFIG_UMW2653_USB_DWC3_HOST_PHY
		#ifdef CONFIG_REGUNREG_USBHUB_BY_ACC_ONOFF
		sprd_change_panel_count(0);
		if (NULL != xhci_sleep_thread)
			wake_up_process(xhci_sleep_thread);
		#endif
		#endif
	}


	return IRQ_HANDLED;
}

static int unisoc_acc_gpio_init(struct unisoc_gpio_acc *gpio_acc)
{
	int irq, ret;

	ret = devm_gpio_request(gpio_acc->dev,
			gpio_acc->gpio, "unisoc gpio acc detect");
	if (ret < 0)
		dev_err(gpio_acc->dev, "gpio%d already devm_gpio_request!\n",gpio_acc->gpio);

	ret = gpio_direction_input(gpio_acc->gpio);
	if (ret < 0)
		goto err;

	gpio_acc->acc_status = !!gpio_get_value(gpio_acc->gpio);
	dev_info(gpio_acc->dev," acc gpio init, gpio=%d value=%d\n", gpio_acc->gpio,gpio_acc->acc_status);

	irq = gpio_to_irq(gpio_acc->gpio);
	if (irq < 0){
		ret = -1;
		goto err;
	}

	gpio_acc->acc_irq = irq;

	return ret;
err:
	devm_gpio_free(gpio_acc->dev, gpio_acc->gpio);

	return ret;
}

static void unisoc_acc_gpio_close(struct input_dev *input)
{
//	struct unisoc_gpio_acc *gpio_acc = input_get_drvdata(input);

}

static ssize_t unisoc_acc_detect_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	int value;
	struct unisoc_gpio_acc *gpio_acc  = dev_get_drvdata(dev);

	value = !!gpio_get_value(gpio_acc->gpio);

	dev_info(dev,  "gpio_acc->gpio=%d  value=%d\n", gpio_acc->gpio, value);
	return sprintf(buf, "gpio_acc->gpio=%d  value=%d\n", gpio_acc->gpio, value);
}

static ssize_t unisoc_acc_detect_store(struct device *dev,
	struct device_attribute *attr,
	const char *buf, size_t count)
{
	return count;
}
static DEVICE_ATTR_RW(unisoc_acc_detect);
  
static struct attribute *unisoc_acc_detect_attrs[] = {
	&dev_attr_unisoc_acc_detect.attr,
	NULL
};
ATTRIBUTE_GROUPS(unisoc_acc_detect);

static int unisoc_acc_gpio_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct unisoc_gpio_acc *gpio_acc;
	int error = 0;
	int ret;


	gpio_acc = devm_kzalloc(&pdev->dev, sizeof(*gpio_acc), GFP_KERNEL);
	if (!gpio_acc)
		return -ENOMEM;
	gpio_acc->dev = dev;
	platform_set_drvdata(pdev, gpio_acc);

	gpio_acc->gpio =  of_get_named_gpio(np, "acc-gpios", 0);
	dev_info(dev, "acc detect gpio =%d\n",gpio_acc->gpio);
	if (gpio_acc->gpio < 0) {
		gpio_acc->gpio = -1;
		dev_err(dev,  "invalid unisoc gpio acc dectect: %d\n",gpio_acc->gpio);
		error = -EINVAL;
		goto err_request_gpio_failed;
	}
	if (of_property_read_u32(np, "linux,code_acc_on", &gpio_acc->acc_on_keycode)) {
		dev_err(dev, "acc on have no keycode\n");
		error = -EINVAL;
		goto err_request_gpio_failed;
	}
	dev_info(dev, "acc_on_keycode=%d\n",gpio_acc->acc_on_keycode);
	if (of_property_read_u32(np, "linux,code_acc_off", &gpio_acc->acc_off_keycode)) {
		dev_err(dev, "acc on have no keycode\n");
		error = -EINVAL;
		goto err_request_gpio_failed;
	}
	dev_info(dev, "acc_off_keycode=%d\n",gpio_acc->acc_off_keycode);

	if(gpio_is_valid(gpio_acc->gpio)){
		ret = unisoc_acc_gpio_init(gpio_acc);
		if (ret) {
			dev_err(dev, "fail to config unisoc gpio acc dectect\n");
			error = -EINVAL;
			goto err_request_gpio_failed;
		}
	}

	gpio_acc->input_dev = devm_input_allocate_device(&pdev->dev);
	if (!gpio_acc->input_dev) {
		dev_err(&pdev->dev, "failed to allocate input device.\n");
		error = -ENOMEM;
		goto err_request_gpio_failed;
	}
	dev_info(dev, "gpio_acc->input_dev =%x\n",gpio_acc->input_dev );

	gpio_acc->input_dev->name = "unisoc-acc-det";
	gpio_acc->input_dev->phys = "unisoc-acc-det/input0";
	gpio_acc->input_dev->id.bustype = BUS_HOST;
	gpio_acc->input_dev->id.version = 0;
	gpio_acc->input_dev->evbit[0] = BIT_MASK(EV_KEY);
	gpio_acc->input_dev->close = unisoc_acc_gpio_close;
	//	input_set_capability(gpio_acc->input_dev, EV_KEY, KEY_POWER);
	set_bit(gpio_acc->acc_on_keycode, gpio_acc->input_dev->keybit);
	set_bit(gpio_acc->acc_off_keycode, gpio_acc->input_dev->keybit);
	set_bit(EV_REP, gpio_acc->input_dev->evbit);

	input_set_drvdata(gpio_acc->input_dev, gpio_acc);


	error = input_register_device(gpio_acc->input_dev);
	if (error) {
		error = -EINVAL;
		dev_err(&pdev->dev, "failed to register input device.\n");
		goto err_request_gpio_failed;
	}

	ret = sysfs_create_groups(&dev->kobj, unisoc_acc_detect_groups);
	if (ret)
		dev_warn(dev, "failed to create unisoc_acc_detect  attributes\n");

	ret = irq_set_irq_type(gpio_acc->acc_irq, IRQ_TYPE_LEVEL_LOW);
	if (ret < 0){
		error = -EINVAL;
		goto err_request_gpio_failed;
	}

	ret = devm_request_threaded_irq(gpio_acc->dev, gpio_acc->acc_irq,
				NULL, unisoc_gpio_acc_handler,
				IRQF_SHARED | IRQF_NO_SUSPEND|IRQF_ONESHOT,
				"unisoc gpio acc dectect irq", gpio_acc);
	if (ret < 0){
		error = -EINVAL;
		dev_err(&pdev->dev, "failed to register irq.\n");
		goto err_request_gpio_failed;
	}
	return 0;

err_request_gpio_failed:
	return error;
}

static const struct of_device_id unisoc_gpoi_acc_of_match[] = {
	{ .compatible = "sprd,unisoc-acc-det", },
	{}
};
MODULE_DEVICE_TABLE(of, sc27xx_vibra_of_match);

static struct platform_driver unisoc_gpio_acc_driver = {
	.driver = {
		.name = "unisoc-acc-det",
		.of_match_table = unisoc_gpoi_acc_of_match,
	},
	.probe = unisoc_acc_gpio_probe,
};

module_platform_driver(unisoc_gpio_acc_driver);

MODULE_DESCRIPTION("Spreadtrum gpio acc Driver");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("xxx  <xxx@spreadtrum.com>");
