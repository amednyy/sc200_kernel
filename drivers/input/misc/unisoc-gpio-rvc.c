// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2018 Spreadtrum Communications Inc.
 */

#include <linux/module.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>
#include <linux/regmap.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>


struct unisoc_gpio_rvc {
	struct device *dev;
	struct input_dev	*input_dev;
	int	gpio;
	int    rvc_on_keycode;
	int    rvc_off_keycode;
	int	rvc_status;
	int	rvc_irq;
};



static irqreturn_t unisoc_gpio_rvc_handler(int irq, void *dev_id)
{
	int value;
	struct unisoc_gpio_rvc *gpio_rvc = (struct unisoc_gpio_rvc *)dev_id;;

	value = !!gpio_get_value(gpio_rvc->gpio);
	dev_info(gpio_rvc->dev, "%s called gpio=%d value=%d gpio_rvc->input_dev=%x \n", __func__, gpio_rvc->gpio, value, gpio_rvc->input_dev);
	irq_set_irq_type(irq, value ? IRQ_TYPE_LEVEL_LOW : IRQ_TYPE_LEVEL_HIGH);
	gpio_rvc->rvc_status = value;
	//	wake_lock_timeout(&system_wakeup_lock,msecs_to_jiffies(3000));
	if (gpio_rvc->rvc_status == 1) {
		dev_info(gpio_rvc->dev, "rvc on\n");
		input_report_key(gpio_rvc->input_dev, gpio_rvc->rvc_on_keycode, 1);
		input_sync(gpio_rvc->input_dev);
		input_report_key(gpio_rvc->input_dev, gpio_rvc->rvc_on_keycode, 0);
		input_sync(gpio_rvc->input_dev);
	} else {
		dev_info(gpio_rvc->dev, "rvc off\n");
		input_report_key(gpio_rvc->input_dev, gpio_rvc->rvc_off_keycode, 1);
		input_sync(gpio_rvc->input_dev);
		input_report_key(gpio_rvc->input_dev, gpio_rvc->rvc_off_keycode, 0);
		input_sync(gpio_rvc->input_dev);
	}
	return IRQ_HANDLED;
}

static int unisoc_rvc_gpio_init(struct unisoc_gpio_rvc *gpio_rvc)
{
	int irq, ret;

	ret = devm_gpio_request(gpio_rvc->dev,
			gpio_rvc->gpio, "unisoc gpio rvc detect");
	if (ret < 0)
		dev_err(gpio_rvc->dev, "gpio%d already devm_gpio_request!\n", gpio_rvc->gpio);

	ret = gpio_direction_input(gpio_rvc->gpio);
	if (ret < 0)
		goto err;

	gpio_rvc->rvc_status = !!gpio_get_value(gpio_rvc->gpio);
	dev_info(gpio_rvc->dev, " rvc gpio init, gpio=%d value=%d\n", gpio_rvc->gpio, gpio_rvc->rvc_status);

	irq = gpio_to_irq(gpio_rvc->gpio);
	if (irq < 0) {
		ret = -1;
		goto err;
	}

	gpio_rvc->rvc_irq = irq;

	return ret;
err:
	devm_gpio_free(gpio_rvc->dev, gpio_rvc->gpio);

	return ret;
}

static void unisoc_rvc_gpio_close(struct input_dev *input)
{
//	struct unisoc_gpio_rvc *gpio_rvc = input_get_drvdata(input);

}

static ssize_t unisoc_rvc_detect_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	int value;
	struct unisoc_gpio_rvc *gpio_rvc  = dev_get_drvdata(dev);

	value = !!gpio_get_value(gpio_rvc->gpio);

	dev_info(dev,  "gpio_rvc->gpio=%d  value=%d\n", gpio_rvc->gpio, value);
	//return sprintf(buf, "gpio_rvc->gpio=%d  value=%d\n", gpio_rvc->gpio, value);
	return sprintf(buf, "%d\n", value);
}

static ssize_t unisoc_rvc_detect_store(struct device *dev,
	struct device_attribute *attr,
	const char *buf, size_t count)
{
	return count;
}
static DEVICE_ATTR_RW(unisoc_rvc_detect);
static DEVICE_ATTR(rvc_state, S_IRUGO, unisoc_rvc_detect_show, NULL);

static struct attribute *unisoc_rvc_detect_attrs[] = {
	&dev_attr_unisoc_rvc_detect.attr,
	&dev_attr_rvc_state.attr,
	NULL
};
ATTRIBUTE_GROUPS(unisoc_rvc_detect);

static int unisoc_rvc_gpio_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct unisoc_gpio_rvc *gpio_rvc;
	int error = 0;
	int ret;


	gpio_rvc = devm_kzalloc(&pdev->dev, sizeof(*gpio_rvc), GFP_KERNEL);
	if (!gpio_rvc)
		return -ENOMEM;
	gpio_rvc->dev = dev;
	platform_set_drvdata(pdev, gpio_rvc);

	gpio_rvc->gpio =  of_get_named_gpio(np, "rvc-gpios", 0);
	dev_info(dev, "rvc detect gpio =%d\n", gpio_rvc->gpio);
	if (gpio_rvc->gpio < 0) {
		gpio_rvc->gpio = -1;
		dev_err(dev,  "invalid unisoc gpio rvc dectect: %d\n",
		gpio_rvc->gpio);
		error = -EINVAL;
		goto err_request_gpio_failed;
	}
	if (of_property_read_u32(np, "linux,code_rvc_on", &gpio_rvc->rvc_on_keycode)) {
		dev_err(dev, "rvc on have no keycode\n");
		error = -EINVAL;
		goto err_request_gpio_failed;
	}
	dev_info(dev, "rvc_on_keycode=%d\n", gpio_rvc->rvc_on_keycode);
	if (of_property_read_u32(np, "linux,code_rvc_off", &gpio_rvc->rvc_off_keycode)) {
		dev_err(dev, "rvc on have no keycode\n");
		error = -EINVAL;
		goto err_request_gpio_failed;
	}
	dev_info(dev, "rvc_off_keycode=%d\n", gpio_rvc->rvc_off_keycode);

	if (gpio_is_valid(gpio_rvc->gpio)) {
		ret = unisoc_rvc_gpio_init(gpio_rvc);
		if (ret) {
			dev_err(dev, "fail to config unisoc gpio rvc dectect\n");
			error = -EINVAL;
			goto err_request_gpio_failed;
		}
	}

	gpio_rvc->input_dev = devm_input_allocate_device(&pdev->dev);
	if (!gpio_rvc->input_dev) {
		dev_err(&pdev->dev, "failed to allocate input device.\n");
		error = -ENOMEM;
		goto err_request_gpio_failed;
	}
	dev_info(dev, "gpio_rvc->input_dev =%x\n", gpio_rvc->input_dev);

	gpio_rvc->input_dev->name = "unisoc-rvc-det";
	gpio_rvc->input_dev->phys = "unisoc-rvc-det/input0";
	gpio_rvc->input_dev->id.bustype = BUS_HOST;
	gpio_rvc->input_dev->id.version = 0;
	gpio_rvc->input_dev->evbit[0] = BIT_MASK(EV_KEY);
	gpio_rvc->input_dev->close = unisoc_rvc_gpio_close;
	//	input_set_capability(gpio_rvc->input_dev, EV_KEY, KEY_POWER);
	set_bit(gpio_rvc->rvc_on_keycode, gpio_rvc->input_dev->keybit);
	set_bit(gpio_rvc->rvc_off_keycode, gpio_rvc->input_dev->keybit);
	set_bit(EV_REP, gpio_rvc->input_dev->evbit);

	input_set_drvdata(gpio_rvc->input_dev, gpio_rvc);


	error = input_register_device(gpio_rvc->input_dev);
	if (error) {
		error = -EINVAL;
		dev_err(&pdev->dev, "failed to register input device.\n");
		goto err_request_gpio_failed;
	}

	ret = sysfs_create_groups(&dev->kobj, unisoc_rvc_detect_groups);
	if (ret)
		dev_warn(dev, "failed to create unisoc_rvc_detect  attributes\n");

	ret = irq_set_irq_type(gpio_rvc->rvc_irq, IRQ_TYPE_LEVEL_LOW);
	if (ret < 0) {
		error = -EINVAL;
		goto err_request_gpio_failed;
	}

	ret = devm_request_threaded_irq(gpio_rvc->dev, gpio_rvc->rvc_irq,
				NULL, unisoc_gpio_rvc_handler,
				IRQF_SHARED | IRQF_NO_SUSPEND|IRQF_ONESHOT,
				"unisoc gpio rvc dectect irq", gpio_rvc);
	if (ret < 0) {
		error = -EINVAL;
		dev_err(&pdev->dev, "failed to register irq.\n");
		goto err_request_gpio_failed;
	}
	return 0;

err_request_gpio_failed:
	return error;
}

static const struct of_device_id unisoc_gpoi_rvc_of_match[] = {
	{ .compatible = "sprd,unisoc-rvc-det", },
	{}
};
MODULE_DEVICE_TABLE(of, sc27xx_vibra_of_match);

static struct platform_driver unisoc_gpio_rvc_driver = {
	.driver = {
		.name = "unisoc-rvc-det",
		.of_match_table = unisoc_gpoi_rvc_of_match,
	},
	.probe = unisoc_rvc_gpio_probe,
};

module_platform_driver(unisoc_gpio_rvc_driver);

MODULE_DESCRIPTION("Spreadtrum gpio rvc Driver");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("xxx  <xxx@spreadtrum.com>");
