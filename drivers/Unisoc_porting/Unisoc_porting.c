#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/device.h>
#include <linux/input.h>
#include <linux/wait.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/timer.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/pm_runtime.h>
#include <linux/kobject.h>
#include <linux/sysfs.h>
#ifdef CONFIG_OF
#include <linux/of.h>
#include <linux/of_gpio.h>
#endif
#ifdef CONFIG_FB
#include <linux/notifier.h>
#include <linux/fb.h>
#endif

#define Unisoc_DRIVER_NAME                                               "Unisoc_base"
#define DRIVER_VERSION							"V1.0.0"
#define Unisoc_DRIVER_DEBUG(str,...)				printk("Unisoc_driver %s %d debug " str, __func__, __LINE__, ##__VA_ARGS__)
#define Unisoc_DRIVER_ERR(str,...)				printk("Unisoc_driver %s %d error " str, __func__, __LINE__, ##__VA_ARGS__)

static int hubsw_gpio = 0;

static struct class *Unisoc_class = NULL;
static struct device *base_device = NULL;

static ssize_t hubsw_gpio_show(struct device *dev, struct device_attribute *attr, char *buf)
{
    snprintf(buf, PAGE_SIZE, "%d\n", gpio_get_value(hubsw_gpio));

    return strlen(buf) + 1;
}
static ssize_t hubsw_gpio_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t size)
{
    if(size > 0) {
        if(1 == simple_strtoul(buf, NULL, 10)) {
            gpio_set_value(hubsw_gpio,1);
        } else if(0 == simple_strtoul(buf, NULL, 10)) {
            gpio_set_value(hubsw_gpio,0);
        }
    }
    return size;
}
static DEVICE_ATTR(hubsw, S_IRUSR | S_IRGRP | S_IROTH, hubsw_gpio_show,hubsw_gpio_store);
static ssize_t version_show(struct device *dev, struct device_attribute *attr, char *buf)
{
    snprintf(buf, PAGE_SIZE, "%s\n", DRIVER_VERSION);

    return strlen(buf) + 1;
}
static DEVICE_ATTR(version, S_IRUSR | S_IRGRP | S_IROTH, version_show, NULL);
static struct attribute *Unisoc_attributes[] = {
    &dev_attr_version.attr,
    &dev_attr_hubsw.attr,
    NULL
};

static const struct attribute_group Unisoc_attr_group = {
    .attrs = Unisoc_attributes,
};
static int Unisoc_base_probe(struct platform_device *pdev)
{
    int ret;
    struct device_node *np = NULL;

    np = pdev->dev.of_node;

////////////////sys add 
    Unisoc_class = class_create(THIS_MODULE, "Unisoc");
    if (IS_ERR(Unisoc_class)) {
        Unisoc_DRIVER_ERR("%s class_create failed\n",__func__);
        ret = PTR_ERR(Unisoc_class);
        goto EXIT;
    }
    base_device = device_create(Unisoc_class, NULL, MKDEV(0, 0), NULL, "base");
    if (IS_ERR(base_device)) {
        Unisoc_DRIVER_ERR("%s device_create failed\n",__func__);
        ret = PTR_ERR(base_device);
        goto EXIT_CLASS;
    } 

    ret = sysfs_create_group(&base_device->kobj, &Unisoc_attr_group);
    if (ret) {
        Unisoc_DRIVER_ERR("%s device_create failed\n",__func__);
        goto EXIT_CLASS;
    }
//////////////////sys end
//////////////////hubsw en add 
    hubsw_gpio = of_get_named_gpio(np,"hubsw-gpio", 0);
    if (!gpio_is_valid(hubsw_gpio)) {
        Unisoc_DRIVER_ERR("GPIO_PIN_SP_POWERON\n");
        ret = -EINVAL;
        goto EXIT_CLASS;
    }
    ret = gpio_request(hubsw_gpio, "hubsw-gpio");
    if (ret){
        printk("gpio scan-en-gpio request err\n");
        goto EXIT_CLASS;
    }
    gpio_direction_output(hubsw_gpio, 0);
/////////////////hubsw en end

    Unisoc_DRIVER_DEBUG("%s sucess\n",__func__);
    return 0;

EXIT_CLASS:
    class_destroy(Unisoc_class);

EXIT:
    return ret;
}
static int Unisoc_base_remove(struct platform_device *pdev)
{

    device_destroy(Unisoc_class, base_device->devt);

    class_destroy(Unisoc_class);

    return 0;
}
static int Unisoc_platform_driver_suspend(struct device *dev)
{

    Unisoc_DRIVER_DEBUG("ok\n");

    return 0;
}
static int Unisoc_platform_driver_resume(struct device *dev)
{

    Unisoc_DRIVER_DEBUG("ok\n");

    return 0;
}

static const struct dev_pm_ops Unisoc_platform_driver_pm_ops = {
    .suspend = Unisoc_platform_driver_suspend,
    .resume = Unisoc_platform_driver_resume,
};

static const struct of_device_id Unisoc_base_of_match[] = {
    { .compatible = "Unisoc,Unisoc_base", },
    {}
};

static struct platform_driver Unisoc_base_platform_driver = {
    .driver = {
        .name = Unisoc_DRIVER_NAME,
        .owner	= THIS_MODULE,
        .pm = &Unisoc_platform_driver_pm_ops,
        .of_match_table = Unisoc_base_of_match,
    },

    .probe	= Unisoc_base_probe,
    .remove	= Unisoc_base_remove,
};


static int __init Unisoc_base_driver_init(void)
{
    int ret = 0;

    Unisoc_DRIVER_DEBUG("%s init ver:%s\n",__func__, DRIVER_VERSION);

    ret = platform_driver_register(&Unisoc_base_platform_driver);
    if (ret) {
        goto EXIT;
    }


EXIT:
    return ret;
}

static void __exit Unisoc_base_driver_exit(void)
{


    platform_driver_unregister(&Unisoc_base_platform_driver);


    Unisoc_DRIVER_DEBUG("%s exit ver:%s\n",__func__, DRIVER_VERSION);
}

module_init(Unisoc_base_driver_init);
module_exit(Unisoc_base_driver_exit);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("ajay");
MODULE_DESCRIPTION("Unisoc base driver");
MODULE_VERSION(DRIVER_VERSION);
