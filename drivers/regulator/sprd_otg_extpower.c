/*
 * otg regulator driver for otg power which is controlled by gpio.
 * Copyright (C) 2018 Spreadtrum Communications Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/module.h>
#include <linux/of.h>
#include <linux/gpio.h>
#include <linux/gpio/consumer.h>
#include <linux/platform_device.h>
#include <linux/regmap.h>
#include <linux/regulator/driver.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/of_regulator.h>

 struct otg_power_en_gpio {
	struct gpio_desc *gpio;
	bool power_enable;
};
static int ext_otgpwr_regulator_enable(struct regulator_dev *rdev)
{
	struct otg_power_en_gpio *otg_pwren = rdev_get_drvdata(rdev);

	pr_info("sprd otg ext power-regulator: enable\n");
	gpiod_set_value_cansleep(otg_pwren->gpio, 1);
	otg_pwren->power_enable = 1;

	return 0;
}

static int ext_otgpwe_regulator_disable(struct regulator_dev *rdev)
{
	struct otg_power_en_gpio *otg_pwren = rdev_get_drvdata(rdev);

	pr_info("sprd otg ext power-regulator: disable\n");
	gpiod_set_value_cansleep(otg_pwren->gpio, 0);
	otg_pwren->power_enable = 0;

	return 0;
}

static int regulator_ext_otgpwr_is_enabled(struct regulator_dev *rdev)
{
	struct otg_power_en_gpio *otg_pwren = rdev_get_drvdata(rdev);
	int val;

	val = otg_pwren->power_enable;
	return val;
}

static struct regulator_ops ext_otgpwr_regulator_ops = {
	.enable = ext_otgpwr_regulator_enable,
	.disable = ext_otgpwe_regulator_disable,
	.is_enabled = regulator_ext_otgpwr_is_enabled,
};

static const struct regulator_desc otg_extpwr_regulator_desc = {
	.name = "otg-vbus",
	.of_match = "otg-vbus",
	.type = REGULATOR_VOLTAGE,
	.owner = THIS_MODULE,
	.ops = &ext_otgpwr_regulator_ops,
};

static int sprd_extpwr_regulator_probe(struct platform_device *pdev)
{
	struct regulator_config config = {};
	struct regulator_dev *rdev;
	struct gpio_desc *gpio;
	struct otg_power_en_gpio *otg_pwren;
	int ret;

	otg_pwren = devm_kzalloc(&pdev->dev, sizeof(*otg_pwren), GFP_KERNEL);
	if (!otg_pwren)
		return -ENOMEM;

	gpio = devm_gpiod_get_optional(&pdev->dev, "otgpower", GPIOD_OUT_LOW);
	if (IS_ERR(gpio))
		return PTR_ERR(gpio);

	otg_pwren->gpio = gpio;

	config.dev = &pdev->dev;
	config.driver_data = otg_pwren;
	config.of_node = pdev->dev.of_node;

	rdev = devm_regulator_register(&pdev->dev,
				       &otg_extpwr_regulator_desc, &config);
	if (IS_ERR(rdev)) {
		ret = PTR_ERR(rdev);
		dev_err(&pdev->dev,
			"failed to register regulator: (%d)\n", ret);
		return ret;
	}
	printk(" %s end\n", __FUNCTION__);
	return 0;
}

static const struct of_device_id sprd_extpwr_regulator_of_match[] = {
	{ .compatible = "sprd,ext-otg-power", },
	{ }
};

static struct platform_driver sprd_extpwr_regulator_driver = {
	.driver = {
		.name		= "extotgvbus-regulator",
		.of_match_table	= sprd_extpwr_regulator_of_match,
	},
	.probe = sprd_extpwr_regulator_probe,
};

static int __init sprd_ext_otgpwr_regulator_driver_init(void)
{
	return platform_driver_register(&sprd_extpwr_regulator_driver);
}

subsys_initcall_sync(sprd_ext_otgpwr_regulator_driver_init);

MODULE_DESCRIPTION("otg regulator driver for ext power");
MODULE_LICENSE("GPL v2");
