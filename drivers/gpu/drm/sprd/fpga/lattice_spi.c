#include <linux/device.h>
#include <linux/module.h>
#include <linux/of_gpio.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/spi/spi.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <asm/unaligned.h>

#ifdef CONFIG_LCD_RM67200
#include "lattice_rm67200.h"
#endif
#ifdef CONFIG_LCD_TD4310
#include "lattice_td4310.h"
#include "lattice_td4310_single.h"
#endif
//#define LATTICE_DEBUG
static struct spi_device *g_spi;

struct fpga_data {
	struct gpio_desc *crstn_pin;
	struct gpio_desc *rstn_pin;
	struct gpio_desc *en1v2_pin;
	struct gpio_desc *en3v3_pin;
	struct gpio_desc *en1v8_pin;
	struct gpio_desc *en2v5_pin;
};

static char cmd_init[] = {0xff, 0xa4, 0xc6, 0xf4, 0x8a};
static char cmd_en_prog[] = {0xc6, 0x00, 0x00, 0x00};
static char cmd_exit_prog1[] = {0x26, 0x00, 0x00, 0x00};
static char cmd_exit_prog2[] = {0xff, 0xff, 0xff, 0xff};
static char cmd_erase_device[] = {0x0e, 0x00, 0x00, 0x00};
static char cmd_init_addr[] = {0x46, 0x00, 0x00, 0x00};
static char cmd_transfer_file[] = {0x7a, 0x00, 0x00, 0x00};
#ifdef LATTICE_DEBUG
static char cmd_checkid[] = {0xe0, 0x00, 0x00, 0x00};
static char cmd_read_status[] = {0x3c, 0x00, 0x00, 0x00};
#endif

static bool single_lcd_display;

static int __init boot_mode(char *str)
{
	if (!str)
		return 0;

	if (!strncmp(str, "charger", strlen("charger")))
		single_lcd_display = true;

	return 0;
}
__setup("androidboot.mode=", boot_mode);

static void fpga_power(bool enable)
{
	struct fpga_data *data = NULL;

	if(!g_spi)
		return;
	data = spi_get_drvdata(g_spi);
	if (enable == 0) {
		gpiod_set_value(data->crstn_pin, 0);
		gpiod_set_value(data->rstn_pin, 0);
	}

	if(!IS_ERR(data->en1v2_pin))
		gpiod_set_value(data->en1v2_pin, enable);

	if(!IS_ERR(data->en3v3_pin))
		gpiod_set_value(data->en3v3_pin, enable);

	if(!IS_ERR(data->en1v8_pin))
		gpiod_set_value(data->en1v8_pin, enable);

	if(!IS_ERR(data->en2v5_pin))
		gpiod_set_value(data->en2v5_pin, enable);

	pr_info("[lattice] fpga_power:%d(1:on, 0:off)!\n", enable);
}

void reset_fpga(void)
{
	struct fpga_data *data = NULL;

	if(!g_spi)
		return;
	data = spi_get_drvdata(g_spi);
	gpiod_set_value(data->rstn_pin, 1);
	mdelay(1);
	gpiod_set_value(data->rstn_pin, 0);
	mdelay(1);
	gpiod_set_value(data->rstn_pin, 1);
}
EXPORT_SYMBOL_GPL(reset_fpga);

static int lattice_spi_write(char *tx_buf, unsigned len)
{
	struct spi_message m;
	struct spi_transfer t;

	memset(&t, 0, sizeof(struct spi_transfer));
	spi_message_init(&m);

	spi_message_add_tail(&t, &m);

	t.tx_buf = tx_buf;
	t.len = len;
	t.speed_hz = 48000000;

	return spi_sync(g_spi, &m);
}

static int lattice_spi_write_bitfile(char *tx_buf_cmd, unsigned cmd_len, char *tx_buf_data, unsigned data_len)
{
	struct spi_message m;
	struct spi_transfer t_cmd;
	struct spi_transfer t_data;

	memset(&t_cmd, 0, sizeof(struct spi_transfer));
	memset(&t_data, 0, sizeof(struct spi_transfer));
	spi_message_init(&m);

	spi_message_add_tail(&t_cmd, &m);
	spi_message_add_tail(&t_data, &m);

	t_cmd.tx_buf = tx_buf_cmd;
	t_cmd.len = cmd_len;
	t_cmd.speed_hz = 48000000;
	t_data.tx_buf = tx_buf_data;
	t_data.len = data_len;
	t_data.speed_hz = 48000000;

	return spi_sync(g_spi, &m);
}

void fpga_load_bitfile(bool enable)
{
#ifdef LATTICE_DEBUG
	unsigned char id[4] = {0};
	unsigned char status[4] = {0};
#endif
	struct fpga_data *data = NULL;

	if(!g_spi)
		return;
	data = spi_get_drvdata(g_spi);

	if(!enable) {
		fpga_power(enable);
		return;
	}

	fpga_power(enable);
	pr_info("[lattice] fpga_load_bitfile enter!\n");

	gpiod_set_value(data->crstn_pin, 0);
	mdelay(10);
	lattice_spi_write(cmd_init, 5);
	mdelay(10);
	gpiod_set_value(data->crstn_pin, 1);
	mdelay(10);
#ifdef LATTICE_DEBUG
	spi_write_then_read(g_spi, cmd_checkid, 4, id, 4);
	pr_info("[lattice] fpga_load_bitfile id=%x,%x,%x,%x\n", id[0], id[1],id[2],id[3]);
#endif
	lattice_spi_write(cmd_en_prog, 4);
	mdelay(1);

	lattice_spi_write(cmd_erase_device, 4);
	mdelay(10);

	lattice_spi_write(cmd_init_addr, 4);
	mdelay(1);
	if(single_lcd_display)
	lattice_spi_write_bitfile(cmd_transfer_file, 4, bitfile_single, sizeof(bitfile_single));
	else
	lattice_spi_write_bitfile(cmd_transfer_file, 4, bitfile, sizeof(bitfile));
	mdelay(1);
#ifdef LATTICE_DEBUG
	spi_write_then_read(g_spi, cmd_read_status, 4, status, 4);
	pr_info("[lattice] fpga_load_bitfile status=%x,%x,%x,%x\n", status[0], status[1],status[2],status[3]);
#endif
	lattice_spi_write(cmd_exit_prog1, 4);
	mdelay(1);
	lattice_spi_write(cmd_exit_prog2, 4);

	pr_info("[lattice] fpga_load_bitfile end!\n");
}
EXPORT_SYMBOL_GPL(fpga_load_bitfile);

static int lattice_probe(struct spi_device *spi)
{
	struct fpga_data *data;
	g_spi = spi;
	g_spi->mode = SPI_MODE_1;

	pr_info("[lattice] lattice_probe in!\n");
	data = devm_kzalloc(&spi->dev, sizeof(*data), GFP_KERNEL);
	if (!data) {
		dev_err(&spi->dev, "Memory allocation for fpga_data failed\n");
		return -ENOMEM;
	}
	spi_set_drvdata(spi, data);

	data->crstn_pin = devm_gpiod_get(&spi->dev, "crstn", GPIOD_OUT_HIGH);
	if (IS_ERR(data->crstn_pin)) {
		pr_err("crstn_pin invalid\n");
	}
	data->rstn_pin = devm_gpiod_get(&spi->dev, "rstn", GPIOD_OUT_HIGH);
	if (IS_ERR(data->rstn_pin)) {
		pr_err("rstn_pin invalid\n");
	}
	data->en1v2_pin = devm_gpiod_get(&spi->dev, "en1v2", GPIOD_OUT_HIGH);
	if (IS_ERR(data->en1v2_pin)) {
		pr_err("en1v2_pin invalid\n");
		return -EPROBE_DEFER;
	}
	data->en3v3_pin = devm_gpiod_get(&spi->dev, "en3v3", GPIOD_OUT_HIGH);
	if (IS_ERR(data->en3v3_pin)) {
		pr_err("en3v3_pin invalid\n");
	}
	data->en1v8_pin = devm_gpiod_get(&spi->dev, "en1v8", GPIOD_OUT_HIGH);
	if (IS_ERR(data->en1v8_pin)) {
		pr_err("en1v8_pin invalid\n");
	}
	data->en2v5_pin = devm_gpiod_get(&spi->dev, "en2v5", GPIOD_OUT_HIGH);
	if (IS_ERR(data->en2v5_pin)) {
		pr_err("en2v5_pin invalid\n");
	}

	pr_info("[lattice] lattice_probe success!\n");

	return 0;
}

static int lattice_remove(struct spi_device *spi)
{
	return 0;
}

static struct of_device_id lattice_fpga_of_match_table[] = {
	{.compatible = "lattice-spi"},
	{},
};

static struct spi_driver lattice_driver = {
	.probe = lattice_probe,
	.remove = lattice_remove,
	.driver = {
		.name = "lattice-spi",
		.of_match_table = of_match_ptr(lattice_fpga_of_match_table),
	},
};

module_spi_driver(lattice_driver);

MODULE_AUTHOR("Pillow Zeng <pillow.zeng@unisoc.com>");
MODULE_DESCRIPTION("Lattice FPGA configuration via SPI");
MODULE_LICENSE("GPL");
